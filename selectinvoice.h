#ifndef SELECTINVOICE_H
#define SELECTINVOICE_H

#include <QDialog>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlRecord>

namespace Ui {
class selectinvoice;
}

class selectinvoice : public QDialog
{
    Q_OBJECT

public:
    explicit selectinvoice(QWidget *parent = nullptr);
    ~selectinvoice();

    int GetInvoiceId();

private slots:

    void SetInvoiceId();

private:
    Ui::selectinvoice *ui;
    QSqlQueryModel *model_comboBox_invoice;
    int invoiceid = 0;
};

#endif // SELECTINVOICE_H
