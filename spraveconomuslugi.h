#ifndef SPRAVECONOMUSLUGI_H
#define SPRAVECONOMUSLUGI_H

#include <QDialog>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QComboBox>
#include <QKeyEvent>
#include <QItemDelegate>
#include <QLineEdit>
#include <QDebug>
#include <QObject>

// Набор глобальных перечислителей
class Econom : public QObject {
public:
    /// достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int

    enum class econom :int {id,name_usl,changes};
    Q_ENUM(econom)

    Q_OBJECT
    Econom() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums
};


//****************************************** Делегат для редактирования справочника экономистов ********************************

class spravecon_delegate : public QItemDelegate
{
    Q_OBJECT
public:

    QWidget * parent_w;

    explicit spravecon_delegate(QWidget *parent)
    {
        parent_w = parent;
    }

    ~spravecon_delegate()
    {

    }

    // функция инициации процесса редактирования
    bool editorEvent(QEvent *event, QAbstractItemModel *, const QStyleOptionViewItem , const QModelIndex )
    {
        if(event->type()== QEvent::KeyPress)
        {
            QKeyEvent *KeyEvent = static_cast<QKeyEvent*>(event);

            if (KeyEvent->key() == Qt::Key_F2)
            {
                return false;
            }else
                return true; // если не F2 то не редактируем
        }

        if(event->type() != QEvent::MouseButtonDblClick){ //  не двойной клик мышкой
            return false;  // если не двойной клик то выполняем то что положено на 1 клик
        }

        return true;
    }

    // создаем виджет редактора значения
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        if(index.column() == 2) // Если хотим изменить поле уровня доступа
        {
            QComboBox *editor = new QComboBox(parent);
            return editor;
        }

        return QItemDelegate::createEditor(parent,option,index);
    }

    //передаем значение от модели в редактор
    void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QLineEdit *name = static_cast<QLineEdit*>(editor);
        name->setText(index.model()->data(index,Qt::DisplayRole).toString().trimmed());
        return;
    }
    //передает значение от редактора в модель
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
    {

        QLineEdit *name = static_cast<QLineEdit*>(editor);
        if(name->text().trimmed() == model->data(index).toString()){
            return;
        }

        QModelIndexList IndexList = model->match(model->index(0,1),Qt::DisplayRole,name->text().trimmed(),1,Qt::MatchFixedString);
        if(IndexList.length() != 0){
            QMessageBox::information(parent_w, "Information", "Такое наименование существует!");
            return;
        }
        model->setData(model->index(index.row(),static_cast<int>(Econom::econom::changes)),QString("true"));
        return QItemDelegate::setModelData(editor,model,index);
    }
};






namespace Ui {
class spraveconomuslugi;
}

class spraveconomuslugi : public QDialog
{
    Q_OBJECT

public:

    explicit spraveconomuslugi(QWidget *parent = nullptr);
    ~spraveconomuslugi();


    bool SetModel();
    void SetHeaderData();
    void SetColumnsWidth();

private slots:

    void addRecord();
    void delRecord();
    void SetChangeData();
    void SaveChanges();

    void CloseWindows();

private:
    Ui::spraveconomuslugi *ui;
    QStandardItemModel *model;
    bool changeData = false;
};

#endif // SPRAVECONOMUSLUGI_H
