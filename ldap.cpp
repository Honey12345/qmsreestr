/**************************   WINDOWS   **********************************************/
// Функция получения полного имени пользователя и права администратора из AD
//************************************************************************************
//  Add Wldap32.dll to your project.
//  Добавте файлы определения именно в таком порядке
//  #include "windows.h"
//  #include "winldap.h"
//
#include "windows.h"
#include "winldap.h"
#include <QString>
#include <QStringList>
#include <QMessageBox>
#include <QProcess>

bool LDAPAuth(QString login, QString pass, QString *cn){
    return false;
    Q_UNUSED(login);Q_UNUSED(pass);Q_UNUSED(cn);
}

QStringList LDAP_connect()
{
    QString PathAD, UserName, str_temp;
    QStringList envlist,param;
    // Return value 0 - username from (OS) system, 1 - displayName from AD, 2 - true whis ok authentification on AD else false
    param << "" << "" << "false";


    // получение имени пользователя из системы

    envlist = QProcess::systemEnvironment();
//qDebug() << envlist;
    str_temp = envlist.filter("USERNAME").at(0);
    UserName = str_temp.remove("USERNAME=");
    str_temp = envlist.filter("LOGONSERVER").at(0) +"." + envlist.filter("USERDNSDOMAIN").at(0);
    PathAD=str_temp.remove("LOGONSERVER=\\\\").remove("USERDNSDOMAIN=");


    param[0] = UserName;
    param[1] = "";
    PWCHAR hostName = NULL;
    PWCHAR pMyDN = NULL;
    LDAP* pLdapConnection = NULL;
    ULONG version = LDAP_VERSION3;
    ULONG connectSuccess = 0;
    ULONG returnCode = 0;
    ULONG searchSuccess = 0;

    //  Initialize a session. LDAP_PORT is the default port, 389.

    QString str_error;
    QString Test = PathAD;
    hostName = (WCHAR*)(Test.utf16());
    QString Test2 =  "DC=" + PathAD.section(".",1,1) + ",DC=" +  PathAD.section(".",2); // "DC=VIVEYA,DC=local";
//    Test2 =  "OU=PERINATAL,DC=guzpc,DC=local";
    pMyDN = (WCHAR*)(Test2.utf16());

    pLdapConnection = ldap_init(hostName, LDAP_PORT);

    if (pLdapConnection == NULL)
    {
        //  Set the HRESULT based on the Windows error code.
        char hr = HRESULT_FROM_WIN32(GetLastError());

        str_error = "Ошибка инициализации Active Directory.\nКод ошибки 0х" + QString::fromUtf8(&hr);
        QMessageBox::information(0, "Внимание",str_error);
        //        qDebug() << "ldap_init failed with 0x" << hr;
        ldap_unbind(pLdapConnection);
        return param;
    }
    //    else
    //        qDebug() << "ldap_init succeeded";


    //  Set the version to 3.0 (default is 2.0).
    returnCode = ldap_set_option(pLdapConnection,
                                 LDAP_OPT_PROTOCOL_VERSION,
                                 (void*)&version);

    if(returnCode == LDAP_SUCCESS);
    //        qDebug() << "ldap_set_option succeeded - version set to 3";
    else
    {
        str_error = "Ошибка установки параметров Active Directory.\nКод ошибки 0х" + QString::number(returnCode,16);
        QMessageBox::information(0, "Внимание",str_error);
        //        qDebug() << "SetOption Error: " << returnCode;
        ldap_unbind(pLdapConnection);
        return param ;
    }


    // Connect to the server.
    connectSuccess = ldap_connect(pLdapConnection, NULL);

    if(connectSuccess != LDAP_SUCCESS)
    {

        str_error = "Ошибка подключения к Active Directory.\nКод ошибки 0х" + QString::number(connectSuccess,16);
        QMessageBox::information(0, "Внимание",str_error);


        //qDebug() << "ldap_connect failed with 0x" + QString::number(connectSuccess,16);
        ldap_unbind(pLdapConnection);
        return param ;
    }

    //  Bind with current credentials (login credentials). Be
    //  aware that the password itself is never sent over the
    //  network, and encryption is not used.

    returnCode = ldap_bind_s(pLdapConnection, NULL, NULL,
                             LDAP_AUTH_NEGOTIATE);


    if (returnCode == LDAP_SUCCESS)
    {
        param[2] = "true";
    //        qDebug() << "The bind was successful ";
    }
    else
    {
        str_error = "<p align='center'>Ошибка авторизации в Active Directory.<br>"
                    "Пользователь '" + UserName + "' не найден.<br>"
                                              "Код ошибки 0х" + QString::number(returnCode,16);
        QMessageBox::information(0, "Внимание",str_error);
        ldap_unbind(pLdapConnection);
        //        qDebug() << "The bind was unsuccessful with 0x" + QString::number(returnCode,16);
        return param ;
    }


    //Variables for Search Results
    LDAPMessage* pSearchResult;
    PWCHAR pMyFilter = NULL;

    QString str = "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" + UserName + "))";
    //    QString Test7 = QString::fromLatin1((str));
    pMyFilter = (WCHAR*)(str.utf16());

    PWCHAR pMyAttributes[3];

    pMyAttributes[0] = (WCHAR*)QString("cn").utf16();
    pMyAttributes[1] = (WCHAR*)QString("displayName").utf16();
    //    pMyAttributes[3] = (WCHAR*)QString("sn").utf16();
//    pMyAttributes[2] = (WCHAR*)QString("memberOf").utf16();
    pMyAttributes[2] = NULL;
//    pMyAttributes[3] = NULL;

    /*    PWCHAR pMyAttributes[6];

        pMyAttributes[0] = (WCHAR*)QString("cn").utf16();
        pMyAttributes[1] = (WCHAR*)QString("company").utf16();
        pMyAttributes[2] = (WCHAR*)QString("userPrincipalName").utf16();
        pMyAttributes[3] = (WCHAR*)QString("sn").utf16();
        pMyAttributes[4] = (WCHAR*)QString("memberOf").utf16();
        pMyAttributes[5] = NULL;*/

    searchSuccess = ldap_search_s(
                pLdapConnection,    // Session handle
                pMyDN,              // DN to start search
                LDAP_SCOPE_SUBTREE, // Scope
                pMyFilter,          // Filter
                pMyAttributes,      // Retrieve list of attributes
                0,                  // Get both attributes and values
                &pSearchResult);    // [out] Search results


    if (searchSuccess != LDAP_SUCCESS)
    {

        str_error = "Ошибка поиска в Active Directory!\nКод ошибки 0х" + QString::number(searchSuccess,16);
        QMessageBox::information(0, "Внимание",str_error);
        //          qDebug() << "ldap_search_s failed with 0x0" + QString::number(searchSuccess,16);
        ldap_unbind_s(pLdapConnection);

        if(pSearchResult != NULL)
        {
            ldap_msgfree(pSearchResult);
            return param;
        }
    }
    //       else;
    //           qDebug() << "ldap_search succeeded ";


    //*********************************************************************************************************
    // Get the number of entries returned.
    ULONG numberOfEntries;

    numberOfEntries = ldap_count_entries(
                pLdapConnection,    // Session handle
                pSearchResult);     // Search result

    /*if(numberOfEntries == NULL)
        {
            qDebug("ldap_count_entries failed with 0x%0lx \n",errorCode);
            ldap_unbind_s(pLdapConnection);
            if(pSearchResult != NULL)
                ldap_msgfree(pSearchResult);
        }
        else
            qDebug("ldap_count_entries succeeded \n");
        */
    //qDebug("The number of entries is: %d \n", numberOfEntries);


    //----------------------------------------------------------
    // Loop through the search entries, get, and output the
    // requested list of attributes and values.
    //----------------------------------------------------------
    LDAPMessage* pEntry = NULL;
    ULONG iCnt = 0;
    BerElement* pBer = NULL;
    PWCHAR pAttribute = NULL;
    PWCHAR* ppValue = NULL;
    ULONG iValue = 0;

    for( iCnt=0; iCnt < numberOfEntries; iCnt++ )
    {
        // Get the first/next entry.

        if( !iCnt ) // !iCnt = true если 0
            pEntry = ldap_first_entry(pLdapConnection, pSearchResult);
        else
            pEntry = ldap_next_entry(pLdapConnection, pEntry);


        // Output the entry number.
        //qDebug("ENTRY NUMBER %i \n", iCnt);

        // Get the first attribute name.
        pAttribute = ldap_first_attribute(
                    pLdapConnection,   // Session handle
                    pEntry,            // Current entry
                    &pBer);            // [out] Current BerElement

        // Output the attribute names for the current object
        // and output values.
        while(pAttribute != NULL)
        {
            // Output the attribute name.
            QString abc = QString::fromWCharArray(pAttribute);
            //            qDebug() << "Name of attribute" << abc;

            // Get the string values.

            ppValue = ldap_get_values(
                        pLdapConnection,  // Session Handle
                        pEntry,           // Current entry
                        pAttribute);      // Current attribute

            // Print status if no values are returned (NULL ptr)
            if(ppValue == NULL)
            {
                qDebug(": [NO ATTRIBUTE VALUE RETURNED]");
            }

            // Output the attribute values
            else
            {
                iValue = ldap_count_values(ppValue);
                if(!iValue)
                {
                    qDebug(": [BAD VALUE LIST]");
                }
                else
                {
                    // Output the first attribute value

                    if(abc == "displayName")
                        param[1] = QString::fromWCharArray(*ppValue);
                    //                    else if(abc == "cn")
                    //                        param[0] = QString::fromWCharArray(*ppValue);

                    // Output more values if available
//                    ULONG z;
//                    for(z=1; z<iValue; z++)
//                    {
//                        QString abc3 = QString::fromWCharArray(ppValue[z]);
//                        if(abc3.contains("CN=Администраторы домена"))
//                            param[2] = "1";
//                        //qDebug() << "abc3" << abc3;
//                    }
                }
            }

            // Free memory.
            if(ppValue != NULL)
                ldap_value_free(ppValue);
            ppValue = NULL;
            ldap_memfree(pAttribute);

            // Get next attribute name.
            pAttribute = ldap_next_attribute(
                        pLdapConnection,   // Session Handle
                        pEntry,            // Current entry
                        pBer);             // Current BerElement
            //            qDebug("\n");
        }

        //        if( pBer != NULL )
        //            ber_bvfree(pBer);
        pBer = NULL;
    }

    //  Normal cleanup and exit.
    ldap_unbind(pLdapConnection);
    return param ;

}
