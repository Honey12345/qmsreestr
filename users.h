#ifndef USERS_H
#define USERS_H

#include "global.h"
#include <QDialog>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlError>
#include <QComboBox>
#include <QKeyEvent>
#include <QItemDelegate>
#include <QLineEdit>
#include <QDebug>
#include <QSettings>
#include <QMessageBox>
#include <QTimer>


//****************************************** Модель Пользователей ********************************

class my_users_SqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    QWidget * parent_w;

    explicit my_users_SqlQueryModel(QWidget *parent)
    {
        parent_w = parent;

    }
    ~my_users_SqlQueryModel()
    {
    }

    Qt::ItemFlags flags(const QModelIndex &index) const
    {
        Qt::ItemFlags flags = QSqlQueryModel::flags(index);

        if ( index.column() == 1 || index.column() == 2 || index.column() == 3 || index.column() == 4)
            flags |= Qt::ItemIsEditable;
        return flags;

    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
        QVariant value = QSqlQueryModel::data(index, role);
        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
        case Qt::DisplayRole:
            if(index.column() == 2){
                return strKod.value(value.toInt());
            }
            if(index.column() == 4){
                return (data.toBool()) ? "ДА" : "НЕТ";
            }
            break;

        case Qt::TextAlignmentRole:
            if(index.column() == 2 || index.column() == 4){
                return int(Qt::AlignHCenter| Qt::AlignVCenter);
            }
            else{
                return int(Qt::AlignLeft| Qt::AlignVCenter);
            }
            break;
        case Qt::TextColorRole:
            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),4)).toBool())
            {
                return QBrush(Qt::gray,Qt::SolidPattern);
            }
            break;
        default:
            break;

        }
        return QSqlQueryModel::data(index, role);

    }

private slots:

    void reciveData_NameKodAccess(const QStringList str);

signals:


private:
     QStringList strKod;

};

//****************************************** Делегат для редактирования Пользователей ********************************

class user_delegate : public QItemDelegate
{
    Q_OBJECT
public:

    QWidget * parent_w;

    explicit user_delegate(QWidget *parent)
    {
        parent_w = parent;
    }

    ~user_delegate()
    {

    }

    // функция инициации процесса редактирования
    bool editorEvent(QEvent *event, QAbstractItemModel *, const QStyleOptionViewItem , const QModelIndex )
    {
        if(event->type()== QEvent::KeyPress)
        {
            QKeyEvent *KeyEvent = static_cast<QKeyEvent*>(event);

            if (KeyEvent->key() == Qt::Key_F2)
            {
                return false;
            }else
                return true; // если не F2 то не редактируем
        }

        if(event->type() != QEvent::MouseButtonDblClick){ //  не двойной клик мышкой
            return false;  // если не двойной клик то выполняем то что положено на 1 клик
        }

        return true;
    }

    // создаем виджет редактора значения
QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(index.column() == 2 || index.column() == 4) // Если хотим изменить поле уровня доступа
    {
        QComboBox *editor = new QComboBox(parent);
        return editor;
    }

  return QItemDelegate::createEditor(parent,option,index);
}

    //передаем значение от модели в редактор
void setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if(index.column() == 2)
    {
        QComboBox *dostup = static_cast<QComboBox*>(editor);
        for(int i=0; i<strKod_delegate.size(); i++)
        {
            dostup->addItem(strKod_delegate.value(i));
        }
        dostup->setCurrentText(index.model()->data(index,Qt::DisplayRole).toString());
        return;
    }
    if(index.column()== 1 || index.column() == 3)
    {
        QLineEdit *name = static_cast<QLineEdit*>(editor);
        name->setText(index.model()->data(index,Qt::DisplayRole).toString().trimmed());
        return;
    }
    if(index.column() == 4)
    {
        QComboBox *dostup = static_cast<QComboBox*>(editor);
        dostup->addItem("ДА");
        dostup->addItem("НЕТ");
        dostup->setCurrentText(index.model()->data(index,Qt::DisplayRole).toString());
        return;
    }

    return QItemDelegate::setEditorData(editor,index);
}
    //передает значение от редактора в модель
void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{

    if (index.column() == 1)
    {
        QLineEdit *name = static_cast<QLineEdit*>(editor);
        if(name->text().trimmed() == model->data(index).toString()){
            return;
        }
        QString id =  model->data(model->index(index.row(),0,QModelIndex())).toString();
        QSqlQuery query;
        query.prepare("SELECT * FROM users  WHERE name_domen = ?");
        query.addBindValue(name->text().trimmed());
        if(!query.exec())
        {
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        };
        if(query.first())
        {
            QMessageBox::information(parent_w, "Information", "Такое имя пользователя  существует \n измените имя пользователя!");
            return;
        }
        query.prepare("select set_config('auth.username', ?, false)");
        query.addBindValue(UserName);
        if(!query.exec()){
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return ;
        }
        query.prepare("UPDATE users SET name_domen = ? WHERE id = ?");
        query.addBindValue(name->text().trimmed());
        query.addBindValue(id);
        if(!query.exec())
        {
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        };

    }
    if (index.column() == 2)
    {

        QComboBox *dostup = static_cast<QComboBox*>(editor);
        if(dostup->currentText() == model->data(index).toString()){
            return;
        }
        QString id =  model->data(model->index(index.row(),0,QModelIndex())).toString();
        QSqlQuery query;
        query.prepare("select set_config('auth.username', ?, false)");
        query.addBindValue(UserName);
        if(!query.exec()){
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return ;
        }
        query.prepare("UPDATE users SET kod_access = ? WHERE id = ?");
        query.addBindValue(dostup->currentIndex());
        query.addBindValue(id);
        if(!query.exec())
        {
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        };
    }
    if (index.column() == 3)
    {
        QLineEdit *name = static_cast<QLineEdit*>(editor);
        if(name->text().trimmed() == model->data(index).toString()){
            return;
        }
        QString id =  model->data(model->index(index.row(),0,QModelIndex())).toString();
        QSqlQuery query;
        query.prepare("select set_config('auth.username', ?, false)");
        query.addBindValue(UserName);
        if(!query.exec()){
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return ;
        }
        query.prepare("UPDATE users SET fio = ? WHERE id = ?");
        query.addBindValue(name->text().trimmed());
        query.addBindValue(id);
        if(!query.exec())
        {
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        };
    }
    if (index.column() == 4)
    {
        QComboBox *dostup = static_cast<QComboBox*>(editor);
        if(dostup->currentText().trimmed() == model->data(index).toString().trimmed()){
            return;
        }
        QString id =  model->data(model->index(index.row(),0,QModelIndex())).toString();
        QSqlQuery query;
        query.prepare("select set_config('auth.username', ?, false)");
        query.addBindValue(UserName);
        if(!query.exec()){
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return ;
        }
        query.prepare("UPDATE users SET active = ? WHERE id = ?");
        query.addBindValue(dostup->currentIndex()==0 ? true : false);
        query.addBindValue(id);
        if(!query.exec())
        {
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        };
    }
}



signals:
public slots:
private slots:

    void reciveData_NameKodAccess_delegate(const QStringList str);

private:
    QStringList strKod_delegate;


};

namespace Ui {
class users;
}

class users : public QDialog
{
    Q_OBJECT

public:
    explicit users(QWidget *parent );
    ~users();

public slots:

    void reciveData_users();

private slots:

    void on_AppendUser_clicked();
    void refresh(int reg);
    void user_refresh();

    void on_deleteUser_clicked();

signals:

    void sendNameKodaccess(const QStringList);


private:
    Ui::users *ui;
    my_users_SqlQueryModel *model_users;

};

#endif // USERS_H
