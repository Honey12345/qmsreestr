#include "complex_add_new_service.h"
#include "ui_complex_add_new_service.h"
#include "global.h"

complex_add_new_service::complex_add_new_service(QWidget *parent, int idcomplex, int regim) :
    QDialog(parent),
    ui(new Ui::complex_add_new_service)
{
    rEgim = regim;
    iDcomplex = idcomplex;
    ui->setupUi(this);
    if(settings->value("complex_add_new_service").isValid()){
        setGeometry(settings->value("complex_add_new_service").toRect());
    }
    SetStyleGroupBox();

    model_econom = new QSqlQueryModel;
    model_price = new QSqlQueryModel;
    SetModelPrice();
    SetModelEconom();
//    ui->tableView_sprav_econom->selectRow(0);
//    ui->tableView_sprav_price->selectRow(0);
    ui->pushButton->setFocus();
    ui->lineEdit_nameusl->setStyleSheet("QLineEdit { color: blue ; background-color: lightGray }");
    ui->lineEdit_price->setStyleSheet("QLineEdit { color: blue ; background-color: lightGray }");
    if(rEgim == Regim::AddServiceShablon){
        this->setWindowTitle("Добавление услуги в комплекс (шаблон)" );
        connect(ui->pushButton,&QPushButton::clicked,this,&complex_add_new_service::AddService);
        ui->pushButton->setText("Добавить в Шаблон");
        ui->lineEdit_code_service->setVisible(false);
        ui->label->setVisible(false);
    }
    else if(rEgim == Regim::AddServiceComplex){
        this->setWindowTitle("Добавление услуги в новый комплекс" );
        connect(ui->pushButton,&QPushButton::clicked,this,&complex_add_new_service::AddServiceInComplex);
        ui->pushButton->setText("Добавить в Комплекс");
        ui->lineEdit_code_service->setVisible(false);
        ui->label->setVisible(false);
    }
    else if(rEgim == Regim::AddServiceDogovor){
        QSqlQuery query;
        query.prepare("select * from dogovor_complex");
        this->setWindowTitle("Добавление услуги в договор" );
        connect(ui->pushButton,&QPushButton::clicked,this,&complex_add_new_service::AddServiceInDogovor);
        ui->pushButton->setText("Добавить в Договор");
    }

    connect(ui->tableView_sprav_econom,&QTableView::doubleClicked,this,&complex_add_new_service::TableView_econom_DoubleClicked);
    connect(ui->tableView_sprav_price,&QTableView::doubleClicked,this,&complex_add_new_service::TableView_price_DoubleClicked);

    connect(ui->tableView_sprav_price->horizontalHeader(),&QHeaderView::sectionDoubleClicked,this,&complex_add_new_service::HeaderDoubleClickedPrice);
    connect(ui->tableView_sprav_econom->horizontalHeader(),&QHeaderView::sectionDoubleClicked,this,&complex_add_new_service::HeaderDoubleClickedUsl);
    // создаем объект QLineEdit, его родителем делаем QHeaderView из нашего QTableView, в результате наш объект
    // QLineEdit будет выводиться в РОДИТЕЛЕ ! Что нам и нужно, определяя какая колонка по индексу выбрана и смещая width
    // geometry(width,0,200,25) объекта QlineElbn на ширину колонок, можно выводить в нужных колонках.

    findeKod = new QLineEdit(ui->tableView_sprav_price->horizontalHeader());
    findeKod->setText("");
    findeKod->close();
    connect(findeKod,&QLineEdit::editingFinished,findeKod,&QLineEdit::close);
    connect(findeKod,&QLineEdit::textChanged,this,&complex_add_new_service::FindeKod);
    findeNamePrice = new QLineEdit(ui->tableView_sprav_price->horizontalHeader());
    findeNamePrice->setText("");
    findeNamePrice->close();
    connect(findeNamePrice,&QLineEdit::editingFinished,findeNamePrice,&QLineEdit::close);
    connect(findeNamePrice,&QLineEdit::textChanged,this,&complex_add_new_service::FindeNamePrice);
    findeNameUsl = new QLineEdit(ui->tableView_sprav_econom->horizontalHeader());
    findeNameUsl->setText("");
    findeNameUsl->close();
    connect(findeNameUsl,&QLineEdit::editingFinished,findeNameUsl,&QLineEdit::close);
    connect(findeNameUsl,&QLineEdit::textChanged,this,&complex_add_new_service::FindeNameUsl);
}

complex_add_new_service::~complex_add_new_service()
{
    settings->setValue("complex_add_new_service", geometry());
    delete findeNamePrice;
    delete findeNameUsl;
    delete findeKod;
    delete model_econom;
    delete model_price;
    delete ui;
}

void complex_add_new_service::SetStyleGroupBox()
{
    ui->groupBox->setTitle("Добавляемая услуга в комплекс");
    ui->groupBox->setStyleSheet("QGroupBox {"
                                "border: 2px solid red;"
                                "border-radius: 9px;"
                                "border-color: red;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: red"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_2->setTitle("Услуги ПЭО");
    ui->groupBox_2->setStyleSheet("QGroupBox {"
                                "border: 2px solid blue;"
                                "border-radius: 9px;"
                                "border-color: blue;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: blue"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_3->setTitle("Услуги по прайсу");
    ui->groupBox_3->setStyleSheet("QGroupBox {"
                                "border: 2px solid green;"
                                "border-radius: 9px;"
                                "border-color: green;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: green"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

}

void complex_add_new_service::SetModelPrice()
{
    QSqlQuery query;
    query.prepare("select * from qmspriceusl where closedate isnull order by code_price");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    model_price->setQuery(query);
    model_price->setHeaderData(static_cast<int>(ComplexAddService::price::code_price),Qt::Horizontal,"Код прайса");
    model_price->setHeaderData(static_cast<int>(ComplexAddService::price::name_price),Qt::Horizontal,"Наименование");
    ui->tableView_sprav_price->setModel(model_price);
    ui->tableView_sprav_price->setColumnWidth(static_cast<int>(ComplexAddService::price::code_price),150);
    ui->tableView_sprav_price->horizontalHeader()->setStretchLastSection(true);
    ui->tableView_sprav_price->setColumnHidden(static_cast<int>(ComplexAddService::price::id),true);
    ui->tableView_sprav_price->setColumnHidden(static_cast<int>(ComplexAddService::price::closedata),true);

}

void complex_add_new_service::SetModelEconom()
{
    QSqlQuery query;
    query.prepare("select * from econom_usl order by name_usl");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    model_econom->setQuery(query);
    model_econom->setHeaderData(static_cast<int>(ComplexAddService::econom::name_usl),Qt::Horizontal," Наименование услуги ");
    ui->tableView_sprav_econom->setModel(model_econom);
    ui->tableView_sprav_econom->setColumnHidden(static_cast<int>(ComplexAddService::econom::id),true);
    ui->tableView_sprav_econom->horizontalHeader()->setStretchLastSection(true);
    return;
}

void complex_add_new_service::AddService()
{

    if(ideconom == 0 || idprice ==0){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Выберите добавляемую связку услуги с прайсом!</p>");
        return;
    }

    QSqlQuery query;
    query.prepare("select * from complex_services where idcomplex = ? and ideconom = ? and idprice = ?");
    query.addBindValue(iDcomplex);
    query.addBindValue(ideconom);
    query.addBindValue(idprice);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(query.first()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Такая услуга связанная с прайсом<br>есть в этом комплексе!</p>");
        return;
    }
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
        db.rollback();
        return;
    }
    query.prepare("insert into complex_services(idcomplex,ideconom,idprice) values(?,?,?)");
    query.addBindValue(iDcomplex);
    query.addBindValue(ideconom);
    query.addBindValue(idprice);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");
        return;
    }
    emit addServiceInShablon();
    ui->tableView_sprav_econom->setRowHidden(ui->tableView_sprav_econom->currentIndex().row(),true);
    ui->tableView_sprav_price->setRowHidden(ui->tableView_sprav_price->currentIndex().row(),true);
    ideconom = 0;
    idprice =0;
    ui->lineEdit_price->clear();
    ui->lineEdit_nameusl->clear();

}

void complex_add_new_service::AddServiceInComplex()
{
    if(ideconom == 0 || idprice ==0){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Выберите добавляемую связку услуги с прайсом!</p>");
        return;
    }
    emit addServiceInComplex(ideconom,idprice);
    ui->lineEdit_nameusl->clear();
    ui->lineEdit_price->clear();
    ideconom = 0;
    idprice = 0;
}

void complex_add_new_service::AddServiceInDogovor()
{
    if(ideconom == 0 || idprice ==0){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Выберите добавляемую связку услуги с прайсом!</p>");
        return;
    }
    if(ui->lineEdit_code_service->text().isEmpty()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Нет кода услуги!</p>");
        return;
    }
    QSqlQuery query;
    query.prepare("select * from dogovor_complex_services where idcomplex = ? and code_service = ? ");
    query.addBindValue(iDcomplex);
    query.addBindValue(ui->lineEdit_code_service->text().trimmed());
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(query.first()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Такой код услуги есть в этом комплексе договора</p>");
        return;
    }
    query.prepare("select * from dogovor_complex_services where idcomplex = ? and idprice = ? ");
    query.addBindValue(iDcomplex);
    query.addBindValue(idprice);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(query.first()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Такая услуг по прайсу есть в этом комплексе  договора!</p>");
        return;
    }
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
        return;
    }
    query.prepare("insert into dogovor_complex_services (idcomplex, ideconom, idprice, code_service) values(?,?,?,?) returning idservicecomplex");
    query.addBindValue(iDcomplex);
    query.addBindValue(ideconom);
    query.addBindValue(idprice);
    query.addBindValue(ui->lineEdit_code_service->text().trimmed());
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.first();
    emit addServiceInDogovor(query.value("idservicecomplex").toInt());
    ui->lineEdit_code_service->clear();
    ui->lineEdit_nameusl->clear();
    ui->lineEdit_price->clear();
    ideconom = 0;
    idprice = 0;

}

void complex_add_new_service::TableView_price_DoubleClicked(QModelIndex index)
{
    QString code_price = model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAddService::price::code_price))).toString();
    QString name_price = model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAddService::price::name_price))).toString();
    idprice = model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAddService::price::id))).toInt();
    ui->lineEdit_price->setText(code_price + " " + name_price);
}

void complex_add_new_service::TableView_econom_DoubleClicked(QModelIndex index)
{
    QString name_usl = model_econom->data(model_econom->index(index.row(),static_cast<int>(ComplexAddService::econom::name_usl))).toString();
    ideconom = model_econom->data(model_econom->index(index.row(),static_cast<int>(ComplexAddService::econom::id))).toInt();
    ui->lineEdit_nameusl->setText(name_usl);
}

void complex_add_new_service::FindeKod(QString str)
{
    if(str.length() < 2){
        return;
    }
    str+="*";
    QModelIndexList IndexList4 = model_price->match(model_price->index(0,static_cast<int>(ComplexAddService::price::code_price)),Qt::DisplayRole,str,1,Qt::MatchWildcard);
    if(IndexList4.length() == 0){
        return;
    }
    ui->tableView_sprav_price->selectRow(IndexList4.at(0).row());
}

void complex_add_new_service::FindeNamePrice(QString str)
{
    if(str.length() < 4){
        return;
    }
    str = "*" + str + "*";
    QModelIndexList IndexList4 = model_price->match(model_price->index(0,static_cast<int>(ComplexAddService::price::name_price)),Qt::DisplayRole,str,1,Qt::MatchWildcard);
    if(IndexList4.length() == 0){
        return;
    }
    ui->tableView_sprav_price->selectRow( IndexList4.at(0).row());
}

void complex_add_new_service::FindeNameUsl(QString str)
{
    if(str.length() < 4){
        return;
    }
    str = "*" + str + "*";
    QModelIndexList IndexList4 = model_econom->match(model_econom->index(0,static_cast<int>(ComplexAddService::econom::name_usl)),Qt::DisplayRole,str,1,Qt::MatchWildcard);
    if(IndexList4.length() == 0){
        return;
    }
    ui->tableView_sprav_econom->selectRow( IndexList4.at(0).row());
}

void complex_add_new_service::HeaderDoubleClickedPrice(int index)
{
    if(index == static_cast<int>(ComplexAddService::price::code_price)){
        ui->tableView_sprav_price->setSelectionBehavior(QAbstractItemView::SelectItems);
        ui->tableView_sprav_price->selectColumn(static_cast<int>(ComplexAddService::price::code_price));
        int widthColumn_curr = ui->tableView_sprav_price->columnWidth(index);
        int widthColumn_prev = ui->tableView_sprav_price->columnWidth(index-1);
        QRect lineRect (0 + widthColumn_prev,0,widthColumn_curr,25);
        findeKod->setGeometry(lineRect);
        findeKod->show();
        findeKod->setFocus();
        ui->tableView_sprav_price->setSelectionBehavior(QAbstractItemView::SelectRows);
    }
    else if(index == static_cast<int>(ComplexAddService::price::name_price)){
        ui->tableView_sprav_price->setSelectionBehavior(QAbstractItemView::SelectItems);
        ui->tableView_sprav_price->selectColumn(static_cast<int>(ComplexAddService::price::name_price));
        int widthColumn_curr = ui->tableView_sprav_price->columnWidth(index);
        int widthColumn_prev = ui->tableView_sprav_price->columnWidth(index-1);
        QRect lineRect (0 + widthColumn_prev,0,widthColumn_curr,25);
        findeNamePrice->setGeometry(lineRect);
        findeNamePrice->show();
        findeNamePrice->setFocus();
        ui->tableView_sprav_price->setSelectionBehavior(QAbstractItemView::SelectRows);
    }
}

void complex_add_new_service::HeaderDoubleClickedUsl(int index)
{
    if(index == static_cast<int>(ComplexAddService::econom::name_usl)){
        ui->tableView_sprav_econom->setSelectionBehavior(QAbstractItemView::SelectItems);
        ui->tableView_sprav_econom->selectColumn(static_cast<int>(ComplexAddService::econom::name_usl));
        int widthColumn_curr = ui->tableView_sprav_econom->columnWidth(index);
        int widthColumn_prev = ui->tableView_sprav_econom->columnWidth(index-1);
        QRect lineRect (0 + widthColumn_prev,0,widthColumn_curr,25);
        findeNameUsl->setGeometry(lineRect);
        findeNameUsl->show();
        findeNameUsl->setFocus();
        ui->tableView_sprav_price->setSelectionBehavior(QAbstractItemView::SelectRows);
    }
}

void complex_add_new_service::NotAdded()
{
    QMessageBox::critical(this,"Ошибка",
                    "<p style='text-align: center'>Такая услуга есть в этом договоре!</p>");
}
