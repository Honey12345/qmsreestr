#ifndef SPLASHWINDOWS_H
#define SPLASHWINDOWS_H

#include <QDialog>
#include <QSplashScreen>
#include <QMovie>
#include <QString>
#include <QTimer>
#include <QTime>

namespace Ui {
class splashWindows;
}

class splashWindows : public QDialog
{
    Q_OBJECT

public:
    explicit splashWindows(QWidget *parent = nullptr, QString text = QString());
    ~splashWindows();
    bool splashWindowsSqlExec();
    void setText(QString);

public slots:

    void splashWindowsEnd(bool);

private slots:

   void startSplash();
   void slotTimerAlarm();

private:
    Ui::splashWindows *ui;
    QSplashScreen *splash;
    QMovie *m_movie;
    QTimer *timer;
    QTime *elaps;
    bool sqlexec;
};

#endif // SPLASHWINDOWS_H
