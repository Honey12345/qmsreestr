#include "sesquery.h"

SesQuery::SesQuery()
{

}

SesQuery::~SesQuery()
{
    stopThreads();  /* останавливаем и удаляем потоки  при окончании работы сессии */
}

void SesQuery::startQuery(QSqlQuery *query)
{
    stopThreads();
    addThread(query);
}

void SesQuery::addThread(QSqlQuery *query)
{
    ThreadQuery* worker = new ThreadQuery(query);
    QThread* thread = new QThread;
    worker->moveToThread(thread);

/*  Теперь внимательно следите за руками.  Раз: */
    connect(thread, SIGNAL(started()), worker, SLOT(process()));
/* … и при запуске потока будет вызван метод process(), который создаст
 *  построитель отчетов, который будет работать в новом потоке

Два: */
    connect(worker, SIGNAL(finish(bool)), thread, SLOT(quit()));
/* … и при завершении работы построителя отчетов, обертка построителя передаст потоку сигнал finished()
 *  , вызвав срабатывание слота quit()

Три:
*/
    connect(this, SIGNAL(stopAll()), worker, SLOT(stop()));
/* … и Session может отправить сигнал о срочном завершении работы обертке построителя,
 *  а она уже остановит построитель и направит сигнал finished() потоку

Четыре: */
    connect(worker, SIGNAL(finish(bool)), worker, SLOT(deleteLater()));
/* … и обертка пометит себя для удаления при окончании построения отчета

Пять: */
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
/* … и поток пометит себя для удаления, по окончании построения отчета.
 *  Удаление будет произведено только после полной остановки потока.
*/

 connect(worker,SIGNAL(finish(bool)),this,SLOT(ended(bool)));

/*
И наконец :
*/
    thread->start();
/* Запускаем поток, он запускает RBWorker::process(), который создает ReportBuilder и запускает  построение отчета */

    return ;

}

void SesQuery::stopThreads()
{
    emit  stopAll();
/* каждый RBWorker получит сигнал остановиться,
 *  остановит свой построитель отчетов и вызовет слот quit() своего потока */
}

void SesQuery::ended(bool ok)
{
    emit endquery(ok);
}


ThreadQuery::ThreadQuery(QSqlQuery *query)
{
    q = query;
}

ThreadQuery::~ThreadQuery()
{
}

void ThreadQuery::process()
{
    bool ok = false;
    ok = q->exec();
    emit finish(ok);
    emit finished();
}

void ThreadQuery::stop()
{
}
