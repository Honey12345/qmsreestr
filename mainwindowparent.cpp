#include "mainwindowparent.h"
#include "ui_mainwindowparent.h"
#include "global.h"
#include "enums.h"
#include "users.h"
#include "mainwindow.h"
#include "price_normalization.h"
#include "mainwindowovdpo.h"

MainWindowParent::MainWindowParent(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowParent)
{
    ui->setupUi(this);

    settings = new QSettings("qMSReestr","SettingsTutorial", this);

    if(settings->value("MainWindowParent").isValid()){
        setGeometry(settings->value("MainWindowParent").toRect());
    }
    setWindowTitle("ПЭО.Нормализация : " + displayName );
    ui->toolBar->setMovable(false);

    switch (UserRole) {
    case static_cast<int>((Enums::Access::EconomistRole)):
        ui->menu_setup->setEnabled(false);
        ui->action_price->setEnabled(false);
        ui->action_ovdpo->setEnabled(false);
        ui->action_users->setEnabled(false);
        break;
    case static_cast<int>((Enums::Access::SisterOVDPO)):
        ui->menu_setup->setEnabled(false);
        ui->action_price->setEnabled(false);
        ui->action_users->setEnabled(false);
        ui->action_dogovora->setEnabled(false);
        break;
    case static_cast<int>((Enums::Access::AdminRole)):
        break;
    default:
        ui->menu_main->setEnabled(false);
        ui->menu_setup->setEnabled(false);
        ui->action_price->setEnabled(false);
        ui->action_ovdpo->setEnabled(false);
        ui->action_users->setEnabled(false);
        ui->action_dogovora->setEnabled(false);
        break;
    }

    connect(ui->action_about,&QAction::triggered,this,&MainWindowParent::About);
    connect(ui->action_users,&QAction::triggered,this,&MainWindowParent::userSpravochnik);

    connect(ui->action_dogovora,&QAction::triggered,this,&MainWindowParent::MainWindowsDogovora);
    connect(ui->action_price,&QAction::triggered,this,&MainWindowParent::priceNormalization);
    connect(ui->action_ovdpo,&QAction::triggered,this,&MainWindowParent::MainWindowProfOsmotr);

    QImage image;
    image.load(":/icons/viveyalogo.png");
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->addPixmap(QPixmap::fromImage(image));
    scene->setSceneRect(image.rect());
    ui->graphicsView->setScene(scene);

}

MainWindowParent::~MainWindowParent()
{
    settings->setValue("MainWindowParent", geometry());
    delete ui;
}

void MainWindowParent::About()
{
    QMessageBox::about(this, "О программе", "Версия программы : " + ver);
}

void MainWindowParent::userSpravochnik()
{
    users *d = new users(this);
    d->exec();
    delete d;
}

void MainWindowParent::MainWindowsDogovora()
{
    MainWindow *w = new MainWindow;
    w->show();
    connect(w,&MainWindow::exit,w,&MainWindow::deleteLater);
}

void MainWindowParent::priceNormalization()
{
    price_normalization *d = new price_normalization(this);
    d->exec();
    delete d;
}

void MainWindowParent::MainWindowProfOsmotr()
{
    MainWindowOVDPO *w = new MainWindowOVDPO;
    if(w->SetData()){
        w->show();
        connect(w,&MainWindowOVDPO::exit,w,&MainWindowOVDPO::deleteLater);
    }
    else{
        delete w;
    }
}
