#include "dogovor_set_code.h"
#include "ui_dogovor_set_code.h"


dogovor_set_code::dogovor_set_code(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dogovor_set_code)
{
    ui->setupUi(this);

    setWindowTitle("Установка кодов");

    ui->lineEdit_separate->setValidator(new QRegExpValidator( QRegExp( "[.-]" ) ));


//    connect(ui->lineEdit_const,&QLineEdit::cursorPositionChanged,this,&dogovor_set_code::SetCursorPosition);
//    connect(ui->lineEdit_count,&QLineEdit::cursorPositionChanged,this,&dogovor_set_code::SetCursorPosition);
//    connect(ui->lineEdit_const,&QLineEdit::textEdited,this,&dogovor_set_code::SetCursorPosition);
//    connect(ui->lineEdit_count,&QLineEdit::textEdited,this,&dogovor_set_code::SetCursorPosition);

    connect(ui->lineEdit_const,&QLineEdit::textChanged,this,&dogovor_set_code::SetCursorPosition);
    connect(ui->lineEdit_count,&QLineEdit::textChanged,this,&dogovor_set_code::SetCursorPosition);



    connect(ui->pushButton_cancel,&QPushButton::clicked,this,&dogovor_set_code::reject);
    connect(ui->pushButton_set,&QPushButton::clicked,this,&dogovor_set_code::SetCode);

    ui->lineEdit_const->setCursorPosition(0);
    ui->lineEdit_const->setFocus();
}

dogovor_set_code::~dogovor_set_code()
{
    delete ui;
}

QString dogovor_set_code::GetCode()
{
    return code;
}

void dogovor_set_code::SetCursorPosition()
{
    QLineEdit *lineEditCurrent = static_cast<QLineEdit*>(sender());
    if(lineEditCurrent->text().isEmpty()){
        lineEditCurrent->setCursorPosition(0);
    }
}


void dogovor_set_code::SetCode()
{
    code = "";
    if( ui->lineEdit_const->text().isEmpty() && !ui->lineEdit_count->text().isEmpty()){
        code = ui->lineEdit_count->text();
    }else if (!ui->lineEdit_const->text().isEmpty() && !ui->lineEdit_count->text().isEmpty() && !ui->lineEdit_separate->text().isEmpty()){
        code = ui->lineEdit_const->text().trimmed() + ui->lineEdit_separate->text() + ui->lineEdit_count->text().trimmed();
    }
    else{
        return;
    }
    accept();
}
