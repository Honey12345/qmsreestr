#include "global.h"
#include "complex_edit.h"
#include "ui_complex_edit.h"

complex_edit::complex_edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::complex_edit)
{
    ui->setupUi(this);
    connect(ui->pushButton_cancel,&QPushButton::clicked,this,&complex_edit::reject);
    connect(ui->pushButton_save,&QPushButton::clicked,this,&complex_edit::SaveData);

    connect(ui->lineEdit_code_complex,&QLineEdit::textChanged,this,&complex_edit::ChangeDataComplex);
    connect(ui->lineEdit_name_complex,&QLineEdit::textChanged,this,&complex_edit::ChangeDataComplex);
    connect(ui->doubleSpinBox_stoimost,QOverload<double>::of(&QDoubleSpinBox::valueChanged),this,&complex_edit::ChangeDataComplex);

    ui->pushButton_save->setEnabled(false);
    ui->lineEdit_codecomplexqms->setStyleSheet("QLineEdit { background: rgb(225, 225, 225); }");
}

complex_edit::~complex_edit()
{
    delete ui;
}

bool complex_edit::SetComplexData(int id_complex)
{
    QSqlQuery query;
    query.prepare(" select * from dogovor_complex "
                  " left join dogovor using(iddogovor)"
                  " where idcomplex = ?");
    query.addBindValue(id_complex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    query.first();
    idcomplex = id_complex;
    codecomplex = query.value("codecomplex").toString();
    namecomplex = query.value("namecomplex").toString();
    stoimost = query.value("stoimost").toDouble();
    ui->lineEdit_code_complex->setText(codecomplex);
    ui->lineEdit_name_complex->setText(namecomplex);
    ui->doubleSpinBox_stoimost->setValue(stoimost);
    ui->lineEdit_codecomplexqms->setText(query.value("codecomplexqms").toString());

    this->setWindowTitle("Договор № " + query.value("numberdogovor").toString() + " " + query.value("namekontragent").toString());

    return true;
}

void complex_edit::ChangeDataComplex()
{
    if(ui->lineEdit_code_complex->text() != codecomplex || ui->lineEdit_name_complex->text() != namecomplex || (stoimost - ui->doubleSpinBox_stoimost->value() != 0)){
        ui->pushButton_save->setEnabled(true);
        return;
    }
    ui->pushButton_save->setEnabled(false);
}

void complex_edit::SaveData()
{
    QSqlQuery query;
    query.prepare("select * from dogovor"
                  " left join dogovor_complex using(iddogovor)"
                  " where (codecomplex = ? or namecomplex = ?) and idcomplex != ? and approved isnull and changed isnull and closed isnull");
    query.addBindValue(ui->lineEdit_code_complex->text().trimmed());
    query.addBindValue(ui->lineEdit_name_complex->text().trimmed());
    query.addBindValue(idcomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(query.first())
    {
        QMessageBox::information(this, "Information", "Такое код или название комплекса есть в этом договоре !\n измените данные комплекса!");
        ui->pushButton_save->setEnabled(false);
        return ;
    }
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.prepare("update dogovor_complex set codecomplex = ?, namecomplex = ?, stoimost = ? where idcomplex = ?");
    query.addBindValue(ui->lineEdit_code_complex->text().trimmed());
    query.addBindValue(ui->lineEdit_name_complex->text().trimmed());
    query.addBindValue(ui->doubleSpinBox_stoimost->value());
    query.addBindValue(idcomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }

    return accept();
}
