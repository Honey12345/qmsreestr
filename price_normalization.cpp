#include "price_normalization.h"
#include "ui_price_normalization.h"
#include "global.h"
#include <qtxlsx/xlsx/xlsxdocument.h>

price_normalization::price_normalization(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::price_normalization)
{
    ui->setupUi(this);

    if(settings->value("price_normalization").isValid()){
        setGeometry(settings->value("price_normalization").toRect());
    }

    setWindowState(Qt::WindowNoState | Qt::WindowActive);
    ui->pushButton_price_peo->setEnabled(false);
    ui->pushButton_toExcel->setEnabled(false);
    model = new price_normalization_QStandardItemModel;
    SetModel();

    connect(ui->pushButton_price_qms,&QPushButton::clicked,this,&price_normalization::loadPrice_qMS);
    connect(ui->pushButton_price_peo,&QPushButton::clicked,this,&price_normalization::loadPrice_PEO);
    connect(ui->pushButton_toExcel,&QPushButton::clicked,this,&price_normalization::uploadToExcel);



}

price_normalization::~price_normalization()
{
    settings->setValue("price_normalization", geometry());
    delete model;
    delete ui;
}

void price_normalization::SetModel()
{
    model->setColumnCount(8);
    model->setHeaderData(static_cast<int>(priceNormalization::PriceNorm::kodNew),Qt::Horizontal,"Код Услуги\nNew");
    model->setHeaderData(static_cast<int>(priceNormalization::PriceNorm::nameNew),Qt::Horizontal,"Наименование\nNew");
    model->setHeaderData(static_cast<int>(priceNormalization::PriceNorm::stoimostNew),Qt::Horizontal,"Стоимость\nNew");
    model->setHeaderData(static_cast<int>(priceNormalization::PriceNorm::kodqMS),Qt::Horizontal,"Код Услуги\nqMS");
    model->setHeaderData(static_cast<int>(priceNormalization::PriceNorm::nameqMS),Qt::Horizontal,"Наименование\nqMS");
    model->setHeaderData(static_cast<int>(priceNormalization::PriceNorm::stoimostqMS),Qt::Horizontal,"Стоимость\nqMS");
    model->setHeaderData(static_cast<int>(priceNormalization::PriceNorm::nameRazdel),Qt::Horizontal,"Раздел");

    ui->tableView->setModel(model);

    ui->tableView->setColumnWidth(static_cast<int>(priceNormalization::PriceNorm::kodNew),80);
    ui->tableView->setColumnWidth(static_cast<int>(priceNormalization::PriceNorm::nameNew),500);
    ui->tableView->setColumnWidth(static_cast<int>(priceNormalization::PriceNorm::stoimostNew),150);
    ui->tableView->setColumnWidth(static_cast<int>(priceNormalization::PriceNorm::kodqMS),80);
    ui->tableView->setColumnWidth(static_cast<int>(priceNormalization::PriceNorm::nameqMS),500);
    ui->tableView->setColumnWidth(static_cast<int>(priceNormalization::PriceNorm::stoimostqMS),150);
    ui->tableView->setColumnWidth(static_cast<int>(priceNormalization::PriceNorm::nameRazdel),200);

    ui->tableView->setColumnHidden(static_cast<int>(priceNormalization::PriceNorm::flag),true);

    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(priceNormalization::PriceNorm::kodNew),QHeaderView::Fixed);
    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(priceNormalization::PriceNorm::kodqMS),QHeaderView::Fixed);
    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(priceNormalization::PriceNorm::stoimostNew),QHeaderView::Fixed);
    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(priceNormalization::PriceNorm::stoimostqMS),QHeaderView::Fixed);
//    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(priceNormalization::PriceNorm::nameNew),QHeaderView::ResizeToContents);
//    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(priceNormalization::PriceNorm::nameqMS),QHeaderView::ResizeToContents);
    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(priceNormalization::PriceNorm::nameRazdel),QHeaderView::Stretch);

    ui->tableView->setSortingEnabled(true);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
}

void price_normalization::SetListRazdel(QString razdel)
{
    if(!list_razdel.contains(razdel)){
        list_razdel.append(razdel);
    }
}

void price_normalization::FindCompareInModel(QString kod, QString kod_price, QString name_price, QString stoimost_price)
{
    QString flags;
    QModelIndexList IndexList = model->match(model->index(0,static_cast<int>(priceNormalization::PriceNorm::kodqMS)),Qt::DisplayRole,kod,1,Qt::MatchFixedString);
    if(IndexList.isEmpty()){
        flags = "NewRecord";
        QList<QStandardItem*> list_items;
        list_items.append(new QStandardItem(kod));
        list_items.append(new QStandardItem(name_price));
        list_items.append(new QStandardItem(QString::number(stoimost_price.toDouble(),'f',2)));
        list_items.append(new QStandardItem(QString()));
        list_items.append(new QStandardItem(QString()));
        list_items.append(new QStandardItem(QString()));
        list_items.append(new QStandardItem(QString()));
        list_items.append(new QStandardItem(flags));
        model->appendRow(list_items);
        return;
    }
    QModelIndex index = IndexList.at(0);
    if(kod_price.trimmed() != model->data(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::kodqMS)))){
        flags = flags.isEmpty() ? "KodNoEqually" : flags + "|" +"KodNoEqually";
    }
    if(name_price.trimmed() != model->data(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::nameqMS)))){
        flags = flags.isEmpty() ? "NameNoEqually" : flags + "|" +"NameNoEqually";
    }
    if((stoimost_price.toDouble() - model->data(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::stoimostqMS))).toDouble()) < 0){
        flags = flags.isEmpty() ? "StoimostLow" : flags + "|" +"StoimostLow";
    }
    if((stoimost_price.toDouble() - model->data(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::stoimostqMS))).toDouble()) > 0){
        flags = flags.isEmpty() ? "StoimostUp" : flags + "|" +"StoimostUp";
    }
    if((stoimost_price.toDouble() - model->data(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::stoimostqMS))).toDouble()) == 0){
        flags = flags.isEmpty() ? "StoimostEqually" : flags + "|" +"StoimostEqually";
    }
    if(flags.isEmpty()){
        flags = "Equally";
    }
    model->setData(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::kodNew)),kod_price);
    model->setData(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::nameNew)),name_price);
    model->setData(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::stoimostNew)),QString::number(stoimost_price.toDouble(),'f',2));
    model->setData(model->index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag)),flags);
}

QString price_normalization::CheckFileCodec(QString file_name)
{
    QFile xmlFile(file_name);
    if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this,"Ошибка загрузки XML файла !",
                              "Ошибка открытия файла " + file_name + " для загрузки.",
                              QMessageBox::Ok);
        return QString();
    }
    QByteArray byteArray = xmlFile.readAll();
    xmlFile.close();
//    QTextCodec *codec = QTextCodec::codecForName("KOI8-R");
//    QTextCodec *codec = QTextCodec::codecForName("KOI8-R");
//    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");

    QString string = codec->toUnicode(byteArray);
//    string.replace("utf-8","windows-1251");
    return string;
}

void price_normalization::loadPrice_qMS()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Выбор файла","c:\\TMP\\","Документы (*.xml)");
    if(file_name.isEmpty()){
        return ;
    }

    QString data = CheckFileCodec(file_name);
    if(data.isEmpty()){
        return;
    }

    // Получение количества тегов в файле xml ********************************************************
    QXmlStreamReader xmlReader;
//    xmlReader.setDevice(&xmlFile);
    xmlReader.addData(data);
    int Count = 0;
    while(!xmlReader.atEnd()&& !xmlReader.hasError()){
        xmlReader.readNext();
        Count++;
    }

//    xmlFile.seek(0);
    xmlReader.clear();
//    xmlReader.setDevice(&xmlFile);
    xmlReader.addData(data);

    QProgressDialog *ppwt;
    ppwt = new QProgressDialog("Загрузка прайса...","Отмена",0,Count
                               ,this);
    ppwt->setWindowTitle("Wait...");
    ppwt->setMinimumDuration(0);
    ppwt->setValue(0);
    int n = 0;

    list_razdel.clear();
    model->clear();
    ui->tableView->setModel(nullptr);

    QString Duv,u,stoimost,razdel;
    QList<QStandardItem*> list_items;
    while(!xmlReader.atEnd() && !xmlReader.hasError()){
        n++;
        QXmlStreamReader::TokenType token = xmlReader.readNext();
        if(token == QXmlStreamReader::StartDocument || token != QXmlStreamReader::StartElement){
            continue;
        }
        if(token == QXmlStreamReader::StartElement && xmlReader.name() == "Opr"){
            Duv = "";
            u = "";
            stoimost = "";
            razdel = "";
            list_items.clear();
            do{
                xmlReader.readNext();
                n++;
                if( xmlReader.tokenType() == QXmlStreamReader::StartElement && xmlReader.name() == "Duv"){
                    Duv = xmlReader.readElementText();
                }
                if(xmlReader.tokenType() == QXmlStreamReader::StartElement && xmlReader.name() == "u"){
                    u = xmlReader.readElementText();
                }
                if(xmlReader.tokenType() == QXmlStreamReader::StartElement && xmlReader.name() == "oPr.Mr7"){
                    stoimost = xmlReader.readElementText();
                }
                if(xmlReader.tokenType() == QXmlStreamReader::StartElement && xmlReader.name() == "prrazoprra"){
                    razdel = xmlReader.readElementText();
                }
                if(xmlReader.tokenType() == QXmlStreamReader::EndElement && xmlReader.name() == "Opr"){
                    break;
                }
            }while(true);
            list_items.append(new QStandardItem(QString()));
            list_items.append(new QStandardItem(QString()));
            list_items.append(new QStandardItem(QString()));
            list_items.append(new QStandardItem(Duv));
            list_items.append(new QStandardItem(u));
            list_items.append(new QStandardItem(stoimost));
            list_items.append(new QStandardItem(razdel));
            list_items.append(new QStandardItem(QString()));
            model->appendRow(list_items);
            SetListRazdel(razdel);
            n++;
        }
        if(ppwt->wasCanceled())
        {
           break;
        }
        qApp->processEvents();
        ppwt->setValue(n++);
    }
    qApp->processEvents();
    ppwt->setValue(Count);
    delete ppwt;
    SetModel();
    ui->tableView->setModel(model);
    ui->pushButton_price_peo->setEnabled(true);
    ui->pushButton_price_peo->setFocus();
    return ;
}

void price_normalization::loadPrice_PEO()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Выбор файла","c:\\TMP\\","Документы (*.xlsx *.xls)");
    if(file_name.isEmpty()){
        return ;
    }

    ui->tableView->setModel(nullptr);

    QRegExp reg("(\\d)");
    QXlsx::Document xlsx(file_name);
    int row = 0;
    int rowEmpty = 0;

    QProgressDialog *ppwt;
    ppwt = new QProgressDialog("Загрузка прайса...","Отмена",0,model->rowCount()
                               ,this);
    ppwt->setWindowTitle("Wait...");
    ppwt->setMinimumDuration(0);
    ppwt->setValue(0);
    int n = 0;

    while(rowEmpty < 100) {
        row++;
        if(!xlsx.read(row,1).isValid()){
            rowEmpty++;
            continue;
        }
        else{
            rowEmpty = 0;
        }
        QString str = xlsx.read(row,1).toString().trimmed();
        if(reg.indexIn(str.mid(0,1)) != -1){    // анализируем первый символ кода - если цифра , то считаем что код.
            QString kod = "";
            for(int i = 0; i < str.length(); i++){  // убираем пробелы из кода
                if(str.mid(i,1) != " "){
                    kod +=str.mid(i,1);
                }
            }
            FindCompareInModel(kod, xlsx.read(row,1).toString(), xlsx.read(row,2).toString(), xlsx.read(row,3).toString());
            //qDebug() << kod << xlsx.read(row,1).toString() << xlsx.read(row,2).toString() << xlsx.read(row,3).toString();
            n++;
            qApp->processEvents();
            ppwt->setValue(n++);
        }
    }
    ui->pushButton_toExcel->setEnabled(true);

    SetModel();
    delete ppwt;
    //qDebug() << "end " << row;
}

void price_normalization::uploadToExcel()
{
    QVariant s;
    int rowoffset = 0;
    QXlsx::Document xlsx;
    QXlsx::Format formatHeader, formatLeft,formatRigth, formatCenter, formatMoney, formatData,formatNumber;
    QXlsx::Format formatLeftTextBold,formatRigthText;
    //QXlsx::CellRange range;

    QString filename,str;
    QString winpath;
    winpath = "/Downloads/";
    str = "Прайс_" ;

    filename = QDir::homePath() + winpath + str + "_" + QDateTime::currentDateTime().toString("ddMMyyyyHHmm") + ".xlsx";

    formatHeader.setFontColor(QColor(Qt::blue));
    formatHeader.setFontSize(16);
    formatHeader.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatHeader.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatHeader.setBorderStyle(QXlsx::Format::BorderThin);
    formatHeader.setTextWarp(true);

    formatLeft.setFontColor(QColor(Qt::black));
    formatLeft.setFontSize(12);
    formatLeft.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeft.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatLeft.setBorderStyle(QXlsx::Format::BorderThin);
    formatLeft.setTextWarp(true);

    formatRigth.setFontColor(QColor(Qt::black));
    formatRigth.setFontSize(12);
    formatRigth.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigth.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatRigth.setBorderStyle(QXlsx::Format::BorderThin);

    formatCenter.setFontColor(QColor(Qt::black));
    formatCenter.setFontSize(12);
    formatCenter.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatCenter.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatCenter.setBorderStyle(QXlsx::Format::BorderThin);
    formatCenter.setTextWarp(true);

    formatMoney.setFontColor(QColor(Qt::black));
    formatMoney.setFontSize(12);
    formatMoney.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatMoney.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatMoney.setBorderStyle(QXlsx::Format::BorderThin);
    formatMoney.setNumberFormatIndex(8);

    formatData.setFontColor(QColor(Qt::black));
    formatData.setFontSize(12);
    formatData.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatData.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatData.setBorderStyle(QXlsx::Format::BorderThin);
    formatData.setNumberFormat("dd.mm.yyyy");

    formatNumber.setFontColor(QColor(Qt::black));
    formatNumber.setFontSize(12);
    formatNumber.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatNumber.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatNumber.setBorderStyle(QXlsx::Format::BorderThin);
    formatNumber.setNumberFormatIndex(2);


    formatLeftTextBold.setFontColor(QColor(Qt::black));
    formatLeftTextBold.setFontName("Times New Roman");
    formatLeftTextBold.setFontSize(14);
    formatLeftTextBold.setFontBold(true);
    formatLeftTextBold.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeftTextBold.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    formatRigthText.setFontColor(QColor(Qt::black));
    formatRigthText.setFontSize(12);
    formatRigthText.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigthText.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    int col = 0;
    {
        s = QString("Код ПЭО");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Наименование ПЭО");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Стоимость ПЭО");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Код qMS");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Наименование qMS");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Стоимость qMS");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Раздел прайса");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
    }
    rowoffset++;
    for(int row = 0; row < model->rowCount(); row++){
        QString flags = model->data(model->index(row,static_cast<int>(priceNormalization::PriceNorm::flag))).toString();
        for(col = 0; col < model->columnCount()-1;col++){
            s = model->data(model->index(row,col)).toString();
            switch (col) {
            case static_cast<int>(priceNormalization::PriceNorm::kodNew):
                if(flags.contains("NewRecord")){
                    formatCenter.setFontColor(Qt::darkGreen);
                }
                else if (flags.contains("KodNoEqually")) {
                    formatCenter.setFontColor(Qt::red);
                }
                else if (flags.contains("Equally")) {
                    formatCenter.setFontColor(Qt::black);
                }
                xlsx.write(1+row+rowoffset,1+col,s,formatCenter);
                break;
            case static_cast<int>(priceNormalization::PriceNorm::nameNew):
                if(flags.contains("NewRecord")){
                    formatLeft.setFontColor(Qt::darkGreen);
                }
                else if (flags.contains("NameNoEqually")) {
                    formatLeft.setFontColor(Qt::red);
                }
                else if (flags.contains("Equally")) {
                    formatLeft.setFontColor(Qt::black);
                }
                xlsx.write(1+row+rowoffset,1+col,s,formatLeft);
                break;
            case static_cast<int>(priceNormalization::PriceNorm::stoimostNew):
                // = model->data(model->index(row,1+col)).toDouble();
                if(flags.contains("NewRecord")){
                    formatNumber.setFontColor(Qt::darkGreen);
                }
                else if (flags.contains("StoimostLow")||flags.contains("StoimostUp")) {
                    formatNumber.setFontColor(Qt::black);
                    formatNumber.setPatternBackgroundColor(Qt::red);
                }
                else if (flags.contains("Equally")) {
                    formatNumber.setFontColor(Qt::black);
                }
                xlsx.write(1+row+rowoffset,1+col,s,formatNumber);
                formatNumber.setPatternBackgroundColor(Qt::white);
                break;
            case static_cast<int>(priceNormalization::PriceNorm::kodqMS):
                if(flags.isEmpty()){
                    formatCenter.setFontColor(Qt::blue);
                }
                else{
                    formatCenter.setFontColor(Qt::black);
                }
                xlsx.write(1+row+rowoffset,1+col,s,formatCenter);
                break;
            case static_cast<int>(priceNormalization::PriceNorm::nameqMS):
                if(flags.isEmpty()){
                    formatLeft.setFontColor(Qt::blue);
                }
                else{
                    formatLeft.setFontColor(Qt::black);
                }
                xlsx.write(1+row+rowoffset,1+col,s,formatLeft);
                break;
            case static_cast<int>(priceNormalization::PriceNorm::stoimostqMS):
                //s = model->data(model->index(row,1+col)).toDouble();
                if(flags.isEmpty()){
                    formatNumber.setFontColor(Qt::blue);
                }
                else{
                    formatNumber.setFontColor(Qt::black);
                }
                xlsx.write(1+row+rowoffset,1+col,s,formatNumber);
                break;
            case static_cast<int>(priceNormalization::PriceNorm::nameRazdel):
                formatNumber.setFontColor(Qt::black);
                xlsx.write(1+row+rowoffset,1+col,s,formatLeft);
                break;
            }
        }
    }

    QMessageBox mbox(this);
    if(xlsx.saveAs(filename)){
        filename = "Данные успешно сохранены.\n" + filename;
        mbox.setWindowTitle(tr("Внимание"));
        mbox.setText(filename);
        mbox.setModal(true);
        mbox.setStandardButtons(QMessageBox::Ok);
        mbox.setIcon(QMessageBox::Information);
        QTimer::singleShot(3000,&mbox,SLOT(accept()));
        mbox.exec();
    }
    else
    {
        filename = "Ошибка записи файла.\n" + filename;
        mbox.setWindowTitle(tr("Внимание"));
        mbox.setText(filename);
        mbox.setModal(true);
        mbox.setStandardButtons(QMessageBox::Ok);
        mbox.setIcon(QMessageBox::Information);
        QTimer::singleShot(3000,&mbox,SLOT(accept()));
        mbox.exec();
    }
}

