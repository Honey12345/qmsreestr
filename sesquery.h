#ifndef SESQUERY_H
#define SESQUERY_H

#include <QSqlQuery>
#include <QThread>

class SesQuery  : public QObject {
    Q_OBJECT

public:
    SesQuery();
    ~SesQuery();
    void startQuery(QSqlQuery *query);
private:
    void addThread(QSqlQuery *query);
    void stopThreads();

private slots:
    void ended(bool ok = false);   // слот вызывается по окончании работы потока в котором формируется SQL запрос

signals:
    void stopAll(); //остановка всех потоков
    void endquery(bool);

};

class ThreadQuery : public QObject {
    Q_OBJECT


public:
    ThreadQuery(QSqlQuery *query);
    ~ThreadQuery();
    QSqlQuery *q;
public slots:
    void process(); 	/*  создает и запускает выполнение запроса SQL */
    void stop();    	/*  останавливает процесс */

signals:
    void finish(bool); 	/* сигнал о завершении  работы запроса */
    void finished();
};

#endif // SESQUERY_H
