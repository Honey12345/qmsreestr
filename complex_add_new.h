#ifndef COMPLEX_ADD_NEW_H
#define COMPLEX_ADD_NEW_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlQueryModel>
#include <QMessageBox>
#include <QLineEdit>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QDebug>
#include <QObject>
#include <QTimer>

class ComplexAdd : public QObject {
public:
    /// достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int

    enum class econom :int {id,name_usl};
    Q_ENUM(econom)

    enum class price :int {id,code_price,name_price,closedata};
    Q_ENUM(price)

    enum class complex :int {name_usl,code_price,name_price,id_usl,id_price};
    Q_ENUM(complex)


    Q_OBJECT
    Enums() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums
};

namespace Ui {
class complex_add_new;
}

class complex_add_new : public QDialog
{
    Q_OBJECT

public:
    explicit complex_add_new(QWidget *parent = nullptr);
    ~complex_add_new();

    void SetStyleGroupBox();
    void SetModelPrice();
    void SetModelEconom();

    int GetIdComplex();

private slots:

    void TableView_price_DoubleClicked(QModelIndex index);
    void TableView_econom_DoubleClicked(QModelIndex index);
    void TableView_complex_DoubleClicked();

    void model_complex_RowsInsertedRemoved();

    void saveComplex();

    void FindeKod(QString str);
    void HeaderDoubleClicked(int index);

private:
    Ui::complex_add_new *ui;
    QSqlQueryModel *model_econom, *model_price;
    QStandardItemModel * model_complex;
    int idComplex = 0;
    QLineEdit *findeKod;
};



#endif // COMPLEX_ADD_NEW_H
