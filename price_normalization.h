#ifndef PRICE_NORMALIZATION_H
#define PRICE_NORMALIZATION_H

#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QTextCodec>
#include <QByteArray>
#include <QDateTime>
#include <QXmlStreamReader>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QMap>
#include <QProgressDialog>
#include <QXmlStreamReader>
#include <QTimer>

class priceNormalization : public QObject {
public:
    /// достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int

    enum class PriceNorm :int {kodNew,nameNew,stoimostNew,kodqMS,nameqMS,stoimostqMS,nameRazdel,flag};
    Q_ENUM(PriceNorm)

    Q_OBJECT
    priceNormalization() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums

};


class price_normalization_QStandardItemModel : public QStandardItemModel
{
    Q_OBJECT
public:

    explicit price_normalization_QStandardItemModel()
    {

    }
    ~price_normalization_QStandardItemModel()
    {
    }

//    Qt::ItemFlags flags(const QModelIndex &index) const
//    {
//        Qt::ItemFlags flags = my_DogovorConfirmed_QSqlQueryModel::flags(index);
//        if ( index.column() != static_cast<int>(Dogovor::services::code_service)){
//            //flags.setFlag(Qt::ItemIsEditable,false);
//            flags ^= Qt::ItemIsEditable;
//        }
//        return flags;
//    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
//        QVariant value = QSqlQueryModel::data(index, role);
//        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
//        case Qt::DisplayRole:
//            if(index.column() == static_cast<int>(MainDogovor::dogovor::approved) || index.column() == static_cast<int>(MainDogovor::dogovor::changed)
//                    || index.column() == static_cast<int>(MainDogovor::dogovor::closed)){
//                if(value.isNull()){
//                    return "";
//                }else{
//                    return value.toDateTime().toString("dd-MM-yyyy HH:mm:ss");
//                }
//            }
//            break;

        case Qt::TextAlignmentRole:
            if(index.column() == static_cast<int>(priceNormalization::PriceNorm::kodNew) || index.column() == static_cast<int>(priceNormalization::PriceNorm::kodqMS)){
                return int(Qt::AlignCenter| Qt::AlignVCenter);
            }
            else if(index.column() == static_cast<int>(priceNormalization::PriceNorm::stoimostNew) || index.column() == static_cast<int>(priceNormalization::PriceNorm::stoimostqMS) ){
                return int(Qt::AlignRight| Qt::AlignVCenter);
            }
            break;
        case Qt::TextColorRole:
            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().isEmpty())
            {
                return QBrush(Qt::blue,Qt::SolidPattern);
            }
            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("NewRecord"))
            {
                return QBrush(Qt::darkGreen,Qt::SolidPattern);
            }
            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("KodNoEqually")
                   && index.column() == static_cast<int>(priceNormalization::PriceNorm::kodNew))
            {
                return QBrush(Qt::red,Qt::SolidPattern);
            }
            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("NameNoEqually")
                   && index.column() == static_cast<int>(priceNormalization::PriceNorm::nameNew))
            {
                return QBrush(Qt::red,Qt::SolidPattern);
            }
//            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("StoimostLow")
//                   && index.column() == static_cast<int>(priceNormalization::PriceNorm::stoimostNew))
//            {
//                return QBrush(Qt::red,Qt::SolidPattern);
//            }
//            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("StoimostUp")
//                   && index.column() == static_cast<int>(priceNormalization::PriceNorm::stoimostNew))
//            {
//                return QBrush(Qt::red,Qt::SolidPattern);
//            }
            break;
        case Qt::BackgroundColorRole:
//            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("StoimostEqually")
//                   && index.column() == static_cast<int>(priceNormalization::PriceNorm::stoimostNew))
//            {
//                return QColor(Qt::green);
//            }
            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("StoimostLow")
                   && index.column() == static_cast<int>(priceNormalization::PriceNorm::stoimostNew))
            {
                return QColor(Qt::red);
            }
            if(QStandardItemModel::data(QStandardItemModel::index(index.row(),static_cast<int>(priceNormalization::PriceNorm::flag))).toString().contains("StoimostUp")
                   && index.column() == static_cast<int>(priceNormalization::PriceNorm::stoimostNew))
            {
                return QColor(Qt::red);
            }
        default:
            break;

        }
        return QStandardItemModel::data(index, role);

    }

private slots:
signals:
private:
};


namespace Ui {
    class price_normalization;
}

class price_normalization : public QDialog
{
    Q_OBJECT

public:
    explicit price_normalization(QWidget *parent = nullptr);
    ~price_normalization();

    void SetModel();
    void SetListRazdel(QString);
    void FindCompareInModel(QString, QString, QString, QString);
    QString CheckFileCodec(QString);

private slots:

    void loadPrice_qMS();
    void loadPrice_PEO();
    void uploadToExcel();


private:
    Ui::price_normalization *ui;
    price_normalization_QStandardItemModel *model;
    QStringList list_razdel;
};

#endif // PRICE_NORMALIZATION_H
