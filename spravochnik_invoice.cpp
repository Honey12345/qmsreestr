#include "spravochnik_invoice.h"
#include "ui_spravochnik_invoice.h"
#include "global.h"

spravochnik_invoice::spravochnik_invoice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::spravochnik_invoice)
{
    ui->setupUi(this);
    spravochnik_invoice::setWindowTitle("Справочник счетов");
    model_invoice = new QSqlQueryModel(this);
    refresh_model(TypeRefresh::Init);
    ui->pushButton_add->setEnabled(false);
    ui->comboBox_year->setCurrentIndex(ui->comboBox_year->findText(QString::number(QDate::currentDate().year())));
    ui->comboBox_month->setCurrentIndex(QDate::currentDate().month()-1);


    connect(ui->lineEdit_number,SIGNAL(textChanged(QString)),this,SLOT(setInvoiceNumber(QString)));


    /****************************************************************************************************************************************************************************
     *  Установка стиля отображения GroupBox - рамка, цвет рамки, расположение заголовка и т.д.*
     * *************************************************************************************************************************************************************************/

    ui->groupBox->setStyleSheet("QGroupBox {"
                                "border: 1px solid gray;"
                                "border-radius: 9px;"
                                "border-color: blue;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: blue"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");
//    ui->groupBox->setStyleSheet("QGroupBox::title {"
//                                "subcontrol-origin: margin;"
//                                "left: 3px;"
//                                "padding: 0 3px 0 3px;"
//                            "}");

    ui->pushButton_exit->setDefault(true);

}

spravochnik_invoice::~spravochnik_invoice()
{
    delete model_invoice;
    delete ui;
}

void spravochnik_invoice::refresh_model(TypeRefresh type, int row)
{
    QSqlQuery query;
    query.prepare("SELECT 'Счет ' || invoicenum || ' за ' || case when month = 1 then 'январь'"
                                     " when month = 2 then 'февраль'"
                                     " when month = 3 then 'март'"
                                     " when month = 4 then 'апрель'"
                                     " when month = 5 then 'май'"
                                     " when month = 6 then 'июнь'"
                                     " when month = 7 then 'июль'"
                                     " when month = 8 then 'август'"
                                     " when month = 9 then 'сентябрь'"
                                     " when month = 10 then 'октябрь'"
                                     " when month = 11 then 'ноябрь'"
                                     " when month = 12 then 'декабрь' end || ' ' || to_char(year,'9999') as schet, invoiceid FROM invoice ORDER BY year DESC,month,invoicenum");


    query.exec();
    model_invoice->setQuery(query);
    if(type == TypeRefresh::Init){
        model_invoice->setHeaderData(0,Qt::Horizontal," Счет ");
        model_invoice->setHeaderData(1,Qt::Horizontal, " id ");
        ui->tableView->setModel(model_invoice);
        ui->tableView->hideColumn(1);
        ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        ui->tableView->selectRow(0);
    }
    else if(type == TypeRefresh::Dell){
        if(row > 0 && model_invoice->rowCount() > 0){
            row--;
        }
        else{
            row = 0;
        }
        ui->tableView->selectRow(row);
        ui->tableView->update();
    }
    else if(type == TypeRefresh::Insert)
    {
        ui->tableView->update();
        QModelIndexList list = model_invoice->match(model_invoice->index(0,1),Qt::DisplayRole,invoiceid_return);
        ui->tableView->selectRow(list.at(0).row());
    }
    ui->tableView->setFocus();
}

void spravochnik_invoice::on_pushButton_exit_clicked()
{
    return reject();
}

void spravochnik_invoice::setInvoiceNumber(QString str)
{
    if(str.length() == 0){
        ui->pushButton_add->setEnabled(false);
        return;
    }
    ui->pushButton_add->setEnabled(true);
}

void spravochnik_invoice::on_pushButton_add_clicked()
{
    int month = ui->comboBox_month->currentIndex()+1;
    int year = ui->comboBox_year->currentText().toInt();
    QString invoicenumber = ui->lineEdit_number->text().trimmed();
    QSqlQuery query;
    query.clear();
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {
        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!</p>");
            return;
        }
        else
        {
            QString str_query = "select set_config('auth.username', ?, false)";
            query.prepare(str_query);
            query.addBindValue(UserName);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
            query.prepare("INSERT INTO invoice (year, month, invoicenum, date) VALUES(?, ?, ?, ?) RETURNING invoiceid");
            query.addBindValue(year);
            query.addBindValue(month);
            query.addBindValue(invoicenumber);
            query.addBindValue(QDate::currentDate());
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса добавления счета.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
            query.first();
            invoiceid_return = query.value(0);
        }
        if(!db.commit()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
            db.rollback();
            return;
        }
        QMessageBox *mbox = new QMessageBox(this);
        mbox->setWindowTitle(tr("Внимание"));
        mbox->setText("Данные успешно сохранены.");
        mbox->setModal(true);
        mbox->setStandardButtons(QMessageBox::Ok);
        mbox->setIcon(QMessageBox::Information);
        QTimer::singleShot(1500,mbox,SLOT(deleteLater()));
        mbox->exec();
        refresh_model(TypeRefresh::Insert);
        ui->lineEdit_number->clear();
        return;
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return;
    }
}

void spravochnik_invoice::on_pushButton_del_clicked()
{
    QSqlQuery query;
    int row = ui->tableView->currentIndex().row();
    QSqlRecord rec =  model_invoice->record(row);
    int invoiceid = rec.value(rec.indexOf("invoiceid")).toInt();
    QString strinvoice = rec.value(rec.indexOf("schet")).toString();
    query.prepare("select idreestr from reestr where invoiceid = ?");
    query.addBindValue(invoiceid);
    query.exec();
    if(query.first()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Невозможно удалить<br><font color = blue>" + strinvoice +"</font>.<br>К счету привязаны записи.</p>");
        return;
    }
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {
        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!</p>");
            return;
        }
        else
        {
            QString str_query = "select set_config('auth.username', ?, false)";
            query.prepare(str_query);
            query.addBindValue(UserName);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
            query.prepare("DELETE FROM oms.invoice WHERE invoiceid = ?");
            query.addBindValue(invoiceid);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения удаления счета.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
        }
        if(!db.commit()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
            db.rollback();
            return;
        }
        QMessageBox *mbox = new QMessageBox(this);
        mbox->setWindowTitle(tr("Внимание"));
        mbox->setText("<p align=center><font color = blue>" + strinvoice +"</font><br>успешно удален</p>");
        mbox->setModal(true);
        mbox->setStandardButtons(QMessageBox::Ok);
        mbox->setIcon(QMessageBox::Information);
        QTimer::singleShot(1500,mbox,SLOT(deleteLater()));
        mbox->exec();
        refresh_model(TypeRefresh::Dell,row);
        return;
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return;
    }
}
