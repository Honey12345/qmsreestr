#ifndef SPRAVQMSPRICE_H
#define SPRAVQMSPRICE_H

#include <QDialog>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QDebug>
#include <QLineEdit>

namespace Ui {
class spravqmsprice;
}

class spravqmsprice : public QDialog
{
    Q_OBJECT

public:
    explicit spravqmsprice(QWidget *parent = nullptr);
    ~spravqmsprice();

    void setModel();
    void setHeaderData();
    void setView();

private slots:

    void FindKod(QString);

    void HeaderViewSectionClicked(int index);

private:
    Ui::spravqmsprice *ui;
    QStandardItemModel *model;
    QLineEdit *findeKod;
};

#endif // SPRAVQMSPRICE_H
