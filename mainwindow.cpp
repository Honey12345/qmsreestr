//      Выгрузка в Excel без использования auxcontainer и MS Excell
//       https://github.com/VSRonin/QtXlsxWriter
//       http://qtxlsx.debao.me/
//  Qt + gui-private
//  include(qtxlsx/xlsx/qtxlsx.pri)


#include "enums.h"
#include "global.h"
#include "custommodel.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "loadprice.h"
#include "spravqmsprice.h"
#include "spraveconomuslugi.h"
#include "complex.h"
#include "dogovor_add.h"
#include "complex_add_new_service.h"
#include "inpudatedialog.h"
#include "set_smtp_server.h"
#include <qtxlsx/xlsx/xlsxdocument.h>
#include "smtpdialog.h"
#include "recipients.h"
#include "complex_edit.h"
#include "treeview.h"
#include "spravochnik_invoice.h"
#include "sesquery.h"
#include "splashwindows.h"
#include "selectinvoice.h"
#include "price_normalization.h"




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);    
    //this->setWindowFlags(Qt::Window|Qt::WindowContextHelpButtonHint|Qt::WindowMinMaxButtonsHint);

    loadSettings();
    MainWindow::setWindowTitle("Пользователь : " + displayName + " | HostName : " + db.hostName() + " | База данных : " + db.databaseName());

    switch (UserRole) {
    case static_cast<int>(Enums::Access::EconomistRole):
        ui->action_load_price->setEnabled(false);
        ui->action_complex->setEnabled(false);
        ui->menu_3->setEnabled(false);
        break;
    case static_cast<int>(Enums::Access::SisterOVDPO):
        ui->action_load_price->setEnabled(false);
        ui->action_complex->setEnabled(false);
        ui->menu_3->setEnabled(false);
        break;
    default:
        break;
    }


//    ui->tabWidget->setTabText(0,"Утвержденные Договора");
//    ui->tabWidget->setTabText(1,"Договора в работе");

    model_confirmed = new my_DogovorConfirmed_QSqlQueryModel;
    model_dogovor = new my_Dogovor_QSqlQueryModel;
    model_complex = new my_Complex_QSqlQueryModel;
    model_services = new my_Services_QSqlQueryModel;
    model_reestr = new my_Reestr_QStandardItemModel;

    ui->tableView_confirmed->setModel(model_confirmed);
    ui->tableView_dogovor->setModel(model_dogovor);
    ui->tableView_complex->setModel(model_complex);
    ui->tableView_services->setModel(model_services);
    ui->tableView_reestr->setModel(model_reestr);

    SetModelDogovorConfirmed();
    SetModelDogovor();
    SetModelComplex();
    SetModelServices();
    SetModelReestr();

    model_combobox_invoice = new QSqlQueryModel(ui->comboBox_invoice);
    SetInvoiceCombobox();

    // Подключение к контекстному меню **************************************

    ui->tableView_reestr->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->tableView_reestr,&QTableView::customContextMenuRequested,this,&MainWindow::showContextMenu);



    connect(ui->tabWidget,&QTabWidget::currentChanged,this,&MainWindow::tabWidgetChange);

    connect(ui->action_usligi_qMS,&QAction::triggered,this,&MainWindow::qmspriceSpravochnik);
    connect(ui->action_load_price,&QAction::triggered,this,&MainWindow::load);
    connect(ui->action_usligi_econom,&QAction::triggered,this,&MainWindow::economUslugiSpravochnik);
    connect(ui->action_complex,&QAction::triggered,this,&MainWindow::complexSpravochnik);
    connect(ui->action_add_dogovor,&QAction::triggered,this,&MainWindow::addDogovor);
    connect(ui->actionsmtpserver,&QAction::triggered,this,&MainWindow::SetSmtpServers);
    connect(ui->action_recipient,&QAction::triggered,this,&MainWindow::SetRecipients);

    connect(ui->pushButton_add_dogovor,&QPushButton::clicked,this,&MainWindow::addDogovor);
    connect(ui->pushButton_del_dogovor,&QPushButton::clicked,this,&MainWindow::delDogovor);
    connect(ui->pushButton_add_complex,&QPushButton::clicked,this,&MainWindow::addComplex);
    connect(ui->pushButton_del_complex,&QPushButton::clicked,this,&MainWindow::delComplex);
    connect(ui->pushButton_edit_complex,&QPushButton::clicked,this,&MainWindow::editComplex);
    connect(ui->pushButton_add_service,&QPushButton::clicked,this,&MainWindow::addService);
    connect(ui->pushButton_del_service,&QPushButton::clicked,this,&MainWindow::delService);

    connect(ui->tableView_dogovor,&MyTableView_dogovor::changeData_dogovor,this,&MainWindow::ChangeDataDogovor);
    connect(ui->tableView_complex,&MyTableView_complex::changeData_complex,this,&MainWindow::ChangeDataComplex);
    connect(ui->tableView_confirmed,&MyTableView_dogovor_approved::changeData_dogovor_approved,this,&MainWindow::ChangeDataDogovorApproved);

    connect(ui->pushButton_approved,&QPushButton::clicked,this,&MainWindow::DogovorApproved);
    connect(ui->pushButton_close,&QPushButton::clicked,this,&MainWindow::DogovorClose);
    connect(ui->pushButton_edit,&QPushButton::clicked,this,&MainWindow::DogovorEdit);

    connect(ui->tableView_confirmed,&QTableView::doubleClicked,this,&MainWindow::DoubleClickedDogovor);

    connect(ui->action_load_reestr,&QAction::triggered,this,&MainWindow::loadReestr);
    connect(ui->pushButton_del_reestr_records,&QPushButton::clicked,this,&MainWindow::delReestrRecords);
    connect(ui->pushButton_Exel,&QPushButton::clicked,this,&MainWindow::printReestr);

    connect(ui->action_invoice,&QAction::triggered,this,&MainWindow::invoiceSpravochnik);
    connect(ui->pushButton_comboBox_invoice_clear,&QPushButton::clicked,this,&MainWindow::combobox_clear_change);
    connect(ui->comboBox_invoice,QOverload<int>::of(&QComboBox::currentIndexChanged),this,&MainWindow::combobox_invoice_change);

    connect(ui->action_refresh,&QAction::triggered,this,&MainWindow::refreshButton);

    connect(ui->action_exit,&QAction::triggered,this,&MainWindow::Exit);

    CheckPushButtons();

    ui->tabWidget->setCurrentIndex(1);

    SetStyleGroupBox();

    ChangeDataDogovorApproved(ui->tableView_confirmed->currentIndex());
}


MainWindow::~MainWindow()
{
    saveSettings();

    delete model_confirmed;
    delete model_dogovor;
    delete model_complex;
    delete model_services;
    delete model_reestr;

    delete model_combobox_invoice;

    if(model_tree != nullptr){
        delete model_tree;
    }
    delete ui;
}

void MainWindow::loadSettings()
{
    int screen_width = QApplication::desktop()->width();
    int screen_height = QApplication::desktop()->height();
    setGeometry(settings->value("MainWindow", QRect(screen_width/2-1693/2,screen_height/2-823/2,1693,823)).toRect());
}

void MainWindow::saveSettings()
{
    settings->setValue("MainWindow", geometry());
}

void MainWindow::SetModelDogovor()
{
    SetQueryDogovor();
    model_dogovor->setHeaderData(static_cast<int>(MainDogovor::dogovor::numberdogovor),Qt::Horizontal,"Номер договора");
    model_dogovor->setHeaderData(static_cast<int>(MainDogovor::dogovor::namekontragent),Qt::Horizontal,"Контрагент");
    model_dogovor->setHeaderData(static_cast<int>(MainDogovor::dogovor::start),Qt::Horizontal,"Начало");
    model_dogovor->setHeaderData(static_cast<int>(MainDogovor::dogovor::finish),Qt::Horizontal,"Окончание");

    ui->tableView_dogovor->setColumnHidden(static_cast<int>(MainDogovor::dogovor::iddogovor),true);
    ui->tableView_dogovor->setColumnHidden(static_cast<int>(MainDogovor::dogovor::approved),true);
    ui->tableView_dogovor->setColumnHidden(static_cast<int>(MainDogovor::dogovor::changed),true);
    ui->tableView_dogovor->setColumnHidden(static_cast<int>(MainDogovor::dogovor::closed),true);

    ui->tableView_dogovor->setColumnWidth(static_cast<int>(MainDogovor::dogovor::numberdogovor),150);
    ui->tableView_dogovor->setColumnWidth(static_cast<int>(MainDogovor::dogovor::namekontragent),450);
    ui->tableView_dogovor->setColumnWidth(static_cast<int>(MainDogovor::dogovor::start),100);
    ui->tableView_dogovor->setColumnWidth(static_cast<int>(MainDogovor::dogovor::finish),100);

    ui->tableView_dogovor->selectRow(0);
    ui->tableView_dogovor->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::dogovor::numberdogovor),QHeaderView::Fixed);
    ui->tableView_dogovor->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::dogovor::namekontragent),QHeaderView::Stretch);
    ui->tableView_dogovor->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::dogovor::start),QHeaderView::Fixed);
    ui->tableView_dogovor->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::dogovor::finish),QHeaderView::Fixed);
}

void MainWindow::SetModelComplex()
{
    SetQueryComplex();
    model_complex->setHeaderData(static_cast<int>(MainDogovor::complex::codecomplex),Qt::Horizontal,"Код");
    model_complex->setHeaderData(static_cast<int>(MainDogovor::complex::namecomplex),Qt::Horizontal,"Наименование");
    model_complex->setHeaderData(static_cast<int>(MainDogovor::complex::stoimost),Qt::Horizontal,"Стоимость");

    ui->tableView_complex->setColumnHidden(static_cast<int>(MainDogovor::complex::idcomplex),true);
    ui->tableView_complex->setColumnHidden(static_cast<int>(MainDogovor::complex::iddogovor),true);


    ui->tableView_complex->setColumnWidth(static_cast<int>(MainDogovor::complex::codecomplex),100);
    ui->tableView_complex->setColumnWidth(static_cast<int>(MainDogovor::complex::namecomplex),600);
    ui->tableView_complex->setColumnWidth(static_cast<int>(MainDogovor::complex::stoimost),200);

    ui->tableView_complex->selectRow(0);
    ui->tableView_complex->horizontalHeader()->setSectionResizeMode(3,QHeaderView::Stretch);
    ui->tableView_complex->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Fixed);
    ui->tableView_complex->horizontalHeader()->setSectionResizeMode(4,QHeaderView::Fixed);

    QModelIndex index = ui->tableView_dogovor->currentIndex();
    QString str = " Договор № " + model_dogovor->data(model_dogovor->index(index.row(),static_cast<int>(MainDogovor::dogovor::numberdogovor))).toString() +
            " (" + model_dogovor->data(model_dogovor->index(index.row(),static_cast<int>(MainDogovor::dogovor::namekontragent))).toString();
    ui->groupBox_complex->setTitle(str);
}

void MainWindow::SetModelServices()
{
    SetQueryServices();
    model_services->setHeaderData(static_cast<int>(MainDogovor::services::code_service),Qt::Horizontal,"Код");
    model_services->setHeaderData(static_cast<int>(MainDogovor::services::name_usl),Qt::Horizontal,"Наименование");
    model_services->setHeaderData(static_cast<int>(MainDogovor::services::code_price),Qt::Horizontal,"Прайс");
    model_services->setHeaderData(static_cast<int>(MainDogovor::services::name_price),Qt::Horizontal,"Наименвание по прайсу");

    ui->tableView_services->setColumnHidden(static_cast<int>(MainDogovor::services::idservicecomplex),true);
    ui->tableView_services->setColumnHidden(static_cast<int>(MainDogovor::services::idcomplex),true);
    ui->tableView_services->setColumnHidden(static_cast<int>(MainDogovor::services::ideconom),true);
    ui->tableView_services->setColumnHidden(static_cast<int>(MainDogovor::services::idprice),true);

    ui->tableView_services->setColumnWidth(static_cast<int>(MainDogovor::services::code_service),150);
    ui->tableView_services->setColumnWidth(static_cast<int>(MainDogovor::services::name_usl),350);
    ui->tableView_services->setColumnWidth(static_cast<int>(MainDogovor::services::code_price),150);
    ui->tableView_services->setColumnWidth(static_cast<int>(MainDogovor::services::name_price),350);
    ui->tableView_services->selectRow(0);

    ui->tableView_services->horizontalHeader()->setSectionResizeMode(4,QHeaderView::Fixed);
    ui->tableView_services->horizontalHeader()->setSectionResizeMode(5,QHeaderView::Stretch);
    ui->tableView_services->horizontalHeader()->setSectionResizeMode(6,QHeaderView::Fixed);
    ui->tableView_services->horizontalHeader()->setSectionResizeMode(7,QHeaderView::Stretch);

    QModelIndex index = ui->tableView_complex->currentIndex();
    QString str = " Комплекс № " + model_complex->data(model_complex->index(index.row(),static_cast<int>(MainDogovor::complex::codecomplex))).toString() +
            " (" + model_complex->data(model_complex->index(index.row(),static_cast<int>(MainDogovor::complex::namecomplex))).toString();
    ui->groupBox_services->setTitle(str);
}

void MainWindow::SetModelDogovorConfirmed()
{
    SetQueryConfirmed();
    model_confirmed->setHeaderData(static_cast<int>(MainDogovor::dogovor::numberdogovor),Qt::Horizontal,"Номер договора");
    model_confirmed->setHeaderData(static_cast<int>(MainDogovor::dogovor::namekontragent),Qt::Horizontal,"Контрагент");
    model_confirmed->setHeaderData(static_cast<int>(MainDogovor::dogovor::start),Qt::Horizontal,"Начало");
    model_confirmed->setHeaderData(static_cast<int>(MainDogovor::dogovor::finish),Qt::Horizontal,"Окончание");
    model_confirmed->setHeaderData(static_cast<int>(MainDogovor::dogovor::approved),Qt::Horizontal,"Утвержден");
    model_confirmed->setHeaderData(static_cast<int>(MainDogovor::dogovor::changed),Qt::Horizontal,"Изменен");
    model_confirmed->setHeaderData(static_cast<int>(MainDogovor::dogovor::closed),Qt::Horizontal,"Закрыт");

    ui->tableView_confirmed->setColumnHidden(static_cast<int>(MainDogovor::dogovor::iddogovor),true);

    ui->tableView_confirmed->setColumnWidth(static_cast<int>(MainDogovor::dogovor::numberdogovor),150);
    ui->tableView_confirmed->setColumnWidth(static_cast<int>(MainDogovor::dogovor::namekontragent),450);
    ui->tableView_confirmed->setColumnWidth(static_cast<int>(MainDogovor::dogovor::start),100);
    ui->tableView_confirmed->setColumnWidth(static_cast<int>(MainDogovor::dogovor::finish),100);
    ui->tableView_confirmed->setColumnWidth(static_cast<int>(MainDogovor::dogovor::approved),180);
    ui->tableView_confirmed->setColumnWidth(static_cast<int>(MainDogovor::dogovor::changed),180);
    ui->tableView_confirmed->setColumnWidth(static_cast<int>(MainDogovor::dogovor::closed),180);
    ui->tableView_confirmed->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::dogovor::namekontragent),QHeaderView::Stretch);

    ui->tableView_confirmed->selectRow(0);

}

QString MainWindow::FormFileDogovor(int iddogovor)
{
    QSqlQuery query;
    query.prepare(""
                  "select codecomplex,namecomplex, stoimost,code_service,name_usl,code_price,name_price,numberdogovor,namekontragent,start,finish,approved,changed,closed,codecomplexqms  from dogovor_complex"
                  "  left join dogovor_complex_services on dogovor_complex.idcomplex = dogovor_complex_services.idcomplex"
                  "  left join econom_usl u on dogovor_complex_services.ideconom = u.id"
                  "  left join qmspriceusl q on dogovor_complex_services.idprice = q.id"
                  "  left join dogovor using(iddogovor)"
                  " where iddogovor = ? order by codecomplex,namecomplex,name_usl"
                  "");
    query.addBindValue(iddogovor);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return QString();
    }
    query.first();
    QVariant s;
    int rowoffset = 0;
    QXlsx::Document xlsx;
    QXlsx::Format formatHeader, formatLeft,formatRigth, formatCenter, formatMoney, formatData,formatNumber;
    QXlsx::Format formatLeftTextBold,formatRigthText;
    QXlsx::CellRange range;

    QString filename,str;
    QString winpath;
    winpath = "/Downloads/";
    str = "Договор_№_" ;

    filename = QDir::homePath() + winpath + str + query.value("numberdogovor").toString()+ ".xlsx";
//            + "_" + QDateTime::currentDateTime().toString("ddMMyyyyHHmm") + ".xlsx";

    formatHeader.setFontColor(QColor(Qt::blue));
    formatHeader.setFontSize(16);
    formatHeader.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatHeader.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatHeader.setBorderStyle(QXlsx::Format::BorderThin);
    formatHeader.setTextWarp(true);

    formatLeft.setFontColor(QColor(Qt::black));
    formatLeft.setFontSize(12);
    formatLeft.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeft.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatLeft.setBorderStyle(QXlsx::Format::BorderThin);
    formatLeft.setTextWarp(true);

    formatRigth.setFontColor(QColor(Qt::black));
    formatRigth.setFontSize(12);
    formatRigth.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigth.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatRigth.setBorderStyle(QXlsx::Format::BorderThin);

    formatCenter.setFontColor(QColor(Qt::black));
    formatCenter.setFontSize(12);
    formatCenter.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatCenter.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatCenter.setBorderStyle(QXlsx::Format::BorderThin);
    formatCenter.setTextWarp(true);

    formatMoney.setFontColor(QColor(Qt::black));
    formatMoney.setFontSize(12);
    formatMoney.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatMoney.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatMoney.setBorderStyle(QXlsx::Format::BorderThin);
    formatMoney.setNumberFormatIndex(8);

    formatData.setFontColor(QColor(Qt::black));
    formatData.setFontSize(12);
    formatData.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatData.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatData.setBorderStyle(QXlsx::Format::BorderThin);
    formatData.setNumberFormat("dd.mm.yyyy");

    formatNumber.setFontColor(QColor(Qt::black));
    formatNumber.setFontSize(12);
    formatNumber.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatNumber.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatNumber.setBorderStyle(QXlsx::Format::BorderThin);
    formatNumber.setNumberFormatIndex(2);


    formatLeftTextBold.setFontColor(QColor(Qt::black));
    formatLeftTextBold.setFontName("Times New Roman");
    formatLeftTextBold.setFontSize(14);
    formatLeftTextBold.setFontBold(true);
    formatLeftTextBold.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeftTextBold.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    formatRigthText.setFontColor(QColor(Qt::black));
    formatRigthText.setFontSize(12);
    formatRigthText.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigthText.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(1);
    range.setLastColumn(2);
    str = "Договор №";
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,1,str,formatLeft);
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(3);
    range.setLastColumn(4);
    str = query.value("numberdogovor").toString();
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,3,str,formatCenter);
    rowoffset++;
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(1);
    range.setLastColumn(2);
    str = "Контрагент:";
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,1,str,formatLeft);
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(3);
    range.setLastColumn(4);
    str = query.value("namekontragent").toString();
    xlsx.mergeCells(range,formatCenter);
    xlsx.write(1+rowoffset,3,str,formatCenter);
    rowoffset++;
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(1);
    range.setLastColumn(2);
    str = "Действует:";
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,1,str,formatLeft);
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(3);
    range.setLastColumn(4);
    str = query.value("start").toDate().toString("dd-MM-yyyy") +
            " - " + query.value("finish").toDate().toString("dd-MM-yyyy");
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,3,str,formatCenter);
    rowoffset++;
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(1);
    range.setLastColumn(2);
    str = "Утвержден:";
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,1,str,formatLeft);
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(3);
    range.setLastColumn(4);
    str = query.value("approved").toString();
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,3,str,formatCenter);
    rowoffset++;
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(1);
    range.setLastColumn(2);
    str = "Изменен:";
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,1,str,formatLeft);
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(3);
    range.setLastColumn(4);
    str = query.value("changed").toString();
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,3,str,formatCenter);
    rowoffset++;
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(1);
    range.setLastColumn(2);
    str = "Закрыт";
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,1,str,formatLeft);
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(3);
    range.setLastColumn(4);
    str = query.value("closed").toString();
    xlsx.mergeCells(range,formatLeft);
    xlsx.write(1+rowoffset,3,str,formatCenter);

    rowoffset++;
    int col = 0;
    {
        s = QString("Код комплекса");
        xlsx.write(1+rowoffset, 1, s, formatHeader);
        range.setFirstRow(1+rowoffset);
        range.setLastRow(1+rowoffset);
        range.setFirstColumn(2);
        range.setLastColumn(3);
        s = QString("Наименование комплекса");
        xlsx.mergeCells(range,formatLeft);
        xlsx.write(1+rowoffset,2,s,formatHeader);
        s = QString("Стоимость");
        xlsx.write(1+rowoffset, 4, s, formatHeader);
        s = QString("Код qMS");
        xlsx.setColumnWidth(5,20);
        xlsx.write(1+rowoffset, 5, s, formatHeader);
        rowoffset++;

        s = QString("Код");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Наименование услуги");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Код прайса");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Наименование по прайсу");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
    }
    QString codecomplex = QString();
    rowoffset++;
    do{
        if(codecomplex != query.value("codecomplex").toString()){
            s = codecomplex = query.value("codecomplex").toString();
            formatHeader.setFontColor(QColor(Qt::black));
            formatHeader.setPatternBackgroundColor(QColor(Qt::cyan));
            formatNumber.setFontSize(16);
            formatNumber.setPatternBackgroundColor(QColor(Qt::cyan));
            xlsx.write(1+rowoffset,1,s,formatHeader);
            range.setFirstRow(1+rowoffset);
            range.setLastRow(1+rowoffset);
            range.setFirstColumn(2);
            range.setLastColumn(3);
            s = query.value("namecomplex").toString();
            xlsx.mergeCells(range,formatLeft);
            xlsx.write(1+rowoffset,2,s,formatHeader);
            s = query.value("stoimost").toDouble();
            xlsx.write(1+rowoffset,4,s,formatNumber);
            s = query.value("codecomplexqms").toString();
            xlsx.write(1+rowoffset,5,s,formatHeader);
            rowoffset++;
        }
        s = query.value("code_service").toString();
        xlsx.write(1+rowoffset,1,s,formatCenter);
        s = query.value("name_usl").toString();
        xlsx.write(1+rowoffset,2,s,formatCenter);
        s = query.value("code_price").toString();
        xlsx.write(1+rowoffset,3,s,formatCenter);
        s = query.value("name_price").toString();
        xlsx.write(1+rowoffset,4,s,formatCenter);
        rowoffset++;
    }while(query.next());

    if(xlsx.saveAs(filename)){
//        filename = "Данные успешно сохранены.\n" + filename;
//        QMessageBox *mbox = new QMessageBox(this);
//        mbox->setWindowTitle(tr("Внимание"));
//        mbox->setText(filename);
//        mbox->setModal(true);
//        mbox->setStandardButtons(QMessageBox::Ok);
//        mbox->setIcon(QMessageBox::Information);
//        QTimer::singleShot(5000,mbox,SLOT(accept()));
//        mbox->exec();
        //**** Проверка на доступность файла *************************
    }
    else
    {
        filename = "Ошибка записи файла.\n" + filename;
        QMessageBox mbox(this);
        mbox.setWindowTitle(tr("Внимание"));
        mbox.setText(filename);
        mbox.setModal(true);
        mbox.setStandardButtons(QMessageBox::Ok);
        mbox.setIcon(QMessageBox::Critical);
        QTimer::singleShot(3000,&mbox,SLOT(accept()));
        mbox.exec();
        return QString();
    }
    return filename;
}

void MainWindow::SendMail(int iddogovor)
{
    QSqlQuery query;
    query.prepare("select * from dogovor where iddogovor = ?");
    query.addBindValue(iddogovor);
    query.exec();
    query.first();
    QString numberdogovor = query.value("numberdogovor").toString();

    query.prepare("select * from smtpservers where active ");
    if(!query.exec() || !query.first()){
        return;
    }
    QString server = query.value("servername").toString();
    int port = query.value("serverport").toInt();
    QString username = query.value("username").toString();
    QString password = query.value("password").toString();

    query.prepare("select string_agg(recipients,';') as rcpt from recipients");
    if(!query.exec() || !query.first()){
        return;
    }
    QString recipient = query.value("rcpt").toString();
    QString subject = QString::fromUtf8("Утвержденный договор");
    //QString certificat = "mailviveya.cer";
    QString message = "Утвержден договор № " + numberdogovor;
    QStringList files;
    files.append(FormFileDogovor(iddogovor));

//    Smtp* smtp = new Smtp(username, password, server, port);
//    connect(smtp,&Smtp::status,this,&MainWindow::MailSent);

//    if(!files.isEmpty() ){
//        smtp->sendMail(username, recipient , subject,message,certificat, files );
//    }
//    else{
//        smtp->sendMail(username, recipient , subject,message,certificat);
//    }
    smtpDialog *smtp = new smtpDialog();
    smtp->setParams(username, password, server, recipient, subject, message, files, port);

    QThread *thread = new QThread;
    smtp->moveToThread(thread);

    // при запуске патока будет вызван метод отправки почты по smtp
    connect(thread,&QThread::started,smtp,&smtpDialog::sendMail);

    // при завершении передачи почты, отправщик почты передаст потоку сигнал finish() , вызвав срабатывание слота quit()
    connect(smtp,&smtpDialog::finish,thread,&QThread::quit);

    // поток пометит себя для удаления, по окончании работы потока
    connect(thread,&QThread::finished,thread,&QThread::deleteLater);

    // отправщик почты пометит себя для удаления при окончании работы потока
    connect(thread,&QThread::finished,smtp,&smtpDialog::deleteLater);

    //запускаем поток
    thread->start();

}

void MainWindow::SetStyleGroupBox()
{
    ui->groupBox_dogovor->setStyleSheet("QGroupBox {"
                                "border: 2px solid red;"
                                "border-radius: 9px;"
                                "border-color: red;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: red"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_complex->setStyleSheet("QGroupBox {"
                                "border: 2px solid blue;"
                                "border-radius: 9px;"
                                "border-color: blue;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: blue"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_services->setStyleSheet("QGroupBox {"
                                "border: 2px solid green;"
                                "border-radius: 9px;"
                                "border-color: green;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: green"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

}

void MainWindow::SetModelReestr()
{
    QSqlQuery query;

    if(ui->comboBox_invoice->currentIndex() != -1)
    {
        query.prepare("select * from reestr where invoiceid = ? order by nomdog,nomkomp,fio");
        QSqlRecord rec =model_combobox_invoice->record(ui->comboBox_invoice->currentIndex());
        query.addBindValue(rec.value(rec.indexOf("invoiceid")).toInt());
    }
    else
    {
        query.prepare("select * from reestr where invoiceid isnull order by nomdog,nomkomp,fio");
    }

    SesQuery *ses = new SesQuery();
    splashWindows *dspl = new splashWindows(this,"Запрос\nк БД..");
    connect(ses,SIGNAL(endquery(bool)),dspl,SLOT(splashWindowsEnd(bool)));
    ses->startQuery(&query);
    while(!dspl->exec()){
    };
    if(!dspl->splashWindowsSqlExec()){
        QMessageBox::critical(nullptr,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса реестра<br><font color=red>" + query.lastError().text() +"</font></p>");
    }
    delete ses;
    delete dspl;
    model_reestr->clear();
    if(query.size() == 0){
        model_reestr->setColumnCount(12);
    }
    else{
        QList<QStandardItem*>list;
        query.first();
        do{
            for(int col = 0; col < query.record().count(); col++){
                if(col == static_cast<int>(MainDogovor::reestr::stoimost)){
                    list.append(new QStandardItem(QString::number(query.value(col).toDouble(),'f',2)));
                }
                else{
                    list.append(new QStandardItem(query.value(col).toString()));
                }
            }
            model_reestr->appendRow(list);
            list.clear();
        }while(query.next());
    }
    SetModelReestrHeader();
    SetModelReestrColumnView();
}

void MainWindow::SetModelReestrHeader()
{
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::idreestr),Qt::Horizontal,"id");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::nomepizod),Qt::Horizontal,"Эпизод №");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::fio),Qt::Horizontal,"ФИО пациента");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::birthday),Qt::Horizontal,"Дата р.");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::regnom),Qt::Horizontal,"Рег. №");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::datazakr),Qt::Horizontal,"Дата услуги");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::nomdog),Qt::Horizontal,"Договор №");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::namedog),Qt::Horizontal,"Контрагент");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::nomkomp),Qt::Horizontal,"Комплекс №");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::namekomp),Qt::Horizontal,"Наименование комплекса");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::stoimost),Qt::Horizontal,"Стоимость");
    model_reestr->setHeaderData(static_cast<int>(MainDogovor::reestr::invoiceid),Qt::Horizontal,"Период");
}

void MainWindow::SetModelReestrColumnView()
{
    ui->tableView_reestr->setColumnHidden(static_cast<int>(MainDogovor::reestr::idreestr),true);
    ui->tableView_reestr->setColumnHidden(static_cast<int>(MainDogovor::reestr::invoiceid),true);

    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::nomepizod),100);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::fio),450);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::birthday),80);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::regnom),100);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::datazakr),80);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::nomdog),100);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::namedog),430);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::nomkomp),100);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::namekomp),430);
    ui->tableView_reestr->setColumnWidth(static_cast<int>(MainDogovor::reestr::stoimost),100);

//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::nomepizod),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::birthday),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::regnom),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::datazakr),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::nomdog),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::namedog),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::nomkomp),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::namekomp),QHeaderView::Fixed);
//    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::stoimost),QHeaderView::Fixed);

    ui->tableView_reestr->horizontalHeader()->setSectionResizeMode(static_cast<int>(MainDogovor::reestr::fio),QHeaderView::Stretch);
}

void MainWindow::SetInvoiceCombobox()
{
    int currentindex;
    if(model_combobox_invoice->rowCount() == 0){
        currentindex = -1;
    }
    else{
        currentindex = ui->comboBox_invoice->currentIndex();
    }
    QSqlQuery query;
    query.prepare("SELECT 'Счет ' || invoicenum || ' за ' || case when month = 1 then 'январь'"
                                     " when month = 2 then 'февраль'"
                                     " when month = 3 then 'март'"
                                     " when month = 4 then 'апрель'"
                                     " when month = 5 then 'май'"
                                     " when month = 6 then 'июнь'"
                                     " when month = 7 then 'июль'"
                                     " when month = 8 then 'август'"
                                     " when month = 9 then 'сентябрь'"
                                     " when month = 10 then 'октябрь'"
                                     " when month = 11 then 'ноябрь'"
                                     " when month = 12 then 'декабрь' end || ' ' || to_char(year,'9999') as schet, invoiceid FROM invoice ORDER BY year DESC,month,invoicenum");

    query.exec();
    model_combobox_invoice->setQuery(query);
    ui->comboBox_invoice->setModel(model_combobox_invoice);
    ui->comboBox_invoice->setModelColumn(0);
    ui->comboBox_invoice->setCurrentIndex(currentindex);
}

void MainWindow::closeEvent(QCloseEvent *)
{
    emit Exit();
}

void MainWindow::tabWidgetChange(int index)
{
    enum currentIndex {DogovorApproved,Dogovor,Reestr};

    switch (index) {
    case DogovorApproved:
        switch (UserRole) {
        case static_cast<int>(Enums::Access::EconomistRole):
        case static_cast<int>(Enums::Access::AdminRole):
            ui->action_load_reestr->setEnabled(false);
            ui->action_add_dogovor->setEnabled(false);
            break;
        }
        break;
    case Dogovor:
        switch (UserRole) {
        case static_cast<int>(Enums::Access::EconomistRole):
        case static_cast<int>(Enums::Access::AdminRole):
            ui->action_load_reestr->setEnabled(false);
            ui->action_add_dogovor->setEnabled(true);
            break;
        }
        break;
    case Reestr:
        switch (UserRole) {
        case static_cast<int>(Enums::Access::EconomistRole):
        case static_cast<int>(Enums::Access::AdminRole):
            ui->action_load_reestr->setEnabled(true);
            ui->action_add_dogovor->setEnabled(false);
            break;
        }
        break;
    }
}

void MainWindow::qmspriceSpravochnik()
{
    spravqmsprice *d = new spravqmsprice(this);
    d->exec();
    delete d;
}

void MainWindow::economUslugiSpravochnik()
{
    spraveconomuslugi *d = new spraveconomuslugi(this);
    if(!d->SetModel()){
        return;
    }
    d->exec();
    delete d;
}

void MainWindow::complexSpravochnik()
{
    complex *d = new complex(this);
    if(!d->SetModelComplex()){
        return;
    }
    d->exec();
    delete d;
}

void MainWindow::invoiceSpravochnik()
{
    spravochnik_invoice *d = new spravochnik_invoice(this);
    d->exec();
    delete d;
    disconnect(ui->comboBox_invoice,QOverload<int>::of(&QComboBox::currentIndexChanged),this,&MainWindow::combobox_invoice_change);
    SetInvoiceCombobox();
    connect(ui->comboBox_invoice,QOverload<int>::of(&QComboBox::currentIndexChanged),this,&MainWindow::combobox_invoice_change);
    ui->tableView_reestr->setFocus();

}

void MainWindow::load()
{
    loadPrice *d = new loadPrice(this);
    if(!d->openFile()){
        return;
    }
    d->exec();
    delete d;
}

void MainWindow::addDogovor()
{
    dogovor_add *d = new dogovor_add(this);
    if(d->SetData(0,0)){
        if(d->exec()){
            SetQueryDogovor();
            QModelIndexList ind =  model_dogovor->match(model_dogovor->index(0,static_cast<int>(MainDogovor::dogovor::iddogovor)),Qt::DisplayRole,QVariant(d->GetIdDogovor()),1);
            int row = 0;
            if(!ind.isEmpty()){
                row = ind.at(0).row();
            }
            ui->tableView_dogovor->selectRow(row);
            ind =  model_complex->match(model_complex->index(0,static_cast<int>(MainDogovor::complex::idcomplex)),Qt::DisplayRole,QVariant(d->GetIdComplex()),1);
            row = 0;
            if(!ind.isEmpty()){
                row = ind.at(0).row();
            }
            ui->tableView_complex->selectRow(row);
            ui->tableView_dogovor->setFocus();
            CheckPushButtons();
        }
    }
    delete d;
}

void MainWindow::delDogovor()
{
    QString numberdogovor = model_dogovor->data(model_dogovor->index(ui->tableView_dogovor->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::numberdogovor))).toString();
    int iddogovor = model_dogovor->data(model_dogovor->index(ui->tableView_dogovor->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::iddogovor))).toInt();
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                        "<p style='text-align: center; font-size: 12pt; color: blue'>Будут удалены все комплексы и услуги<br> по договору № " + numberdogovor +
                                        "<br> Удалить договор № <span style='color: red'>" + numberdogovor + "</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    QSqlQuery query;
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.prepare("delete from dogovor cascade where iddogovor = ?");
    query.addBindValue(iddogovor);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    refreshModels();
    CheckPushButtons();
}

void MainWindow::addComplex()
{
    dogovor_add *d = new dogovor_add(this);
    int iddogovor = model_dogovor->data(model_dogovor->index(ui->tableView_dogovor->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::iddogovor)),Qt::DisplayRole).toInt();
    if(d->SetData(1,iddogovor)){
        if(d->exec()){
            SetQueryComplex();
            QModelIndexList ind =  model_complex->match(model_complex->index(0,static_cast<int>(MainDogovor::complex::idcomplex)),Qt::DisplayRole,QVariant(d->GetIdComplex()),1);
            int row = 0;
            if(!ind.isEmpty()){
                row = ind.at(0).row();
            }
            ui->tableView_complex->selectRow(row);
            ui->tableView_dogovor->setFocus();
            CheckPushButtons();
        }
    }
    delete d;
}

void MainWindow::delComplex()
{
    QString numberdogovor = model_dogovor->data(model_dogovor->index(ui->tableView_dogovor->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::numberdogovor))).toString();
    QString codecomplex = model_complex->data(model_complex->index(ui->tableView_complex->currentIndex().row(),static_cast<int>(MainDogovor::complex::codecomplex))).toString();
    int idcomplex = model_complex->data(model_complex->index(ui->tableView_complex->currentIndex().row(),static_cast<int>(MainDogovor::complex::idcomplex))).toInt();
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                        "<p style='text-align: center; font-size: 12pt; color: blue'>Будет удален комплекс № " + codecomplex + "<br> из договора № " + numberdogovor +
                                        "<br> Удалить комплекс № <span style='color: red'>" + codecomplex + "</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    QSqlQuery query;
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.prepare("delete from dogovor_complex cascade where idcomplex = ?");
    query.addBindValue(idcomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    refreshModels(ui->tableView_dogovor->currentIndex().row());
    CheckPushButtons();
}

void MainWindow::editComplex()
{
    int row =ui->tableView_complex->currentIndex().row();
    int idcomplex = model_complex->data(model_complex->index(row,static_cast<int>(MainDogovor::complex::idcomplex))).toInt();
    if(idcomplex == 0){
        return;
    }
    complex_edit *d = new complex_edit(this);

    if(!d->SetComplexData(idcomplex) || !d->exec()){
        delete d;
        return;
    }
    SetQueryComplex();
    ui->tableView_complex->selectRow(row);
    ui->tableView_complex->setFocus();
}

void MainWindow::addService()
{
    int idcomplex = model_complex->data(model_complex->index(ui->tableView_complex->currentIndex().row(),static_cast<int>(MainDogovor::complex::idcomplex))).toInt();
    complex_add_new_service *d = new complex_add_new_service(this,idcomplex,2);
    connect(d,&complex_add_new_service::addServiceInDogovor,this,&MainWindow::SignalAddService);
    d->exec();
    disconnect(d,&complex_add_new_service::addServiceInDogovor,this,&MainWindow::SignalAddService);
    delete d;
}

void MainWindow::delService()
{
    QString numberdogovor = model_dogovor->data(model_dogovor->index(ui->tableView_dogovor->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::numberdogovor))).toString();
    QString codecomplex = model_complex->data(model_complex->index(ui->tableView_complex->currentIndex().row(),static_cast<int>(MainDogovor::complex::codecomplex))).toString();
    QString code_service = model_services->data(model_services->index(ui->tableView_services->currentIndex().row(),static_cast<int>(MainDogovor::services::code_service))).toString();
    int idservicecomplex = model_services->data(model_services->index(ui->tableView_services->currentIndex().row(),static_cast<int>(MainDogovor::services::idservicecomplex))).toInt();
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                        "<p style='text-align: center; font-size: 12pt; color: blue'>Будет удалена услуга  " + code_service + "<br> из комплекса № " + codecomplex +
                                        " договора № " + numberdogovor + "<br> Удалить услугу № <span style='color: red'>" + code_service + "</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    QSqlQuery query;
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.prepare("delete from dogovor_complex_services where idservicecomplex = ?");
    query.addBindValue(idservicecomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    refreshModels(ui->tableView_dogovor->currentIndex().row(),ui->tableView_complex->currentIndex().row(),0);
    CheckPushButtons();

}

void MainWindow::SignalAddService(int idservicecomplex)
{
    SetModelServices();
    QModelIndexList list = model_services->match(model_services->index(0,static_cast<int>(MainDogovor::services::idservicecomplex)),Qt::DisplayRole,QVariant(QString::number(idservicecomplex)),1,Qt::MatchFixedString);
    if(!list.isEmpty()){
        ui->tableView_services->selectRow(list.at(0).row());
    }
    CheckPushButtons();
}

void MainWindow::ChangeDataDogovor(QModelIndex )
{
    SetQueryComplex();
    ui->tableView_complex->selectRow(0);
    ui->tableView_services->selectRow(0);
    ui->tableView_dogovor->setFocus();

    QModelIndex index = ui->tableView_dogovor->currentIndex();
    QString str = " Договор № " + model_dogovor->data(model_dogovor->index(index.row(),static_cast<int>(MainDogovor::dogovor::numberdogovor))).toString() +
            " (" + model_dogovor->data(model_dogovor->index(index.row(),static_cast<int>(MainDogovor::dogovor::namekontragent))).toString();
    ui->groupBox_complex->setTitle(str);
}

void MainWindow::ChangeDataComplex(QModelIndex )
{
    SetQueryServices();
    ui->tableView_services->selectRow(0);
    ui->tableView_complex->setFocus();

    QModelIndex index = ui->tableView_complex->currentIndex();
    QString str = " Комплекс № " + model_complex->data(model_complex->index(index.row(),static_cast<int>(MainDogovor::complex::codecomplex))).toString() +
            " (" + model_complex->data(model_complex->index(index.row(),static_cast<int>(MainDogovor::complex::namecomplex))).toString();
    ui->groupBox_services->setTitle(str);
}

void MainWindow::ChangeDataDogovorApproved(QModelIndex index)
{
    QSqlQuery query;
    if(!index.isValid()){
        return;
    }
    int iddogovor = model_confirmed->data(model_confirmed->index(index.row(),static_cast<int>(MainDogovor::dogovor::iddogovor))).toInt();
    // Формирование Дерева
    query.prepare(""
                  "select codecomplex || ' (' || codecomplexqms || ' )', namecomplex, to_char(stoimost,'999999990.00') || ' руб.', dogovor_complex.idcomplex, code_service, name_usl, ' ' ,idservicecomplex  from dogovor_complex"
                  "  left join dogovor_complex_services on dogovor_complex.idcomplex = dogovor_complex_services.idcomplex"
                  "  left join econom_usl u on dogovor_complex_services.ideconom = u.id"
                  "  left join qmspriceusl q on dogovor_complex_services.idprice = q.id"
                  "  left join dogovor using(iddogovor)"
                  " where iddogovor = ? order by codecomplex,namecomplex,name_usl"
                  "");
    query.addBindValue(iddogovor);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    QList<QVariant> title ;
    title<< "Код комплекса" << "Наименование комплекса" << "Стоимость" << "id";
    if(model_tree != nullptr){
        delete model_tree;
    }
    model_tree = new TreeModel(title,this);
    ui->treeView->setModel(model_tree);
    ui->treeView->setColumnHidden(3,true);
    ui->treeView->setColumnWidth(0,200);
    //ui->treeView->setColumnWidth(1,650);
    ui->treeView->setColumnWidth(2,100);
    model_tree->SetQuery(query);
    ui->treeView->resizeColumnToContents(1);
}

void MainWindow::SetQueryDogovor()
{
    QSqlQuery query;
    query.prepare("select * from dogovor where approved isnull and changed isnull and closed isnull ");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    model_dogovor->setQuery(query);
}

void MainWindow::SetQueryComplex()
{
    QModelIndex index = ui->tableView_dogovor->currentIndex();
    int iddogovor = model_dogovor->data(model_dogovor->index(index.row(),static_cast<int>(MainDogovor::dogovor::iddogovor))).toInt();
    QSqlQuery query;
    query.prepare("select idcomplex, iddogovor, codecomplex, namecomplex,  to_char(stoimost,'999999990.00') || ' руб.' from dogovor_complex where iddogovor = ? order by codecomplex");
    query.addBindValue(iddogovor);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    model_complex->setQuery(query);
}

void MainWindow::SetQueryServices()
{
    QModelIndex index = ui->tableView_complex->currentIndex();
    int idcomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(MainDogovor::complex::idcomplex))).toInt();
    QSqlQuery query;
    query.prepare("select idservicecomplex,idcomplex,ideconom,idprice,code_service,name_usl,code_price,name_price from dogovor_complex_services"
                  " left join econom_usl on econom_usl.id = ideconom"
                  " left join qmspriceusl on qmspriceusl.id = idprice"
                  " where idcomplex = ? order by name_usl");
    query.addBindValue(idcomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    model_services->setQuery(query);
}

void MainWindow::SetQueryConfirmed()
{
    QSqlQuery query;
    query.prepare("select * from dogovor where approved notnull or changed notnull or closed notnull order by numberdogovor, approved, changed, closed");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    model_confirmed->setQuery(query);
}

void MainWindow::refreshButton()
{
    disconnect(ui->tableView_dogovor,&MyTableView_dogovor::changeData_dogovor,this,&MainWindow::ChangeDataDogovor);
    disconnect(ui->tableView_complex,&MyTableView_complex::changeData_complex,this,&MainWindow::ChangeDataComplex);
    disconnect(ui->tableView_confirmed,&MyTableView_dogovor_approved::changeData_dogovor_approved,this,&MainWindow::ChangeDataDogovorApproved);


    SetModelDogovorConfirmed();
    SetModelDogovor();
    SetModelComplex();
    SetModelServices();
    SetModelReestr();

    SetInvoiceCombobox();

    CheckPushButtons();

    ChangeDataDogovorApproved(ui->tableView_confirmed->currentIndex());

    connect(ui->tableView_dogovor,&MyTableView_dogovor::changeData_dogovor,this,&MainWindow::ChangeDataDogovor);
    connect(ui->tableView_complex,&MyTableView_complex::changeData_complex,this,&MainWindow::ChangeDataComplex);
    connect(ui->tableView_confirmed,&MyTableView_dogovor_approved::changeData_dogovor_approved,this,&MainWindow::ChangeDataDogovorApproved);
}

void MainWindow::refreshModels(int dogovor_row, int complex_row, int service_row)
{
    SetQueryDogovor();
    if(model_dogovor->rowCount() == 0){
        SetQueryComplex();
        if(model_complex->rowCount() == 0){
            SetQueryServices();
        }
    }else{
        ui->tableView_dogovor->selectRow(dogovor_row);
        if(model_complex->rowCount() == 0){
            SetQueryServices();
        }
        else{
            ui->tableView_complex->selectRow(complex_row);
            if(model_services->rowCount() == 0){
                SetQueryServices();
            }
            else{
                ui->tableView_services->selectRow(service_row);
            }
        }
    }
    CheckPushButtons();
}

void MainWindow::CheckPushButtons()
{
    ui->pushButton_approved->setEnabled(true);
    if(model_dogovor->rowCount() == 0){
        ui->pushButton_del_dogovor->setEnabled(false);
        ui->pushButton_add_complex->setEnabled(false);
        ui->pushButton_del_complex->setEnabled(false);
        ui->pushButton_add_service->setEnabled(false);
        ui->pushButton_del_service->setEnabled(false);
        ui->pushButton_approved->setEnabled(false);

    }
    else{
        ui->pushButton_del_dogovor->setEnabled(true);
        ui->pushButton_add_complex->setEnabled(true);
    }
    if (model_complex->rowCount() == 0) {
        ui->pushButton_del_complex->setEnabled(false);
        ui->pushButton_add_service->setEnabled(false);
        ui->pushButton_del_service->setEnabled(false);
        ui->pushButton_edit_complex->setEnabled(false);
        ui->pushButton_approved->setEnabled(false);
    }
    else{
        ui->pushButton_del_complex->setEnabled(true);
        ui->pushButton_add_service->setEnabled(true);
        ui->pushButton_edit_complex->setEnabled(true);
    }
    if(model_services->rowCount() == 0){
        ui->pushButton_del_service->setEnabled(false);
        ui->pushButton_approved->setEnabled(false);
    }
    else{
        ui->pushButton_del_service->setEnabled(true);
    }
}

void MainWindow::DogovorApproved()
{
    QSqlQuery query, query_services;
    int iddogovor_confirmed = 0;
    int iddogovor = model_dogovor->data(model_dogovor->index(ui->tableView_dogovor->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::iddogovor))).toInt();
    QString numberdogovor = model_dogovor->data(model_dogovor->index(ui->tableView_dogovor->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::numberdogovor))).toString();
    query.prepare("select * from dogovor_complex where iddogovor = ? order by codecomplex");
    query.addBindValue(iddogovor);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(!query.first()){
        QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                                "<br> Нельзя подтвердить пустой договор № <span style='color: red'>" + numberdogovor + "</p>");
        return;
    }
    do{
       int idcomplex = query.value("idcomplex").toInt();
       QString codecomplex = query.value("codecomplex").toString();
       query_services.prepare("select * from dogovor_complex_services where idcomplex = ?");
       query_services.addBindValue(idcomplex);
       if(!query_services.exec()){
           QMessageBox::critical(this,"Ошибка",
                           "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
           return;
       }
       if(!query_services.first()){
           QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                                   "<p style='text-align: center; font-size: 12pt; color: blue'>В комплексе № " + codecomplex + " отсутствуют услуги."
                                                   "<br> Нельзя подтвердить договор № <span style='color: red'>" + numberdogovor + "</p>");
           return;
       }
    }while(query.next());

    query.prepare("select * from dogovor where numberdogovor = ? and approved notnull and changed isnull and closed isnull");
    query.addBindValue(numberdogovor);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(query.first()){
        iddogovor_confirmed = query.value("iddogovor").toInt();
    }
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {
        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p style='text-align: center'>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return;
        }
        else{
            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            query.exec();
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                return;
            }
            if(iddogovor_confirmed != 0){
                query.prepare("update dogovor set changed = ? where iddogovor = ?");
                query.addBindValue(QDateTime::currentDateTime());
                query.addBindValue(iddogovor_confirmed);
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                    db.rollback();
                    return;
                }

            }
            query.prepare("update dogovor set approved = ? where iddogovor = ?");
            query.addBindValue(QDateTime::currentDateTime());
            query.addBindValue(iddogovor);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                db.rollback();
                return;
            }
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Транзакция не может быть выполнена!<br><span style='color: red'>" + query.lastError().text() +"</p>");
                db.rollback();
                return;
            }
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Транзакции не поддерживаются в используемой СУБД!</p>");
        return ;
    }

    QMessageBox mbox(this);
    mbox.setWindowTitle(tr("Внимание"));
    mbox.setText("Договор подтвержден.");
    mbox.setModal(true);
    mbox.setStandardButtons(QMessageBox::Ok);
    mbox.setIcon(QMessageBox::Information);
    QTimer::singleShot(1500,&mbox,SLOT(accept()));
    mbox.exec();

    refreshModels();
    SetQueryConfirmed();
    QModelIndex index = QModelIndex();
    QModelIndexList listIndex = model_confirmed->match(model_confirmed->index(0,static_cast<int>(MainDogovor::dogovor::iddogovor)),Qt::DisplayRole,QVariant(iddogovor),1,Qt::MatchFixedString);
    if(!listIndex.isEmpty()){
        index = listIndex.at(0);
    }

    ui->tableView_confirmed->selectRow(index.row());
    SendMail(iddogovor);

}

void MainWindow::DogovorClose()
{

    if(!model_confirmed->data(model_confirmed->index(ui->tableView_confirmed->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::changed))).toString().isEmpty()){
        QMessageBox::warning(this,"Внимание",
                              "<p style='text-align: center'>Нельзя закрыть изменения договора!</p>");
        return;
    }
    else if(!model_confirmed->data(model_confirmed->index(ui->tableView_confirmed->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::closed))).toString().isEmpty()){
        QMessageBox::warning(this,"Внимание",
                              "<p style='text-align: center'>Договор уже закрыт!</p>");
        return;
    }
    int iddogovor = model_confirmed->data(model_confirmed->index(ui->tableView_confirmed->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::iddogovor))).toInt();
    InpuDateDialog *d = new InpuDateDialog(this);
    d->SetDataDialog("Дата закрытия Договора");
    if(!d->exec()){
        delete d;
        return;
    }
    QDateTime datatime(d->GetDate(),QTime::currentTime());
    delete d;
    QSqlQuery query;
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.prepare("update dogovor set closed = ? where iddogovor = ?");
    query.addBindValue(datatime);
    query.addBindValue(iddogovor);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    SetQueryConfirmed();
    ui->tableView_confirmed->selectRow(ui->tableView_confirmed->currentIndex().row());
    ui->tableView_confirmed->setFocus();
}

void MainWindow::DogovorEdit()
{

    if(!model_confirmed->data(model_confirmed->index(ui->tableView_confirmed->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::changed))).toString().isEmpty()){
        QMessageBox::warning(this,"Внимание",
                             "<p style='text-align: center'>Нельзя править изменения договора!</p>");
        return;
    }
    else if(!model_confirmed->data(model_confirmed->index(ui->tableView_confirmed->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::closed))).toString().isEmpty()){
        QMessageBox::warning(this,"Внимание",
                             "<p style='text-align: center'>Договор уже закрыт!</p>");
        return;
    }
    QString numberdogovor = model_confirmed->data(model_confirmed->index(ui->tableView_confirmed->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::numberdogovor))).toString();
    if(model_dogovor->match(model_dogovor->index(0,static_cast<int>(MainDogovor::dogovor::numberdogovor)),Qt::DisplayRole,QVariant(numberdogovor),1,Qt::MatchFixedString).length() !=0 ){
        QMessageBox::warning(this,"Внимание",
                             "<p style='text-align: center'>Договор с номером № <span style='color: blue'>" + numberdogovor + "<span style='color: black'><br> есть в рабочих договорах!</p>");
        return;
    }

    int n = QMessageBox::warning(nullptr,"Внимание!","<p style='text-align: center'>Текущий договор с номером № <span style='color: blue'>" + numberdogovor +
                                 "<br><span style='color: black'>будет скопирован в рабочие договора.<br><span style='color: red'> Изменить номер договора и название нельзя!"
                                 "<br>Можно изменить комплексы и услуги в комплексах!</p>",
                         QMessageBox::Yes|QMessageBox::No,
                         QMessageBox::Yes);
    if (n != QMessageBox::Yes){
        return;
    }

    int iddogovor = model_confirmed->data(model_confirmed->index(ui->tableView_confirmed->currentIndex().row(),static_cast<int>(MainDogovor::dogovor::iddogovor))).toInt();
    QSqlQuery query;

    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {
        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p style='text-align: center'>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return;
        }
        else{
            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                db.rollback();
                return;
            }
            query.prepare("with w_row as(select numberdogovor, namekontragent, start, finish from dogovor where iddogovor = ?)"
                          " insert into dogovor (numberdogovor, namekontragent, start, finish) select * from w_row returning iddogovor");
            query.addBindValue(iddogovor);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                db.rollback();
                return;
            }
            query.first();
            int iddogovor_new = query.value("iddogovor").toInt();
            query.prepare("select * from dogovor_complex where iddogovor = ?");
            query.addBindValue(iddogovor);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                db.rollback();
                return;
            }
            query.first();
            // Вставляем копии комплексов из договора
            do{
                QSqlQuery query_complex;
                //  вставляем копию записи комплекса и получаем новый idcomplex_new
                query_complex.prepare("insert into dogovor_complex (iddogovor, codecomplex, namecomplex, stoimost, codecomplexqms) values(?,?,?,?,?) returning idcomplex");
                query_complex.addBindValue(iddogovor_new);
                query_complex.addBindValue(query.value("codecomplex"));
                query_complex.addBindValue(query.value("namecomplex"));
                query_complex.addBindValue(query.value("stoimost"));
                query_complex.addBindValue(query.value("codecomplexqms"));
                if(!query_complex.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p style='text-align: center'>Ошибка выполнения запроса insert into dogovor_complex.<br><span style='color: red'>" + query_complex.lastError().text() +"</p>");
                    db.rollback();
                    return;
                }
                query_complex.first();
                int idcomplex_new = query_complex.value("idcomplex").toInt();
                // получаем список услуг из старого ( копируемого ) комплекса
                query_complex.prepare("select * from dogovor_complex_services where idcomplex = ?");
                query_complex.addBindValue(query.value("idcomplex"));
                if(!query_complex.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p style='text-align: center'>Ошибка выполнения запроса .<br><span style='color: red'>" + query_complex.lastError().text() +"</p>");
                    db.rollback();
                    return;
                }
                query_complex.first();
                QSqlQuery query_services;
                do{
                    query_services.prepare("insert into dogovor_complex_services(idcomplex,ideconom,idprice,code_service) values(?,?,?,?)");
                    query_services.addBindValue(idcomplex_new);
                    query_services.addBindValue(query_complex.value("ideconom"));
                    query_services.addBindValue(query_complex.value("idprice"));
                    query_services.addBindValue(query_complex.value("code_service"));
                    if(!query_services.exec()){
                        QMessageBox::critical(this,"Ошибка",
                                              "<p style='text-align: center'>Ошибка выполнения запроса .<br><span style='color: red'>" + query_services.lastError().text() +"</p>");
                        db.rollback();
                        return;
                    }
                }while(query_complex.next());// пока не переберем все услуги комплекса который копируем


            }while(query.next());   // пока не переберем все комплексы договора который копируем
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Транзакция не может быть выполнена!<br><span style='color: red'>" + query.lastError().text() +"</p>");
                db.rollback();
                return;
            }
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Транзакции не поддерживаются в используемой СУБД!</p>");
        return ;
    }

    QMessageBox mbox(this);
    mbox.setWindowTitle(tr("Внимание"));
    mbox.setText("Договор скопирован в рабочие.");
    mbox.setModal(true);
    mbox.setStandardButtons(QMessageBox::Ok);
    mbox.setIcon(QMessageBox::Information);
    QTimer::singleShot(1500,&mbox,SLOT(accept()));
    mbox.exec();

    refreshModels();
}

void MainWindow::SetSmtpServers()
{
    set_smtp_server *d = new set_smtp_server(this);
    if(d->SetSqlQueryModel()){
        d->exec();
    }
    delete d;
}

void MainWindow::SetRecipients()
{
    recipients *d = new recipients(this);
    if(d->SetSqlQueryModel()){
        d->exec();
    }
    delete d;

}

void MainWindow::DoubleClickedDogovor(QModelIndex index)
{
    int iddogovor = model_confirmed->data(model_confirmed->index(index.row(),static_cast<int>(MainDogovor::dogovor::iddogovor))).toInt();
    QString filename = FormFileDogovor(iddogovor);
    if(filename.isEmpty()){
        return;
    }
    if(QFile::exists(filename))
    {

        if(!QDesktopServices::openUrl(QUrl(QUrl::fromLocalFile(filename))))
//            if(!QDesktopServices::openUrl(QUrl(string)))
        {

            QMessageBox::critical(this,"Ошибка",
                                  "<p style='text-align: center; color: red'>Ошибка открытия файла: <span style='color: blue'>" + filename + "</p>");
            return;

        }
    }
    else
    {
        QMessageBox::critical(this,
                              QString::fromUtf8("Ошибка !"),
                              QString::fromUtf8("<p style='text-align: center; color: red'>Файл не найден!<br>"
                                                "<span style='color: black'>Обратитесь к Администратору</p>"));
        return;
    }
}

void MainWindow::loadReestr()
{

    enum file_reestr {id,nomepizod,fio,birthday,regnom,datazakr,nomdog,nomkomp};
    QSqlQuery query;
    query.prepare("select numberdogovor,namekontragent,codecomplex,namecomplex,to_char(stoimost,'999999990.00') stoimost, codecomplexqms from dogovor left join dogovor_complex using(iddogovor) where approved notnull and changed isnull and closed isnull");
    query.exec();

    QString file_name = QFileDialog::getOpenFileName(this,"Выбор файла","c:\\TMP\\","Документы (*.csv)");
    if(file_name.isEmpty())
        return;
//    int n = QMessageBox::warning(this,"Внимание!","<p align = 'center'>Загрузить файл :<br><font color = red>'" +file_name.section("/",-1)+"'?",
//                         QMessageBox::Yes|QMessageBox::No,
//                         QMessageBox::Yes);
//    if (n != QMessageBox::Yes)
//        return;

    QFile file(file_name);
    file.open(QIODevice::ReadOnly);

    QByteArray data = file.readAll();
    file.close();

//    QTextCodec *codec = QTextCodec::codecForName("KOI8-R");
//    QTextCodec *codec = QTextCodec::codecForName("KOI8-R");
    QTextCodec *codec = QTextCodec::codecForName("Windows-1251");
//    QTextCodec *codec = QTextCodec::codecForName("UTF-8");

    QString string = codec->toUnicode(data);
    QStringList lines = string.split(QString("\r\n"));
    lines.removeLast();
    lines.removeLast();
    lines.removeLast();
    int rem = 0;
    for(int i = 0; i < 10; i++){
        QStringList list_lines = lines.at(i).split(";");
        if(list_lines.at(0) == "№"){
            break;
        }
        else {
           rem++;
        }
    }
    for(int i = 0; i < rem; i++){
        lines.removeFirst();
    }
    rem = 0;
    QStringList *stringlist;
    stringlist =  &lines;

    QString str = stringlist->at(0);

//    int columnCount = 11;
//    model_reestr->setColumnCount(columnCount);
    int rowCount =  stringlist->length();
    QProgressDialog *ppwt = new QProgressDialog("Загрузка данных ...","Отмена",0,rowCount,this);
    ppwt->setWindowTitle("Wait...");
    ppwt->setMinimumDuration(0);
    ppwt->setModal(true);
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {
        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!</p>");
            return;
        }
        else
        {
            QSqlQuery query_reestr;

                for(int row = 1; row < rowCount; row++){
                    str = stringlist->at(row);
                    str = str.replace("_","");
                    QStringList listColumn = str.split(";");
                    if(listColumn.at(file_reestr::nomdog).isEmpty() || listColumn.at(file_reestr::nomkomp).isEmpty()){
                        ppwt->setValue(row);
                        continue;
                    }
                    query_reestr.prepare("insert into reestr (nomepizod, fio, birthday, regnom, datazakr, nomdog, namedog, nomkomp, namekomp, stoimost)"
                                         " values (?,?,?,?,?,?,?,?,?,?)");
                    query_reestr.addBindValue(listColumn.at(file_reestr::nomepizod));
                    query_reestr.addBindValue(listColumn.at(file_reestr::fio));
                    query_reestr.addBindValue(listColumn.at(file_reestr::birthday));
                    query_reestr.addBindValue(listColumn.at(file_reestr::regnom));
                    query_reestr.addBindValue(listColumn.at(file_reestr::datazakr));
                    query_reestr.addBindValue(listColumn.at(file_reestr::nomdog));
                    query.first();
                    bool find = false;
                    do{
                        QString nomdog = query.value("numberdogovor").toString();
                        QString codecomplexqms = query.value("codecomplexqms").toString();
                        if(nomdog == listColumn.at(file_reestr::nomdog) && codecomplexqms == listColumn.at(file_reestr::nomkomp)){
                            query_reestr.addBindValue(query.value("namekontragent"));
                            query_reestr.addBindValue(query.value("codecomplex"));
                            query_reestr.addBindValue(query.value("namecomplex"));
                            query_reestr.addBindValue(query.value("stoimost"));
                            find = true;
                            break;
                        }
                    }while(query.next());
                    if(!find){
                       continue;
                    }
                    if(!query_reestr.exec()){
                        QMessageBox::critical(this,"Ошибка",
                                              "<p align=center>Ошибка выполнения запроса добавления записи в реестр.<br><font color=red>" + query_reestr.lastError().text() +"</font></p>");
                        db.rollback();
                        delete ppwt;
                        return;
                    }
                    ppwt->setValue(row);
                    qApp->processEvents();
                    if(ppwt->wasCanceled()){
                        break;
                    }
                }
        }
        ppwt->setValue(rowCount);
        qApp->processEvents();
        if(!db.commit()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
            db.rollback();
            delete ppwt;
            return;
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        delete ppwt;
        return;
    }
    if(ui->comboBox_invoice->currentIndex() != -1){
        ui->comboBox_invoice->setCurrentIndex(-1);
    }
    else{
        SetModelReestr();
    }
    delete ppwt;
}

void MainWindow::printReestr()
{
    QVariant s;
    int rowoffset = 1;
    QXlsx::Document xlsx;
    QXlsx::Format formatHeader, formatLeft,formatRigth, formatCenter, formatMoney, formatData,formatNumber;
    QXlsx::Format formatLeftTextBold,formatRigthText;
    QXlsx::CellRange range;

    QString filename,str;
    QString winpath;
    winpath = "/Downloads/";
    str = "Реестр" ;

    filename = QDir::homePath() + winpath + str + "_" + QDateTime::currentDateTime().toString("ddMMyyyyHHmm") + ".xlsx";;
    //            + "_" + QDateTime::currentDateTime().toString("ddMMyyyyHHmm") + ".xlsx";

    formatHeader.setFontColor(QColor(Qt::blue));
    formatHeader.setFontSize(16);
    formatHeader.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatHeader.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatHeader.setBorderStyle(QXlsx::Format::BorderThin);
    formatHeader.setTextWarp(true);

    formatLeft.setFontColor(QColor(Qt::black));
    formatLeft.setFontSize(12);
    formatLeft.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeft.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatLeft.setBorderStyle(QXlsx::Format::BorderThin);
    formatLeft.setTextWarp(true);

    formatRigth.setFontColor(QColor(Qt::black));
    formatRigth.setFontSize(12);
    formatRigth.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigth.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatRigth.setBorderStyle(QXlsx::Format::BorderThin);

    formatCenter.setFontColor(QColor(Qt::black));
    formatCenter.setFontSize(12);
    formatCenter.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatCenter.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatCenter.setBorderStyle(QXlsx::Format::BorderThin);
    formatCenter.setTextWarp(true);

    formatMoney.setFontColor(QColor(Qt::black));
    formatMoney.setFontSize(12);
    formatMoney.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatMoney.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatMoney.setBorderStyle(QXlsx::Format::BorderThin);
    formatMoney.setNumberFormatIndex(8);

    formatData.setFontColor(QColor(Qt::black));
    formatData.setFontSize(12);
    formatData.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatData.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatData.setBorderStyle(QXlsx::Format::BorderThin);
    formatData.setNumberFormat("dd.mm.yyyy");

    formatNumber.setFontColor(QColor(Qt::black));
    formatNumber.setFontSize(12);
    formatNumber.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatNumber.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatNumber.setBorderStyle(QXlsx::Format::BorderThin);
    formatNumber.setNumberFormatIndex(2);


    formatLeftTextBold.setFontColor(QColor(Qt::black));
    formatLeftTextBold.setFontName("Times New Roman");
    formatLeftTextBold.setFontSize(14);
    formatLeftTextBold.setFontBold(true);
    formatLeftTextBold.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeftTextBold.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    formatRigthText.setFontColor(QColor(Qt::black));
    formatRigthText.setFontSize(12);
    formatRigthText.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigthText.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    int col = 0;
    {
        s = QString("№ п.п.");
        xlsx.setColumnWidth(col+1,10);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;

        s = QString("ФИО");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(rowoffset,col+1,s,formatHeader);col++;


        s = QString("Дата рождения");
        xlsx.setColumnWidth(col+1,15);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;

        s = QString("Рег.№");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;
        s = QString("Дата закрытия");
        xlsx.setColumnWidth(col+1,15);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;
        s = QString("Договор №");
        xlsx.setColumnWidth(col+1,15);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;
        s = QString("Контрагент");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;
        s = QString("Комплекс №");
        xlsx.setColumnWidth(col+1,15);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;
        s = QString("Комплекс (наименование)");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;
        s = QString("Сумма");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(rowoffset, col+1, s, formatHeader);col++;
    }
    rowoffset++;
    for(int i = 0; i < model_reestr->rowCount(); i++){

        xlsx.write(i+rowoffset, 1, QString::number(i+1), formatCenter);

        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::fio))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::fio), s, formatCenter);

        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::birthday))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::birthday), s, formatData);

        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::regnom))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::regnom), s, formatCenter);

        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::datazakr))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::datazakr), s, formatData);

        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::nomdog))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::nomdog), s, formatCenter);
        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::namedog))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::namedog), s, formatCenter);

        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::nomkomp))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::nomkomp), s, formatCenter);
        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::namekomp))).toString();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::namekomp), s, formatCenter);

        s = model_reestr->data(model_reestr->index(i,static_cast<int>(MainDogovor::reestr::stoimost))).toString().trimmed();
        xlsx.write(i+rowoffset, static_cast<int>(MainDogovor::reestr::stoimost), s, formatNumber);

    }

    if(xlsx.saveAs(filename)){
        //        filename = "Данные успешно сохранены.\n" + filename;
        //        QMessageBox *mbox = new QMessageBox(this);
        //        mbox->setWindowTitle(tr("Внимание"));
        //        mbox->setText(filename);
        //        mbox->setModal(true);
        //        mbox->setStandardButtons(QMessageBox::Ok);
        //        mbox->setIcon(QMessageBox::Information);
        //        QTimer::singleShot(5000,mbox,SLOT(accept()));
        //        mbox->exec();
        //**** Проверка на доступность файла *************************
    }
    else
    {
        filename = "Ошибка записи файла.\n" + filename;
        QMessageBox mbox(this);
        mbox.setWindowTitle(tr("Внимание"));
        mbox.setText(filename);
        mbox.setModal(true);
        mbox.setStandardButtons(QMessageBox::Ok);
        mbox.setIcon(QMessageBox::Critical);
        QTimer::singleShot(3000,&mbox,SLOT(accept()));
        mbox.exec();
        return;
    }


    if(!QDesktopServices::openUrl(QUrl(QUrl::fromLocalFile(filename))))
//            if(!QDesktopServices::openUrl(QUrl(string)))
    {

        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center; color: red'>Ошибка открытия файла: <span style='color: blue'>" + filename + "</p>");
    }


    return;
}

void MainWindow::delReestrRecords()
{
    QModelIndexList indexlist = ui->tableView_reestr->selectionModel()->selectedRows(static_cast<int>(MainDogovor::reestr::idreestr));
    if(indexlist.length() == 1){
        int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 12pt; color: red'> ВНИМАНИЕ !</p>"
                                            "<p style='text-align: center; font-size: 12pt; color: black'>Удалить текущую запись?</p>",
                                     QMessageBox::Yes|QMessageBox::No,
                                     QMessageBox::No);
        if (n != QMessageBox::Yes){
            return;
        }

    }
    else if(indexlist.length() > 1){
        int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 12pt; color: red'> ВНИМАНИЕ !</p>"
                                            "<p style='text-align: center; font-size: 12pt; color: black'>Удалить все отмеченные записи<span style='color: blue'><br>"
                                     + QString::number(indexlist.length()) + "<span style='color: black'> шт. ?</p>",
                                     QMessageBox::Yes|QMessageBox::No,
                                     QMessageBox::No);
        if (n != QMessageBox::Yes){
            return;
        }
    }
    else{
        return;
    }
    QSqlQuery query;
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {
        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!</p>");
            return;
        }
        else
        {
            QModelIndex index;
            for(int i = 0; i < indexlist.length(); i++){
                index = indexlist.at(i);
                query.prepare("delete from reestr where idreestr = ?");
                query.addBindValue(model_reestr->data(index).toInt());
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p align=center>Ошибка удаления записи из реестра.<br><font color=red>" + query.lastError().text() +"</font></p>");
                    db.rollback();
                    return;
                }
            }
        }
        if(!db.commit()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
            db.rollback();
            return;
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return;
    }
    SetModelReestr();
}

void MainWindow::combobox_invoice_change()
{
    SetModelReestr();
}

void MainWindow::combobox_clear_change()
{
    ui->comboBox_invoice->setCurrentIndex(-1);
}


void MainWindow::showContextMenu(QPoint pos)
{
    if(model_reestr->rowCount() == 0){
        return;
    }
    QMenu contextMenu(this);

    if(ui->comboBox_invoice->currentIndex() != -1){
        contextMenu.addAction("Привязать выьранные к периоду",this,&binding);
        contextMenu.addAction("Отвязать выбранные от периода",this,&unbinding);
    }
    else{
        contextMenu.addAction("Привязать выьранные к периоду",this,&binding);
    }
    contextMenu.exec(ui->tableView_reestr->mapToGlobal(pos));
}

void MainWindow::binding()
{
    selectinvoice *d = new selectinvoice(this);
    if(d->exec()){
        int invoiceid = d->GetInvoiceId();
        delete d;
        QModelIndexList indexlist = ui->tableView_reestr->selectionModel()->selectedRows(static_cast<int>(MainDogovor::reestr::idreestr));
        QSqlQuery query;
        if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
        {
            if(!db.transaction())
            {
                QMessageBox::warning(this,"Внимание",
                                     "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                     "обратитесь к Администратору!</p>");
                return;
            }
            else
            {
                QModelIndex index;
                for(int i = 0; i < indexlist.length(); i++){
                    index = indexlist.at(i);
                    query.prepare("update reestr set invoiceid = ? where idreestr = ?");
                    query.addBindValue(invoiceid);
                    query.addBindValue(model_reestr->data(index).toInt());
                    if(!query.exec()){
                        QMessageBox::critical(this,"Ошибка",
                                              "<p align=center>Ошибка привязки записи из реестра.<br><font color=red>" + query.lastError().text() +"</font></p>");
                        db.rollback();
                        return;
                    }
                }
            }
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
        }
        else
        {
            QMessageBox::critical(this,"Ошибка",
                                  "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
            return;
        }
        SetModelReestr();
    }
    else{
        delete d;
    }
    return;
}

void MainWindow::unbinding()
{
    QModelIndexList indexlist = ui->tableView_reestr->selectionModel()->selectedRows(static_cast<int>(MainDogovor::reestr::idreestr));
    QSqlQuery query;
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {
        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!</p>");
            return;
        }
        else
        {
            QModelIndex index;
            for(int i = 0; i < indexlist.length(); i++){
                index = indexlist.at(i);
                query.prepare("update reestr set invoiceid = null where idreestr = ?");
                query.addBindValue(model_reestr->data(index).toInt());
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p align=center>Ошибка отвязки записи из реестра.<br><font color=red>" + query.lastError().text() +"</font></p>");
                    db.rollback();
                    return;
                }
            }
        }
        if(!db.commit()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
            db.rollback();
            return;
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return;
    }
    SetModelReestr();
}

void MainWindow::Exit()
{
    emit exit();
}

