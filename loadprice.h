#ifndef LOADPRICE_H
#define LOADPRICE_H

#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QXmlStreamReader>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QDebug>
#include <QProgressDialog>
#include <QSqlQuery>
#include <QSqlError>

namespace Ui {
class loadPrice;
}

class loadPrice : public QDialog
{
    Q_OBJECT

public:
    explicit loadPrice(QWidget *parent = nullptr);
    ~loadPrice();

    bool openFile();
    void parser_XMLFile(QXmlStreamReader *xmlReader);

private slots:

    void apply();

private:
    Ui::loadPrice *ui;
    int Count = 0;
    QStandardItemModel *model;
};

#endif // LOADPRICE_H
