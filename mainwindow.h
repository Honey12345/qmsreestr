#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDesktopWidget>
#include <QFile>
#include <QFileDialog>
#include <QXmlStreamReader>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QDateTime>
#include <QDesktopServices>
#include <QTextCodec>
#include <QByteArray>
#include "treeview.h"
#include <QStandardItemModel>
#include <QMenu>
#include <QAction>
#include <QThread>


class MainDogovor : public QObject {
public:
    // достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int

    enum class dogovor :int {iddogovor,numberdogovor,namekontragent,start,finish,approved,changed,closed};
    Q_ENUM(dogovor)


    enum class complex :int {idcomplex,iddogovor,codecomplex,namecomplex,stoimost};
    Q_ENUM(complex)

    enum class services :int {idservicecomplex,idcomplex,ideconom,idprice,code_service,name_usl,code_price,name_price};
    Q_ENUM(services)

    enum class reestr :int {idreestr,nomepizod,fio,birthday,regnom,datazakr,nomdog,namedog,nomkomp,namekomp,stoimost,invoiceid};
    Q_ENUM(reestr)


    Q_OBJECT
    MainDogovor() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums
};

//****************************************** Модель ********************************

class my_DogovorConfirmed_QSqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT
public:

    explicit my_DogovorConfirmed_QSqlQueryModel()
    {
    }
    ~my_DogovorConfirmed_QSqlQueryModel()
    {
    }

//    Qt::ItemFlags flags(const QModelIndex &index) const
//    {
//        Qt::ItemFlags flags = my_DogovorConfirmed_QSqlQueryModel::flags(index);
//        if ( index.column() != static_cast<int>(Dogovor::services::code_service)){
//            //flags.setFlag(Qt::ItemIsEditable,false);
//            flags ^= Qt::ItemIsEditable;
//        }
//        return flags;
//    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
        QVariant value = QSqlQueryModel::data(index, role);
        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
        case Qt::DisplayRole:
            if(index.column() == static_cast<int>(MainDogovor::dogovor::approved) || index.column() == static_cast<int>(MainDogovor::dogovor::changed)
                    || index.column() == static_cast<int>(MainDogovor::dogovor::closed)){
                if(value.isNull()){
                    return "";
                }else{
                    return value.toDateTime().toString("dd-MM-yyyy HH:mm:ss");
                }
            }
            break;

        case Qt::TextAlignmentRole:
            if(index.column() == static_cast<int>(MainDogovor::dogovor::namekontragent) ){
                return int(Qt::AlignLeft| Qt::AlignVCenter);
            }
            else{
                return int(Qt::AlignHCenter| Qt::AlignVCenter);
            }
            break;
        case Qt::TextColorRole:
            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::closed))).isNull())
            {
                return QBrush(Qt::gray,Qt::SolidPattern);
            }
            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::changed))).isNull())
            {
                return QBrush(Qt::darkGreen,Qt::SolidPattern);
            }
            break;
        default:
            break;

        }
        return QSqlQueryModel::data(index, role);

    }

private slots:
signals:
private:
};


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void loadSettings();
    void saveSettings();
    void SetModelDogovor();
    void SetModelComplex();
    void SetModelServices();

    void SetModelDogovorConfirmed();

    QString FormFileDogovor(int);
    void SendMail(int);

    void SetStyleGroupBox();

    void SetModelReestr();
    void SetModelReestrHeader();
    void SetModelReestrColumnView();

    void SetInvoiceCombobox();

protected:

    void closeEvent(QCloseEvent *event);

private slots:


    void tabWidgetChange(int);
    void qmspriceSpravochnik();
    void economUslugiSpravochnik();
    void complexSpravochnik();
    void invoiceSpravochnik();
    void load();
    void addDogovor();
    void delDogovor();
    void addComplex();
    void delComplex();
    void editComplex();
    void addService();
    void delService();
    void SignalAddService(int idservicecomplex);

    void ChangeDataDogovor(QModelIndex);
    void ChangeDataComplex(QModelIndex );
    void ChangeDataDogovorApproved(QModelIndex);
    void SetQueryDogovor();
    void SetQueryComplex();
    void SetQueryServices();
    void SetQueryConfirmed();
    void refreshButton();
    void refreshModels(int dogovor_row = 0, int complex_row = 0, int service_row = 0);
    void CheckPushButtons();

    void DogovorApproved();
    void DogovorClose();
    void DogovorEdit();

    void SetSmtpServers();
    void SetRecipients();

    void DoubleClickedDogovor(QModelIndex);

    void loadReestr();
    void printReestr();
    void delReestrRecords();

    void combobox_invoice_change();
    void combobox_clear_change();

    void showContextMenu(QPoint);
    void binding();
    void unbinding();

    void Exit();

signals:

    void exit();

private:
    Ui::MainWindow *ui;
    QXmlStreamReader *xmlReader;
    QSqlQueryModel *model_dogovor, *model_complex, *model_services;
    QSqlQueryModel *model_combobox_invoice;
    QStandardItemModel *model_reestr = nullptr;
    my_DogovorConfirmed_QSqlQueryModel *model_confirmed;
    TreeModel *model_tree = nullptr;
};

#endif // MAINWINDOW_H
