#include "smtpdialog.h"

smtpDialog::smtpDialog()
{
    socket = new QSslSocket(this);

    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket, SIGNAL(connected()), this, SLOT(connected() ) );
    connect(socket,&QSslSocket::encrypted,this,&smtpDialog::Encrypted);
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this,SLOT(errorReceived(QAbstractSocket::SocketError)));
    connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(stateChanged(QAbstractSocket::SocketState)));
    connect(socket, SIGNAL(disconnected()), this,SLOT(disconnected()));

    smtplog.setFileName("LogSmtp.txt");
    if(smtplog.exists()){

        if(smtplog.size() < 200000){
            smtplog.open(QIODevice::ReadOnly);
            LogData = smtplog.readAll();
            smtplog.close();
        }
    }
    smtplog.open(QIODevice::WriteOnly | QFile::Text);

    streamerr.setDevice(&smtplog);
//    streamerr.setCodec("Windows-1251");
    streamerr << QDateTime::currentDateTime().toString("dd.MM.yyyy HH:mm") + "\n";
//    streamerr << "У пациента RegNom : " + regNom + " отсутствует ЕНП !\n";
}

smtpDialog::~smtpDialog()
{
    delete t;
    delete socket;
}

void smtpDialog::setParams(const QString &user, const QString &pass, const QString &host, const QString &to, const QString &subj, const QString &body, QStringList files, int port, int timeout)
{
    QTextCodec *codec1251 = QTextCodec::codecForName("Windows-1251");
    QTextCodec *codecUtf8 = QTextCodec::codecForName("UTF-8");



    this->user = user;
    this->pass = pass;
    this->host = host;
    this->port = port;
    this->from = user;
    this->rcpt = to;
    this->subj = codec1251->toUnicode(codecUtf8->fromUnicode(subj));;
    this->body = body;
    this->files = files;
    this->timeout = timeout;
}

void smtpDialog::sendMail()
{
    message = "To: " + rcpt + "\n";
    message.append("From: " + from + "\n");
    message.append("Subject: " + subj + "\n");
    //Let's intitiate multipart MIME with cutting boundary "frontier"
    message.append("MIME-Version: 1.0\n");
    message.append("Content-Type: multipart/mixed; boundary=frontier\n\n");
    message.append( "--frontier\n" );
    //message.append( "Content-Type: text/html\n\n" );  //Uncomment this for HTML formating, coment the line below
    message.append( "Content-Type: text/plain\n\n" );
    message.append(body);
    message.append("\n\n");

    if(!files.isEmpty())
    {
        streamerr << "Files to be sent: " + QString::number(files.size()) + QString("\n") ;
        foreach(QString filePath, files)
        {
            QFile file(filePath);
            if(file.exists())
            {
                if (!file.open(QIODevice::ReadOnly))
                {
                     streamerr << "Couldn't open the file" + QString("\n");
                     return ;
                }
                QByteArray bytes = file.readAll();
                message.append( "--frontier\n" );
                message.append( "Content-Type: application/octet-stream\nContent-Disposition: attachment; filename="+ QFileInfo(file.fileName()).fileName() +";\nContent-Transfer-Encoding: base64\n\n" );
                message.append(bytes.toBase64());
                message.append("\n");
            }
        }
    }
    else{
        streamerr << "No attachments found" + QString("\n");
    }
    message.append( "--frontier--\n" );
    message.replace( QString::fromLatin1( "\n" ), QString::fromLatin1( "\r\n" ) );
    message.replace( QString::fromLatin1( "\r\n.\r\n" ),QString::fromLatin1( "\r\n..\r\n" ) );

    this->from = from;
    state = Init;

    //socket->setProtocol(QSsl::SslV3); //error  QAbstractSocket::SocketError(21) error  QAbstractSocket::SocketError(20) Connected
    //socket->setProtocol(QSsl::SslV2); //error  QAbstractSocket::SocketError(21) error  QAbstractSocket::SocketError(20) Connected
    //socket->setProtocol(QSsl::TlsV1_0);
    //socket->setProtocol(QSsl::TlsV1_0OrLater);
    //socket->setProtocol(QSsl::TlsV1_1);
    //socket->setProtocol(QSsl::TlsV1_1OrLater);
    socket->setProtocol(QSsl::TlsV1_2);
    //socket->setProtocol(QSsl::TlsV1_2OrLater);
    //socket->setProtocol(QSsl::DtlsV1_0);  // ok
    //socket->setProtocol(QSsl::DtlsV1_0OrLater); // ok
    //socket->setProtocol(QSsl::DtlsV1_2);    //  ok
    //socket->setProtocol(QSsl::DtlsV1_2OrLater); //  ok
    //socket->setProtocol(QSsl::TlsV1_3); //error  QAbstractSocket::SocketError(21) error  QAbstractSocket::SocketError(20) Connected
    //socket->setProtocol(QSsl::TlsV1_3OrLater);  //error  QAbstractSocket::SocketError(21) error  QAbstractSocket::SocketError(20) Connected
    //socket->setProtocol(QSsl::UnknownProtocol);
    //socket->setProtocol(QSsl::AnyProtocol);
    //socket->setProtocol(QSsl::TlsV1SslV3);
    //socket->setProtocol(QSsl::SecureProtocols);

//    if(QFile(certificate).exists()){
//        socket->addCaCertificates(certificate);
//    }
//    else{
//       streamerr << "Не найден файл:  " + certificate + QString("\n");
//    }

    // QSslSocket запросит сертификат у однорангового узла, но не требует, чтобы этот сертификат был действительным
    socket->setPeerVerifyMode(QSslSocket::QueryPeer);
    socket->setProxy(QNetworkProxy::NoProxy);


//    socket->connectToHostEncrypted(host, port); //"smtp.gmail.com" and 465 for gmail TLS
    socket->connectToHost(host,port);
    if (!socket->waitForConnected(timeout)) {
         streamerr << "Ошибка отправки Договора!" + QString("\n");
     }
    t = new QTextStream( socket );
    rcpt_count = 0;
    listrcpt = rcpt.split(";");
}

void smtpDialog::stateChanged(QAbstractSocket::SocketState socketState)
{
    QString str = "";
    int state = socketState;
    switch (state ) {
    case QAbstractSocket::UnconnectedState:
        str = "UnconnectedState";
        break;
    case QAbstractSocket::HostLookupState:
        str = "HostLookupState";
        break;
    case QAbstractSocket::ConnectingState:
        str = "ConnectingState";
        break;
    case QAbstractSocket::ConnectedState:
        str = "ConnectedState";
        break;
    case QAbstractSocket::BoundState:
        str = "BoundState";
        break;
    case QAbstractSocket::ClosingState:
        str = "ClosingState";
        break;
    }
    streamerr << "stateChanged : " + str + QString("\n");
}

void smtpDialog::errorReceived(QAbstractSocket::SocketError)
{
    streamerr << "error " +socket->errorString() + "\n";
}

void smtpDialog::disconnected()
{
    streamerr << "disconneted " + QString("\n\n");
//    ui->pushButton->setEnabled(true);
    streamerr.flush();
    smtplog.write(LogData);

    smtplog.close();
    emit finish();
}

void smtpDialog::connected()
{
    streamerr << "Connected " + QString("\n");
}

void smtpDialog::readyRead()
{

    streamerr << "readyRead" + QString("\n");
   // SMTP is line-oriented

   QString responseLine;
   do
   {
       responseLine = socket->readLine();
       response += responseLine;
   }
   while ( socket->canReadLine() && responseLine[3] != ' ' );

   responseLine.truncate( 3 );

//   qDebug() << "Server response code:" <<  responseLine << "State:" << state << "QSslSocket::SslMode:" << socket->mode();
//   qDebug() << "Server response: " << response;

   if ( state == Init && responseLine == "220" )
   {
       // banner was okay, let's go on
       *t << "EHLO localhost" <<"\r\n";
       t->flush();

       state = Tls;
   }
   //No need, because I'm using socket->startClienEncryption() which makes the SSL handshake for you
   else if (state == Tls && responseLine == "250")
   {
       // Trying AUTH
       streamerr << "STarting Tls" + QString("\n");
       *t << "STARTTLS" << "\r\n";
       t->flush();
       state = HandShake;
   }
   else if ((state == HandShake && responseLine == "250")){
       streamerr << "Waiting to be ready for a handshake" + QString("\n");
   }
   else if (state == HandShake && responseLine == "220")
   {
       socket->startClientEncryption();

       //Send EHLO once again but now encrypted
       *t << "EHLO localhost" << "\r\n";
       t->flush();


//        if(!socket->waitForEncrypted(1000))
//        {
//            qDebug() << socket->errorString() << "state == HandShake && responseLine == 220";
//            state = Close;
//        }
//        else{
//            //Send EHLO once again but now encrypted
//            *t << "EHLO localhost" << "\r\n";
//            t->flush();

//            state = Auth;
//        }
   }
   else if (state == Auth && responseLine == "250")
   {
       // Trying AUTH
       streamerr << "Auth" + QString("\n");
       *t << "AUTH LOGIN" << "\r\n";
       t->flush();
       state = User;
   }
   else if (state == User && responseLine == "334")
   {
       //Trying User
       streamerr << "Username" + QString("\n");
       //GMAIL is using XOAUTH2 protocol, which basically means that password and username has to be sent in base64 coding
       //https://developers.google.com/gmail/xoauth2_protocol
       *t << user.toUtf8().toBase64()  << "\r\n";
       t->flush();

       state = Pass;
   }
   else if (state == Pass && responseLine == "334")
   {
       //Trying pass
       streamerr << "Pass" + QString("\n");
       *t << pass.toUtf8().toBase64() << "\r\n";
       t->flush();

       state = Mail;
   }
   else if ( state == Mail && responseLine == "235" )
   {
       // HELO response was okay (well, it has to be)

       //Apperantly for Google it is mandatory to have MAIL FROM and RCPT email formated the following way -> <email@gmail.com>
       streamerr << "MAIL FROM:<" + from + ">" + "\n";
       *t << "MAIL FROM:<" << from << ">\r\n";
       t->flush();
       state = Rcpt;
   }
   else if ( state == Rcpt && responseLine == "250" )
   {
       if(rcpt_count < listrcpt.length()){
           streamerr << "RCPT TO:<" + listrcpt.at(rcpt_count) + ">" + "\n";
           //Apperantly for Google it is mandatory to have MAIL FROM and RCPT email formated the following way -> <email@gmail.com>
           *t << "RCPT TO: <" << listrcpt.at(rcpt_count) << ">\r\n"; //r
           t->flush();
           rcpt_count++;
       }
       if(rcpt_count >= listrcpt.length()){
           state = Data;
       }
   }
   else if ( state == Data && responseLine == "250" )
   {

       *t << "DATA\r\n";
       t->flush();
       state = Body;
   }
   else if ( state == Body && responseLine == "354" )
   {

       *t << message << "\r\n.\r\n";
       t->flush();
       state = Quit;
   }
   else if ( state == Quit && responseLine == "250" )
   {

       *t << "QUIT\r\n";
       t->flush();
       // here, we just close.
       state = Close;
       streamerr << "Договор отправлен адресатам!" + QString("\n");
   }
   else if ( state == Close )
   {
       //deleteLater();
       socket->close();
       return;
   }
   else
   {
       // something broke.
       //QMessageBox::warning( 0, tr( "Qt Simple SMTP client" ), tr( "Unexpected reply from SMTP server:\n\n" ) + response );
       //state = Close;
       streamerr << "Ошибка отправки договора!" + QString("\n");
   }
   response = "";
}

void smtpDialog::Encrypted()
{
    streamerr << "encrypted" + QString("\n");
    state = Auth;
}
