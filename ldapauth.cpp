/**********************  LINUX   *************************************
Ну вот вроде чтото заработало, может кому понадобиться, опишу алгоритм действий. Рабочая станция с Ubuntu 18.04, установлен Qt 5.12.1 GCC 64bit.
Качаем с указанной этой ссылке http://www.openldap.org/software/download/ OpenLDAP-2.4.47.zip (или tar), распаковываем любую папку в домашнем каталоге.
Устанавливаем в системе следующие пакеты libldap-2.4-2 , libldap-common , libldap2-dev
$ sudo apt-get install libldap-2.4-2 libldap-common libldap2-dev
Мне еще понадобилось установить пакет libsasl2-dev
$sudo apt-get install libsasl2-dev
Переходим в папку с распакованным OpenLDAP-2.4.47 и далее идем в contrib/ldapc++
$cd ~/Download/openldap-2.4.47/contrib/ldapc++/
Далее запускаем :

$ ./configure
$ sudo make
$ sudo make install

Библиотеки и заголовочные фалы установились в /usl/local/lib и /usl/local/include
В файл проекта добавил INCLUDEPATH += /usl/local/include и LIBS += -L/usr/local/lib -lldapcpp
примеры из папки /contrib/ldapc++/examples/ собрались и заработали, сейчас нужно покурить доки чтобы разобраться как с этим работать и попробовать авторизоваться в AD.
Авторизоваться в AD нужно либо по distinguishedName именно так как записано в AD либо по userPrincipalName

lc->bind("CN=Слободчиков Дмитрий Александрович,OU=Програмисты,OU=IVO,DC=viveya,DC=local", "<.hjrhfnbz@1",cons);

lc->bind("slobodchikovda@VIVEYA.LOCAL", "<.hjrhfnbz@1",cons);
**************************************************************************/
#include <QString>
#include <QStringList>
#include "ldap.h"
#include "LDAPConstraints.h"
#include "LDAPConnection.h"

QStringList LDAP_connect(){
    return QStringList();
}


bool LDAPAuth(QString login, QString pass, QString *cn)
{
    LDAPConstraints* cons=new LDAPConstraints;
    LDAPControlSet* ctrls=new LDAPControlSet;
    ctrls->add(LDAPCtrl(LDAP_CONTROL_MANAGEDSAIT));
    cons->setServerControls(ctrls);
    LDAPConnection *lc=new LDAPConnection("dc1.viveya.local",389);
    lc->setConstraints(cons);
    try {
        lc->bind(login.toStdString(),pass.toStdString(),cons);
        StringList tmp;                                          // если нет строки фильтра то выводятся все атрибуты !!! Строка фильтрует выводимые атрибуты
        tmp.add("cn");
        tmp.add("userPrincipalName");
        LDAPSearchResults *r =nullptr;
        login = "userPrincipalName=" + login;
        r=lc->search("DC=viveya,DC=local",LDAPAsynConnection::SEARCH_SUB, // ищем в AD атрибут cn для данного userPrincipalName
                     login.toStdString(),tmp);
        LDAPEntry *entry = r->getNext();    // получаем первую точку входа, можно получить dn (это distinguishedName именно так как записано в AD) entry->getDN()
        const LDAPAttributeList *att = entry->getAttributes();
        for(LDAPAttributeList::const_iterator i = att->begin(); i != att->end();i++) // перебираем список аттрибутов
        {
            //std::cout << "Name attr : " << i->getName() << "  " << i->getNumValues() << std::endl;
            const StringList values = i->getValues();
            for( StringList::const_iterator j = values.begin(); j != values.end(); j++ ){
                if(i->getName()=="cn")
                {
                    cn->append(QString::fromStdString(*j)); //Получаем атрибут cn -
                }
                //std::cout << *j << std::endl;
            }
        }
        return true;
    } catch (LDAPException e) {
        std::cout << e.getResultMsg() << std::endl;
        cn->append(QString::fromStdString(e.getResultMsg()));
        return false;
    }
}
