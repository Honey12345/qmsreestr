#ifndef DOGOVOR_ADD_H
#define DOGOVOR_ADD_H

#include <QDialog>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QDate>
#include <QDebug>
#include <QKeyEvent>
#include <QItemDelegate>
#include <QLineEdit>
#include <QMessageBox>
#include <QTimer>


class Dogovor : public QObject {
public:
    /// достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int

    enum class services :int {code_service,name_usl,code_price,name_price,ideconom,idprice};
    Q_ENUM(services)


    Q_OBJECT
    Dogovor() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums
};

//****************************************** Модель ********************************

class my_Dogovor_StandardItemmodel : public QStandardItemModel
{
    Q_OBJECT
public:
    QWidget * parent_w;

    explicit my_Dogovor_StandardItemmodel(QWidget *parent)
    {
        parent_w = parent;

    }
    ~my_Dogovor_StandardItemmodel()
    {
    }

    Qt::ItemFlags flags(const QModelIndex &index) const
    {
        Qt::ItemFlags flags = QStandardItemModel::flags(index);
        if ( index.column() != static_cast<int>(Dogovor::services::code_service)){
//            flags.setFlag(Qt::ItemIsEditable,false);
            flags ^= Qt::ItemIsEditable;
        }
        return flags;
    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
//        QVariant value = QStandardItemModel::data(index, role);
//        QVariant data=QStandardItemModel::data(index);
//        switch (role)
//        {
//        case Qt::DisplayRole:
//            if(index.column() == 2){
//                return strKod.value(value.toInt());
//            }
//            if(index.column() == 4){
//                return (data.toBool()) ? "ДА" : "НЕТ";
//            }
//            break;

//        case Qt::TextAlignmentRole:
//            if(index.column() == 2 || index.column() == 4){
//                return int(Qt::AlignHCenter| Qt::AlignVCenter);
//            }
//            else{
//                return int(Qt::AlignLeft| Qt::AlignVCenter);
//            }
//            break;
//        case Qt::TextColorRole:
//            if(!QStandardItemModel::data(QStandardItemModel::index(index.row(),4)).toBool())
//            {
//                return QBrush(Qt::gray,Qt::SolidPattern);
//            }
//            break;
//        default:
//            break;

//        }
        return QStandardItemModel::data(index, role);

    }

private slots:
signals:
private:
};


//****************************************** Делегат для редактирования списка услуг в договоре********************************

class dogovor_service_delegate : public QItemDelegate
{
    Q_OBJECT
public:

    QWidget * parent_w;

    explicit dogovor_service_delegate(QWidget *parent)
    {
        parent_w = parent;
    }

    ~dogovor_service_delegate()
    {

    }

    // функция инициации процесса редактирования
    bool editorEvent(QEvent *event, QAbstractItemModel *, const QStyleOptionViewItem , const QModelIndex )
    {
        if(event->type()== QEvent::KeyPress)
        {
            QKeyEvent *KeyEvent = static_cast<QKeyEvent*>(event);

            if (KeyEvent->key() == Qt::Key_F2)
            {
                return false;
            }else
                return true; // если не F2 то не редактируем
        }

        if(event->type() != QEvent::MouseButtonDblClick){ //  не двойной клик мышкой
            return false;  // если не двойной клик то выполняем то что положено на 1 клик
        }

        return true;
    }

    // создаем виджет редактора значения
//    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
//    {
//        if(index.column() == static_cast<int>(Dogovor::services::code_price)) // Если хотим изменить поле уровня доступа
//        {
//            QComboBox *editor = new QComboBox(parent);
//            return editor;
//        }

//        return QItemDelegate::createEditor(parent,option,index);
//    }

    //передаем значение от модели в редактор
//    void setEditorData(QWidget *editor, const QModelIndex &index) const
//    {
//        QLineEdit *name = static_cast<QLineEdit*>(editor);
//        name->setText(index.model()->data(index,Qt::DisplayRole).toString().trimmed());
//        return;
//    }
    //передает значение от редактора в модель
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
    {

        QLineEdit *name = static_cast<QLineEdit*>(editor);
        if(name->text().trimmed() == model->data(index).toString()){
            return;
        }
        QModelIndexList IndexList = model->match(model->index(0,static_cast<int>(Dogovor::services::code_service)),Qt::DisplayRole,name->text().trimmed(),1,Qt::MatchFixedString);
        if(IndexList.length() != 0){
            QMessageBox::information(parent_w, "Information", "Такой код существует!");
            return;
        }
       //model->setData(model->index(index.row(),static_cast<int>(Dogovor::services::code_service)),QString("true"));
        return QItemDelegate::setModelData(editor,model,index);
    }
};


namespace Ui {
class dogovor_add;
}

class dogovor_add : public QDialog
{
    Q_OBJECT
    enum Regim {AddDogovor,AddComplex};

public:
    explicit dogovor_add(QWidget *parent = nullptr);
    ~dogovor_add();

    bool SetData(int,int);
    int GetIdDogovor();
    int GetIdComplex();

    void SetReadOnly(QWidget *widget);

private slots:

    void AddShablonComplex();
    void AddService();
    void SignalAddService(int ideconom, int idprice);
    void DelService();
    void DeleteAll();
    bool CheckSave();
    void SetCode();
    void SetCodePrice();
    void SaveData();
    void SelectionChanged();


signals:

    void NotAdded();

private:
    Ui::dogovor_add *ui;
    my_Dogovor_StandardItemmodel *model_services;
    dogovor_service_delegate *delegate;
    int idDogovor = 0;
    int idComplex = 0;
    int rEgim;

};

#endif // DOGOVOR_ADD_H
