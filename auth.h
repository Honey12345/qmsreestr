#ifndef AUTH_H
#define AUTH_H

#include <QDialog>
#include <QStringList>
#include <QSqlQuery>
#include <QSqlError>
#include <QMessageBox>
#include <QFont>
#include <QTimer>
#include <QProcess>

namespace Ui {
class auth;
}

class auth : public QDialog
{
    Q_OBJECT

public:
    explicit auth(QWidget *parent = nullptr);
    ~auth();

    QString GetUserName();

private slots:

    void AuthDomain();
    void AuthLogin();
    void LineEditEmpty();

    void on_pushButton_clicked();

private:
    Ui::auth *ui;
    int count_auth;
};

#endif // AUTH_H
