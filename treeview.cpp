#include "mainwindow.h"
#include "treeview.h"
#include <QSqlQueryModel>
#include <QDate>
#include <QProgressDialog>
#include <QSqlRecord>



TreeModel::TreeModel(QList<QVariant> title, QObject *parent)
    : QAbstractItemModel(parent)
{

    view = parent->findChild<QTreeView*>("treeView");
    QList<QVariant> rootData;
    rootData = title;
    rootItem = new TreeItem(rootData);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

void TreeModel::SetQuery(QSqlQuery &query)
{
    setupModelData(query, rootItem);
}

QVariant TreeModel::data(const QModelIndex &index, int role = Qt::DisplayRole) const
{
    if (!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole){
        TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
        return item->data(index.column());
    }
    else if(role == Qt::DecorationRole){
        if(!index.parent().isValid() && index.column() == 0 && !view->isExpanded(index)){
            return QIcon(":/icons/folder_Closed.png");
        }
        else if(!index.parent().isValid() && index.column() == 0 && view->isExpanded(index)){
            return QIcon(":/icons/folder_Open.png");
        }
    }
    else if(role == Qt::TextColorRole){
        if(index.parent().isValid()){
            return QBrush(Qt::darkBlue,Qt::SolidPattern);
        }
    }
    return QVariant();
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

void TreeModel::setupModelData(QSqlQuery &query, TreeItem *parent)
{
    int position = 0;
    int levelinner = 1;
    int x;
    QList<TreeItem*> parents;
    QList<int> indentations;
    QList<int> previos;
    QList<QVariant> columnData;
    parents << parent;
    indentations << 0;
    previos << 0 << 0 << 0;
    query.first();
    do                  // пока не все записи
    {
        position = 0;
        x = 1;
        while(position <= levelinner)
        {
            if(previos[position] != query.value(x+2))
            {
                columnData.clear();
                columnData << query.value(x-1).toString() << query.value(x).toString() << query.value(x+1).toString() << query.value(x+2).toString();
                previos[position] = query.value(x+2).toInt();

                if (position > indentations.last()) { //если позиция текущего элемента больше предыдущего
                    // The last child of the current parent is now the new parent
                    // unless the current parent has no children.

                    if (parents.last()->childCount() > 0) { // это ребенок в текущей ветке и изменяем текущего элемента
                        parents << parents.last()->child(parents.last()->childCount()-1);
                        indentations << position;
                    }
                } else {
                    while (position < indentations.last() && parents.count() > 0) {//пока позиция текущего элемента меньше предыдущего и не корневая 0
                        parents.pop_back();
                        indentations.pop_back();
                    }
                }

                parents.last()->appendChild(new TreeItem(columnData, parents.last())); // Создаем ветку с двумя колонками со значениями в columnData
            }
            position++;
            x+=4;
        }
    }while(query.next());
}



TreeItem::TreeItem(const QList<QVariant> &data, TreeItem *parent)
{
    parentItem = parent;
    itemData = data;
}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
}

void TreeItem::appendChild(TreeItem *item)
{
    childItems.append(item);
}

TreeItem *TreeItem::child(int row)
{
    return childItems.value(row);
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::columnCount() const
{
    return itemData.count();
}

QVariant TreeItem::data(int column) const
{
    return itemData.value(column);
}

TreeItem *TreeItem::parent()
{
    return parentItem;
}

int TreeItem::row() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}

