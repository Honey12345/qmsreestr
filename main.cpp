#include "global.h"
#include "auth.h"
#include "mainwindowparent.h"
#include <QApplication>
#include <QSqlError>
#include <QTextCodec>
#include <QCryptographicHash>
#include <QFile>
#include <QTextStream>
#include <QTextCodec>

bool connectionDB();
bool version();
void readConfig(QString section = "");
QString HostName = "";
QString DatabaseName = "";


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString str = argv[1];

    readConfig(str);

    if(!connectionDB())
    {

        QString mess;
        QByteArray arr;
        QTextCodec *codec;

//        codec = QTextCodec::codecForName("Windows-1251");
//        arr = codec->fromUnicode(db.lastError().text());
//        codec = QTextCodec::codecForName("UTF-8");
//        mess = codec->toUnicode(arr);

        arr = db.lastError().text().toLocal8Bit();
        codec = QTextCodec::codecForName("UTF-8");
        mess = codec->toUnicode(arr);

        QMessageBox::critical(nullptr,"Ошибка","<p align=center>Невозможно подключиться к базе данных postgres сервера!<br><font color=red>" + mess + "</font></p>");
        exit(0);
    }
    QSqlQuery query;

    if(db.driverName() == "QPSQL"){
        query.exec("SET search_path TO public,reestr,audit");
    }


    if(!version()){
        int n = QMessageBox::warning(nullptr,"Внимание!","<p align=center>Доступна новая версия.<br>Обновить?</p>",
                             QMessageBox::Yes|QMessageBox::No,
                             QMessageBox::Yes);
        if (n == QMessageBox::Yes)
        {
            QFile file;
//            QString proc = "rnew.exe " + str;
            QString proc = "rnew.exe";
            QStringList arg;
            arg.append(str);
            file.setFileName("rnew.exe");
            if(!file.exists()){
                if(file.copy("//file-server.viveya.local/SharedRes/ivo/rnew.exe","rnew.exe"))
                {
    //            file.setFileName("rnew.exe");
    //            if(file.exists()){
                    QProcess *process = new QProcess(nullptr);

                    process->start(proc,arg);
                    db.close();
                    exit(0);
                }
                else
                {
                    QMessageBox::critical(nullptr,"Ошибка","<p align=center>Отсутствует файл обновления<br>Программа не обновлена!"
                                                           "<br>Обратитесь к Администратору.</p>");
                }
            }
            else{
                QProcess *process = new QProcess(nullptr);
                process->start(proc,arg);
                db.close();
                exit(0);
            }
        }
    }

    QDialog *au = new auth();
    if(!au->exec())
        exit(0);
    delete au;

    NameKodeAccess << "AdminRole" << "GuestRole" << "EconomistRole" << "SisterOVDPO";

    MainWindowParent w;
    w.show();

    return a.exec();
}

// Функция подключения к БД
bool connectionDB()
{
    {
        db = QSqlDatabase::addDatabase("QPSQL");
        db.setHostName(HostName);
        //db.setHostName("192.168.66.15");
        db.setPort(5432);
        db.setDatabaseName(DatabaseName);
        db.setUserName("qmsreestr");
        db.setPassword("1q2w3e@1");
        return db.open();
    }
}

bool version()
{
    ver = "2.15";
    QByteArray hashdata;
    hashdata = QCryptographicHash::hash(ver.toUtf8(),QCryptographicHash::Sha1);
    QSqlQuery query;
    if(false){
        query.prepare("delete from version");
        if(!query.exec()){
            qDebug() << query.lastError();
        }
        query.prepare("insert into version (version) VALUES(?)");
        query.addBindValue(hashdata);
        if(!query.exec()){
            qDebug() << query.lastError();
        }
    }

    query.prepare("select version from version");
    if(!query.exec()){
//            QMessageBox::critical(nullptr,"Ошибка",
//                                  "<p style='text-align: center'>Ошибка проверки версии!<br><span style='color: red'>" + query.lastError().text() +"</p>");
        qDebug() << "Ошибка выполнения запроса select version from version \n" + query.lastError().text();
        return true;
    }
    query.first();
//    qDebug() << "Из БД   " << query.value("version").toByteArray() << query.value("version").toString();
//    qDebug() << "Из кода " << hashdata;
    if(query.first() && query.value("version").toByteArray() != hashdata){
        return false;
    }
    return true;
}

void readConfig(QString sec)
{
    QString filename = QApplication::applicationName() + ".ini";
    QFile inifile(filename);
    if(!inifile.exists()){
        inifile.open(QIODevice::WriteOnly | QFile::Text);
        QTextStream streamer;
        streamer.setDevice(&inifile);
        streamer << "[Default]\nHostName=pamir.viveya.local\nDatabaseName=qmsreestr";
        streamer.flush();
        inifile.close();
    }
    inifile.open(QIODevice::ReadOnly);
    //QByteArray configData = inifile.readAll();
    QStringList configData = QTextCodec::codecForName("Windows-1251")->toUnicode(inifile.readAll()).split("\r\n");
    inifile.close();

    if(sec == "debug"){
        HostName="192.168.66.15";
        DatabaseName="qmsreestr";
        return;
    }
    int pos = configData.indexOf("[Default]");
    HostName = configData.at(pos+1);
    HostName = HostName.remove("HostName=").trimmed();
    DatabaseName = configData.at(pos+2);
    DatabaseName = DatabaseName.remove("DatabaseName=").trimmed();
//    qDebug() << HostName << DatabaseName;
}
