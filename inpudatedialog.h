#ifndef INPUDATEDIALOG_H
#define INPUDATEDIALOG_H

#include <QDialog>
#include <QDate>

namespace Ui {
class InpuDateDialog;
}

class InpuDateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InpuDateDialog(QWidget *parent = nullptr);
    ~InpuDateDialog();

    void SetDataDialog(QString title = "Дата",QDate data = QDate::currentDate());
    QDate GetDate();

private:
    Ui::InpuDateDialog *ui;
};

#endif // INPUDATEDIALOG_H
