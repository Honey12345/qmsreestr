#ifndef OVDPO_KONTR_ADD_EDIT_H
#define OVDPO_KONTR_ADD_EDIT_H

#include <QDialog>
#include <QRegExpValidator>
#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlError>

namespace Ui {
class ovdpo_kontr_add_edit;
}

class ovdpo_kontr_add_edit : public QDialog
{
    Q_OBJECT

public:
    enum Regim {AddKontragent,EditKontragent};
    explicit ovdpo_kontr_add_edit(QWidget *parent = nullptr);
    ~ovdpo_kontr_add_edit();


    bool CheckINN(QString str);
    int GetIdkontragent();
    void SetData(int id, QString inn, QString name);

private slots:

    void LineEditINN_editingFinished();
    void SavePushButton();

private:
    Ui::ovdpo_kontr_add_edit *ui;
    int idkontragent;
    int regim;

};

#endif // OVDPO_KONTR_ADD_EDIT_H
