#include "ovdpo_kontr_add_edit.h"
#include "ui_ovdpo_kontr_add_edit.h"

#include <QDebug>

ovdpo_kontr_add_edit::ovdpo_kontr_add_edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ovdpo_kontr_add_edit)
{
    ui->setupUi(this);
    this->setWindowTitle("Добавление нового контрагента");

    connect(ui->pushButton_cancel,&QPushButton::clicked,this,&ovdpo_kontr_add_edit::reject);
    connect(ui->pushButton_save,&QPushButton::clicked,this,&ovdpo_kontr_add_edit::SavePushButton);
//    connect(ui->lineEdit_inn,&QLineEdit::editingFinished,this,&ovdpo_kontr_add_edit::LineEditINN_editingFinished);

    ui->lineEdit_inn->setValidator(new QRegExpValidator(QRegExp("^[1-9]\\d{1,9}\\d?\\d?$")));
    regim = Regim::AddKontragent;
}

ovdpo_kontr_add_edit::~ovdpo_kontr_add_edit()
{
    delete ui;
}

bool ovdpo_kontr_add_edit::CheckINN(QString str)
{
    if(ui->checkBox_inn->isChecked()){
        return true;
    }
    if(str.length() == 10){
        int ves[10]= {2,4,10,3,5,9,4,6,8,0};
        int summ = 0;
        for(int i = 0; i < 10; i++){
            QString number = str.at(i);
            summ += number.toInt()*ves[i];
        }
        summ = summ % 11;
        if(summ > 9){
            summ = summ % 10;
        }
        if(summ == str.right(1).toInt()){
            return true;
        }
    }
    else if (str.length() == 12) {
        int ves11[11] = {7,2,4,10,3,5,9,4,6,8,0};
        int ves12[12] = {3,7,2,4,10,3,5,9,4,6,8,0};
        int summ11 = 0;
        int summ12 = 0;
        for(int i = 0; i < 11; i++){
            QString number = str.at(i);
            summ11 += number.toInt()*ves11[i];
        }
        summ11 = summ11 % 11;
        if(summ11 > 9){
            summ11 = summ11 % 10;
        }
        for(int i = 0; i < 12; i++){
            QString number = str.at(i);
            summ12 += number.toInt()*ves12[i];
        }
        summ12 = summ12 % 11;
        if(summ12 > 9){
            summ12 = summ12 % 10;
        }
        if(summ11 == str.mid(10,1).toInt() && summ12 == str.mid(11,1).toInt()){
            return true;
        }
    }
    return false;
}

int ovdpo_kontr_add_edit::GetIdkontragent()
{
    return idkontragent;
}

void ovdpo_kontr_add_edit::SetData(int id, QString inn, QString name)
{
    regim = Regim::EditKontragent;
    idkontragent = id;
    ui->lineEdit_inn->setText(inn);
    ui->lineEdit_name->setText(name);
}

void ovdpo_kontr_add_edit::LineEditINN_editingFinished()
{

    QString str = ui->lineEdit_inn->text().trimmed();
    if(str.length() == 10 || str.length() == 12){
        if(CheckINN(str)){
            ui->lineEdit_inn->setStyleSheet("QLineEdit { font-size: 12pt; color: black }");
            return;
        }
    }
    QMessageBox::critical(this,"Ошибка",
                    "<p style='text-align: center ; color: red'>Неверный ИНН.<br><span style='color: red'></p>");
    ui->lineEdit_inn->setStyleSheet("QLineEdit { font-size: 12pt; color: red }");
    return;
}

void ovdpo_kontr_add_edit::SavePushButton()
{
    if(ui->lineEdit_name->text().trimmed().isEmpty()){
        QMessageBox::warning(this,"Внимание",
                        "<p style='text-align: center ; color: red'>Заполните Наименование контрагента</p>");
        return;

    }
    if(ui->lineEdit_inn->text().trimmed().isEmpty()){
        QMessageBox::warning(this,"Внимание",
                        "<p style='text-align: center ; color: red'>Заполните ИНН контрагента</p>");
        return;

    }
    QString inn = ui->lineEdit_inn->text().trimmed();
    QString name = ui->lineEdit_name->text().trimmed();
    if(!CheckINN(inn)){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center ; color: red'>Неверный ИНН.<br><span style='color: red'></p>");
        return;
    }
    if(regim == Regim::AddKontragent){
        QSqlQuery query;
        query.prepare("select * from ovdpo.kontragent where innkontragent = ?");
        query.addBindValue(inn);
        query.exec();
        if(query.first()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center ; color: red'>Контрагент с таким ИНН существует!</p>");
            return;
        }
        query.prepare("insert into ovdpo.kontragent (innkontragent,namekontragent) values (?,?) returning idkontragent");
        query.addBindValue(inn);
        query.addBindValue(name);
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center'>Ошибка выполнения запроса сохранения данных.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        }
        query.first();
        idkontragent = query.value("idkontragent").toInt();
    }
    else if (regim == Regim::EditKontragent) {
        QSqlQuery query;
        query.prepare("select * from ovdpo.kontragent where innkontragent = ? and idkontragent != ?");
        query.addBindValue(inn);
        query.addBindValue(idkontragent);
        query.exec();
        if(query.first()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center ; color: red'>Контрагент с таким ИНН существует!</p>");
            return;
        }
        query.prepare("update ovdpo.kontragent SET innkontragent = ?,namekontragent = ? where idkontragent = ?");
        query.addBindValue(inn);
        query.addBindValue(name);
        query.addBindValue(idkontragent);
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center'>Ошибка выполнения запроса сохранения данных.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        }
    }
    return accept();
}
