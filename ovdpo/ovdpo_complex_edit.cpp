#include "ovdpo_complex_edit.h"
#include "ui_ovdpo_complex_edit.h"

ovdpo_complex_edit::ovdpo_complex_edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ovdpo_complex_edit)
{
    ui->setupUi(this);
    connect(ui->pushButton_cancel,&QPushButton::clicked,this,&ovdpo_complex_edit::reject);
    connect(ui->pushButton_save,&QPushButton::clicked,this,&ovdpo_complex_edit::saveData);
}

ovdpo_complex_edit::~ovdpo_complex_edit()
{
    delete ui;
}

void ovdpo_complex_edit::setData(int id_complex, QString namecomplex, int kolvocomplex)
{
    idcomplex = id_complex;
    this->setWindowTitle(namecomplex);
    ui->spinBox->setValue(kolvocomplex);
}

int ovdpo_complex_edit::getKolvo()
{
    return ui->spinBox->value();
}

void ovdpo_complex_edit::saveData()
{
    return accept();
}
