#ifndef OVDPO_COMPLEX_EDIT_H
#define OVDPO_COMPLEX_EDIT_H

#include <QDialog>

namespace Ui {
class ovdpo_complex_edit;
}

class ovdpo_complex_edit : public QDialog
{
    Q_OBJECT

public:
    explicit ovdpo_complex_edit(QWidget *parent = nullptr);
    ~ovdpo_complex_edit();

    void setData(int idcomplex, QString namecomplex, int kolvocomplex);
    int getKolvo();

private slots:

    void saveData();


private:
    Ui::ovdpo_complex_edit *ui;
    int idcomplex;
};

#endif // OVDPO_COMPLEX_EDIT_H
