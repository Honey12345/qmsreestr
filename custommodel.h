#ifndef CUSTOMMODEL_H
#define CUSTOMMODEL_H

#include "mainwindow.h"
#include <QSqlQueryModel>
#include <QStandardItem>

class my_Dogovor_QSqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT
public:

    explicit my_Dogovor_QSqlQueryModel()
    {

    }
    ~my_Dogovor_QSqlQueryModel()
    {
    }

//    Qt::ItemFlags flags(const QModelIndex &index) const
//    {
//        Qt::ItemFlags flags = my_DogovorConfirmed_QSqlQueryModel::flags(index);
//        if ( index.column() != static_cast<int>(Dogovor::services::code_service)){
//            //flags.setFlag(Qt::ItemIsEditable,false);
//            flags ^= Qt::ItemIsEditable;
//        }
//        return flags;
//    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
//        QVariant value = QSqlQueryModel::data(index, role);
//        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
//        case Qt::DisplayRole:
//            if(index.column() == static_cast<int>(MainDogovor::dogovor::approved) || index.column() == static_cast<int>(MainDogovor::dogovor::changed)
//                    || index.column() == static_cast<int>(MainDogovor::dogovor::closed)){
//                if(value.isNull()){
//                    return "";
//                }else{
//                    return value.toDateTime().toString("dd-MM-yyyy HH:mm:ss");
//                }
//            }
//            break;

        case Qt::TextAlignmentRole:
            if(index.column() == static_cast<int>(MainDogovor::dogovor::namekontragent) ){
                return int(Qt::AlignLeft| Qt::AlignVCenter);
            }
            else if(index.column() == static_cast<int>(MainDogovor::dogovor::numberdogovor) || index.column() == static_cast<int>(MainDogovor::dogovor::start) || index.column() == static_cast<int>(MainDogovor::dogovor::finish)){
                return int(Qt::AlignHCenter| Qt::AlignVCenter);
            }
            break;
//        case Qt::TextColorRole:
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::closed))).isNull())
//            {
//                return QBrush(Qt::gray,Qt::SolidPattern);
//            }
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::changed))).isNull())
//            {
//                return QBrush(Qt::darkGreen,Qt::SolidPattern);
//            }
//            break;
        default:
            break;

        }
        return QSqlQueryModel::data(index, role);

    }

private slots:
signals:
private:
};

class my_Complex_QSqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT
public:

    explicit my_Complex_QSqlQueryModel()
    {

    }
    ~my_Complex_QSqlQueryModel()
    {
    }

//    Qt::ItemFlags flags(const QModelIndex &index) const
//    {
//        Qt::ItemFlags flags = my_DogovorConfirmed_QSqlQueryModel::flags(index);
//        if ( index.column() != static_cast<int>(Dogovor::services::code_service)){
//            //flags.setFlag(Qt::ItemIsEditable,false);
//            flags ^= Qt::ItemIsEditable;
//        }
//        return flags;
//    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
//        QVariant value = QSqlQueryModel::data(index, role);
//        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
//        case Qt::DisplayRole:
//            if(index.column() == static_cast<int>(MainDogovor::dogovor::approved) || index.column() == static_cast<int>(MainDogovor::dogovor::changed)
//                    || index.column() == static_cast<int>(MainDogovor::dogovor::closed)){
//                if(value.isNull()){
//                    return "";
//                }else{
//                    return value.toDateTime().toString("dd-MM-yyyy HH:mm:ss");
//                }
//            }
//            break;

        case Qt::TextAlignmentRole:
            if(index.column() == static_cast<int>(MainDogovor::complex::namecomplex) ){
                return int(Qt::AlignLeft| Qt::AlignVCenter);
            }
            else if(index.column() == static_cast<int>(MainDogovor::complex::codecomplex)){
                return int(Qt::AlignHCenter| Qt::AlignVCenter);
            }
            else if(index.column() == static_cast<int>(MainDogovor::complex::stoimost)){
                return int(Qt::AlignRight| Qt::AlignVCenter);
            }
            break;
//        case Qt::TextColorRole:
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::closed))).isNull())
//            {
//                return QBrush(Qt::gray,Qt::SolidPattern);
//            }
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::changed))).isNull())
//            {
//                return QBrush(Qt::darkGreen,Qt::SolidPattern);
//            }
//            break;
        default:
            break;

        }
        return QSqlQueryModel::data(index, role);

    }

private slots:
signals:
private:
};

class my_Services_QSqlQueryModel : public QSqlQueryModel
{
    Q_OBJECT
public:

    explicit my_Services_QSqlQueryModel()
    {

    }
    ~my_Services_QSqlQueryModel()
    {
    }

//    Qt::ItemFlags flags(const QModelIndex &index) const
//    {
//        Qt::ItemFlags flags = my_DogovorConfirmed_QSqlQueryModel::flags(index);
//        if ( index.column() != static_cast<int>(Dogovor::services::code_service)){
//            //flags.setFlag(Qt::ItemIsEditable,false);
//            flags ^= Qt::ItemIsEditable;
//        }
//        return flags;
//    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
//        QVariant value = QSqlQueryModel::data(index, role);
//        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
//        case Qt::DisplayRole:
//            if(index.column() == static_cast<int>(MainDogovor::dogovor::approved) || index.column() == static_cast<int>(MainDogovor::dogovor::changed)
//                    || index.column() == static_cast<int>(MainDogovor::dogovor::closed)){
//                if(value.isNull()){
//                    return "";
//                }else{
//                    return value.toDateTime().toString("dd-MM-yyyy HH:mm:ss");
//                }
//            }
//            break;

        case Qt::TextAlignmentRole:
            if(index.column() == static_cast<int>(MainDogovor::services::name_usl) || index.column() == static_cast<int>(MainDogovor::services::name_price)){
                return int(Qt::AlignLeft| Qt::AlignVCenter);
            }
            else if(index.column() == static_cast<int>(MainDogovor::services::code_service) || index.column() == static_cast<int>(MainDogovor::services::code_price)){
                return int(Qt::AlignHCenter| Qt::AlignVCenter);
            }
            break;
//        case Qt::TextColorRole:
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::closed))).isNull())
//            {
//                return QBrush(Qt::gray,Qt::SolidPattern);
//            }
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::changed))).isNull())
//            {
//                return QBrush(Qt::darkGreen,Qt::SolidPattern);
//            }
//            break;
        default:
            break;

        }
        return QSqlQueryModel::data(index, role);

    }

private slots:
signals:
private:
};

class my_Reestr_QStandardItemModel : public QStandardItemModel
{
    Q_OBJECT
public:

    explicit my_Reestr_QStandardItemModel()
    {

    }
    ~my_Reestr_QStandardItemModel()
    {
    }

//    Qt::ItemFlags flags(const QModelIndex &index) const
//    {
//        Qt::ItemFlags flags = my_DogovorConfirmed_QSqlQueryModel::flags(index);
//        if ( index.column() != static_cast<int>(Dogovor::services::code_service)){
//            //flags.setFlag(Qt::ItemIsEditable,false);
//            flags ^= Qt::ItemIsEditable;
//        }
//        return flags;
//    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
//        QVariant value = QSqlQueryModel::data(index, role);
//        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
//        case Qt::DisplayRole:
//            if(index.column() == static_cast<int>(MainDogovor::dogovor::approved) || index.column() == static_cast<int>(MainDogovor::dogovor::changed)
//                    || index.column() == static_cast<int>(MainDogovor::dogovor::closed)){
//                if(value.isNull()){
//                    return "";
//                }else{
//                    return value.toDateTime().toString("dd-MM-yyyy HH:mm:ss");
//                }
//            }
//            break;

        case Qt::TextAlignmentRole:
            if(index.column() == static_cast<int>(MainDogovor::reestr::fio) || index.column() == static_cast<int>(MainDogovor::reestr::namedog)
                     || index.column() == static_cast<int>(MainDogovor::reestr::namekomp)){
                return int(Qt::AlignLeft| Qt::AlignVCenter);
            }
            else if(index.column() == static_cast<int>(MainDogovor::reestr::nomepizod) || index.column() == static_cast<int>(MainDogovor::reestr::birthday)
                     || index.column() == static_cast<int>(MainDogovor::reestr::regnom) || index.column() == static_cast<int>(MainDogovor::reestr::datazakr)
                     || index.column() == static_cast<int>(MainDogovor::reestr::nomdog) || index.column() == static_cast<int>(MainDogovor::reestr::nomkomp)){
                return int(Qt::AlignHCenter| Qt::AlignVCenter);
            }
            else if(index.column() == static_cast<int>(MainDogovor::reestr::stoimost)){
                return int(Qt::AlignRight| Qt::AlignVCenter);
            }
            break;
//        case Qt::TextColorRole:
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::closed))).isNull())
//            {
//                return QBrush(Qt::gray,Qt::SolidPattern);
//            }
//            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(MainDogovor::dogovor::changed))).isNull())
//            {
//                return QBrush(Qt::darkGreen,Qt::SolidPattern);
//            }
//            break;
        default:
            break;

        }
        return QStandardItemModel::data(index, role);

    }

private slots:
signals:
private:
};

#endif // CUSTOMMODEL_H
