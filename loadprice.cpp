#include "loadprice.h"
#include "ui_loadprice.h"
#include "global.h"

loadPrice::loadPrice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::loadPrice)
{
    ui->setupUi(this);
    setWindowTitle("Загрузка Прайса");
    ui->dateEdit->setDate(QDate::currentDate());
    model = new QStandardItemModel;

    connect(ui->pushButton,&QPushButton::clicked,this,&loadPrice::apply);
}

loadPrice::~loadPrice()
{
    delete model;
    delete ui;
}

bool loadPrice::openFile()
{
    QString file_name = QFileDialog::getOpenFileName(this,"Выбор файла","c:\\TMP\\","Документы (*.xml)");
    if(file_name.isEmpty()){
        return false;
    }

    QFile xmlFile(file_name);
    if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this,"Ошибка загрузки XML файла !",
                              "Ошибка открытия файла " + file_name + " для загрузки.",
                              QMessageBox::Ok);
        return false;
    }
    // Получение количества тегов в файле xml ********************************************************
    QXmlStreamReader xmlReader;
    xmlReader.setDevice(&xmlFile);
    while(!xmlReader.atEnd()&& !xmlReader.hasError()){
        xmlReader.readNext();
        Count++;
    }

    xmlFile.seek(0);
    xmlReader.clear();
    xmlReader.setDevice(&xmlFile);
    parser_XMLFile(&xmlReader);
    return true;
}

void loadPrice::parser_XMLFile(QXmlStreamReader *xmlReader)
{
    QProgressDialog *ppwt;
    ppwt = new QProgressDialog("Загрузка прайса...","Отмена",0,Count
                               ,QApplication::focusWidget());
    ppwt->setWindowTitle("Wait...");
    ppwt->setMinimumDuration(0);
    ppwt->setValue(0);
    int n = 0;

    QString Duv,u,textelement;
    QList<QStandardItem*> list_items;
    int id = 0;
    while(!xmlReader->atEnd()&& !xmlReader->hasError()){
        n++;
        QXmlStreamReader::TokenType token = xmlReader->readNext();
        QStringRef tegname = QStringRef();
        if(token == QXmlStreamReader::StartDocument || token != QXmlStreamReader::StartElement){
            continue;
        }
        if(token == QXmlStreamReader::StartElement && xmlReader->name() == "Opr"){
            xmlReader->readNextStartElement();
            Duv = "";
            u = "";
            textelement = "";
            list_items.clear();
            tegname = xmlReader->name();
            token = xmlReader->tokenType();
            textelement = xmlReader->readElementText();
            while(token != QXmlStreamReader::EndElement && tegname != "Opr"){

                if( token == QXmlStreamReader::StartElement){
                    if(tegname == "Duv"){
                        Duv = textelement;
                    }
                    if(tegname == "u"){
                        u = textelement;
                    }
                }
                xmlReader->readNext();
                tegname = xmlReader->name();
                token = xmlReader->tokenType();
                textelement = xmlReader->readElementText();
                n++;
            }
            id++;
            list_items.append(new QStandardItem(QString::number(id)));
            list_items.append(new QStandardItem(Duv));
            list_items.append(new QStandardItem(u));
            model->appendRow(list_items);
            n++;
        }
        if(ppwt->wasCanceled())
        {
            delete ppwt;
            return ;
        }
        qApp->processEvents();
        ppwt->setValue(n++);
    }
    qApp->processEvents();
    ppwt->setValue(Count);
    delete ppwt;
    return ;

}

void loadPrice::apply()
{
    QSqlQuery query;
    model->sort(1);

    QProgressDialog *ppwt;
    ppwt = new QProgressDialog("Обработка прайса...","Отмена",0,model->rowCount()
                               ,QApplication::focusWidget());
    ppwt->setWindowTitle("Wait...");
    ppwt->setMinimumDuration(0);
    ppwt->setValue(0);
    int n = 0;
    qApp->processEvents();

    for(int row = 0; row < model->rowCount(); row++){
        query.prepare("select * from qmspriceusl where code_price = ?");
        query.addBindValue(model->data(model->index(row,1)).toString());
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса select * from qmspriceusl where code_price = ?.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            break;
        }
        if(!query.first()){
            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                break;
            }

            query.prepare("INSERT INTO qmspriceusl (code_price, name_price) VALUES (?,?)");
            query.addBindValue(model->data(model->index(row,1)).toString());
            query.addBindValue(model->data(model->index(row,2)).toString());

            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center'>Ошибка выполнения запроса INSERT INTO qmspriceusl (code_price, name_price) VALUES (?,?).<br><span style='color: red'>" + query.lastError().text() +"</p>");
                break;
            }
//            qDebug() << "INSERT " << model->data(model->index(row,1)).toString() << " " << model->data(model->index(row,2)).toString() << " " << model->data(model->index(row,3)).toString();
        }
        else{
            if(model->data(model->index(row,2)) != query.value("name_price")){
                int id = query.value("id").toInt();
                query.prepare("select set_config('auth.username', ?, false)");
                query.addBindValue(UserName);
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                    break;
                }
                query.prepare("update qmspriceusl set closedate = ? where id = ? ");
                query.addBindValue(ui->dateEdit->date().toString("yyyy-MM-dd"));
                query.addBindValue(id);

                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p style='text-align: center'>Ошибка выполнения запроса update qmspriceusl set closedate = ? where id = ? .<br><span style='color: red'>" + query.lastError().text() +"</p>");
                    break;
                }

//                qDebug() << "CLOSE " << query.value("id").toInt() << " " << query.value("code_price").toString() << " " << query.value("name_price").toString() << " " << ui->dateEdit->date().toString("yyyy-MM-dd");

                query.prepare("INSERT INTO qmspriceusl (code_price, name_price) VALUES (?,?)");
                query.addBindValue(model->data(model->index(row,1)).toString());
                query.addBindValue(model->data(model->index(row,2)).toString());

                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p style='text-align: center'>Ошибка выполнения запроса INSERT INTO qmspriceusl (code_price, name_price) VALUES (?,?) .<br><span style='color: red'>" + query.lastError().text() +"</p>");
                    break;
                }
//                qDebug() << "UPDATE NEW " << model->data(model->index(row,1)).toString() << " " << model->data(model->index(row,2)).toString() << " " << model->data(model->index(row,3)).toString();
            }
        }
        if(n%50 == 0){
            qApp->processEvents();
        }
        ppwt->setValue(n++);
        if(ppwt->wasCanceled())
        {
            break;
        }
    }
    delete ppwt;
    reject();
}
