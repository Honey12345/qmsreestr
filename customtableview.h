#ifndef CUSTOMTABLEVIEW_H
#define CUSTOMTABLEVIEW_H
#include <QTableView>



class MyTableView_complex : public QTableView
{
    Q_OBJECT
public:
    explicit MyTableView_complex(QWidget *)
    {
        setMouseTracking(true);
    }

    void currentChanged(const QModelIndex &current, const QModelIndex &previous)
    {
        emit changeData_complex(current);
        return QTableView::currentChanged(current, previous);
    }

signals:
    void changeData_complex(QModelIndex);

public slots:
};

class MyTableView_dogovor : public QTableView
{
    Q_OBJECT
public:
    explicit MyTableView_dogovor(QWidget *)
    {
        setMouseTracking(true);
    }

    void currentChanged(const QModelIndex &current, const QModelIndex &previous)
    {
        emit changeData_dogovor(current);
        return QTableView::currentChanged(current, previous);
    }

signals:
    void changeData_dogovor(QModelIndex);

public slots:
};


class MyTableView_dogovor_approved : public QTableView
{
    Q_OBJECT
public:
    explicit MyTableView_dogovor_approved(QWidget *)
    {
        setMouseTracking(true);
    }

    void currentChanged(const QModelIndex &current, const QModelIndex &previous)
    {
        emit changeData_dogovor_approved(current);
        return QTableView::currentChanged(current, previous);
    }

signals:
    void changeData_dogovor_approved(QModelIndex);

public slots:
};

#endif // CUSTOMTABLEVIEW_H
