#ifndef SMTPDIALOG_H
#define SMTPDIALOG_H

#include <QDialog>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QSslSocket>
#include <QString>
#include <QTextStream>
#include <QDebug>
#include <QByteArray>
#include <QFile>
#include <QFileInfo>
#include <QNetworkProxy>
#include <QTextCodec>

class smtpDialog : public QObject
{
    Q_OBJECT

public:
    explicit smtpDialog();
    ~smtpDialog();

    void setParams(const QString &user, const QString &pass, const QString &host, const QString &to, const QString &subj, const QString &body, QStringList files, int port = 465, int timeout = 30000);

    void sendMail();

signals:
    void finish();

private slots:
    void stateChanged(QAbstractSocket::SocketState socketState);
    void errorReceived(QAbstractSocket::SocketError);
    void disconnected();
    void connected();
    void readyRead();
    void Encrypted();

private:
    int timeout;
    QString message;
    QTextStream *t,streamerr;
    QSslSocket *socket;
    QString from;
    QString rcpt;
    QString response;
    QString user;
    QString pass;
    QString host;
    QString subj;
    QString body;
    QStringList files;
    int port;
    enum states{Tls, HandShake ,Auth,User,Pass,Rcpt,Mail,Data,Init,Body,Quit,Close};
    int state;
    int rcpt_count;
    QStringList listrcpt;
    QFile smtplog;
    QByteArray LogData = "";
};

#endif // SMTPDIALOG_H
