#include "enums.h"
#include "global.h"
#include "users.h"
#include "ui_users.h"
#include <QDebug>
#include <QSqlError>


users::users(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::users)
{
    ui->setupUi(this);

    extern QSettings *settings;

    if(!settings->value("user_geometry").isValid())
          resize(357, 250);
    else
        setGeometry(settings->value("user_geometry").toRect());


    model_users = new my_users_SqlQueryModel(this);
    users::setWindowTitle("Справочник пользователей");
    user_delegate * user_delegate_class = new user_delegate(this); // делегат редактирования для справлчнико пользователей
    connect(this,SIGNAL(sendNameKodaccess(const QStringList)),model_users,SLOT(reciveData_NameKodAccess(QStringList)));
    connect(this,SIGNAL(sendNameKodaccess(const QStringList)),user_delegate_class,SLOT(reciveData_NameKodAccess_delegate(QStringList)));

    connect(user_delegate_class,SIGNAL(closeEditor(QWidget*)),this,SLOT(user_refresh()));

    ui->tableView->setItemDelegate(user_delegate_class);
    reciveData_users();
}

users::~users()
{
    extern QSettings *settings;
    settings->setValue("user_geometry", geometry());
    delete ui;
}

void users::reciveData_users()
{
    QStringList str = NameKodeAccess;
    for(int i = 0;i < str.size();i++)
    {
        ui->UserRole->addItem(str.value(i));
    }
    emit sendNameKodaccess(str);

    QString strQuery = "SELECT id,name_domen,kod_access,fio,active FROM users ORDER BY name_domen ASC";
    model_users->setQuery(strQuery);

    model_users->setHeaderData(0, Qt::Horizontal, "id");
    model_users->setHeaderData(1, Qt::Horizontal, "Доменное имя");
    model_users->setHeaderData(2, Qt::Horizontal, "Уровень доступа");
    model_users->setHeaderData(3, Qt::Horizontal, "ФИО");
    model_users->setHeaderData(4, Qt::Horizontal, "Действ.");
    ui->tableView->setModel(model_users);
    ui->tableView->setColumnHidden(0,true);
    ui->tableView->setColumnWidth(1, 150);
    ui->tableView->setColumnWidth(2, 150);
    ui->tableView->setColumnWidth(3, 250);
    ui->tableView->setFocus();
    ui->tableView->selectRow(0);
}


void my_users_SqlQueryModel::reciveData_NameKodAccess(QStringList str)
{
    strKod = str;
}


void user_delegate::reciveData_NameKodAccess_delegate(QStringList str)
{
    strKod_delegate = str;
}

void users::on_AppendUser_clicked()
{
    if(ui->Login->text().isEmpty())
    {
        QMessageBox::critical(this,
                              QString::fromUtf8("Ошибка при вводе"),
                              QString::fromUtf8("Все поля должны быть заполнены!"));
        return ;
    }
    QString name = ui->Login->text().trimmed();
    QSqlQuery query;
    query.prepare("SELECT * FROM users  WHERE name_domen = ?");
    query.addBindValue(name);
    if(!query.exec())
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    };
    if(query.first())
    {
        QMessageBox::information(this, "Information", "Такое имя пользователя  существует \n измените имя пользователя!");
        ui->Login->setFocus();
        return ;
    }
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return ;
    }
    query.prepare("INSERT INTO users (name_domen, kod_access,fio) "
                  "VALUES (?,?,?)");
    query.addBindValue(ui->Login->text().trimmed());
    query.addBindValue(ui->UserRole->currentIndex());
    query.addBindValue(ui->fio->text().trimmed());

    if(query.exec())
    {

        QMessageBox *mbox = new QMessageBox(this);
        mbox->setWindowTitle(tr("Внимание"));
        mbox->setText("Данные успешно записаны");
        mbox->setStandardButtons(QMessageBox::Ok);
        mbox->setIcon(QMessageBox::Information);
        QTimer::singleShot(1500,mbox,SLOT(accept()));
        mbox->exec();
        delete mbox;
        ui->Login->clear();
        ui->fio->clear();
        ui->UserRole->setCurrentIndex(0);
        refresh(0);

    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса INSERT INTO users (name_domen, kod_access,fio) "
                              "VALUES (?,?,?).<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
}

void users::refresh(int reg)
{
    int row;
    // Если reg = 0 то добавилась новая запись
    if(reg == 0)
    {
        QSqlQuery query;
        query.exec("SELECT id FROM users");
        query.last();
        QVariant id =  query.value(0);
        // обновляем модель
        model_users->setQuery(model_users->query().executedQuery());
        // ищем в данном наборе записей, запись с нужным id, если не нашли возвращается номер строки = -1
        row = model_users->match(model_users->index(0,0),Qt::DisplayRole,id).value(0).row();

        // Выполняем цикл пока не найдем нужную строку или не подгрузим все записи

        while (model_users->canFetchMore() && row == -1) // Если можно подгружать записи в модели ( подгружается по 256 записей)
        {
            model_users->fetchMore();         // то грузим записи
            // ищем в данном наборе записей, запись с нужным id, если не нашли возвращается номер строки = -1
            row = model_users->match(model_users->index(0,0),Qt::DisplayRole,id).value(0).row();
        }
        ui->tableView->selectRow(0);
        ui->tableView->selectRow(row);        // выделяем найденную запись
        ui->tableView->setFocus();

    }
    else // запись редактировалась
    {
        QVariant id =  model_users->data(model_users->index(ui->tableView->currentIndex().row(),0));
        // обновляем модель
        model_users->setQuery(model_users->query().executedQuery());
        // ищем в данном наборе записей, запись с нужным id, если не нашли возвращается номер строки = -1
        row = model_users->match(model_users->index(0,0),Qt::DisplayRole,id).value(0).row();

        // Выполняем цикл пока не найдем нужную строку или не подгрузим все записи

        while (model_users->canFetchMore() && row == -1) // Если можно подгружать записи в модели ( подгружается по 256 записей)
        {
            model_users->fetchMore();         // то грузим записи
            // ищем в данном наборе записей, запись с нужным id, если не нашли возвращается номер строки = -1
            row = model_users->match(model_users->index(0,0),Qt::DisplayRole,id).value(0).row();
        }
        ui->tableView->selectRow(0);
        ui->tableView->selectRow(row);        // выделяем найденную запись
        ui->tableView->setFocus();
    }


}

void users::user_refresh()
{
    refresh(1);
}

void users::on_deleteUser_clicked()
{
    int row = ui->tableView->currentIndex().row(); // номер текущей строки
    // получаем уникальный id текущей записи
    QString id =  model_users->data(model_users->index(row,0,QModelIndex())).toString();
    QSqlQuery query;
    int n = QMessageBox::warning(this,"Внимание!","Вы действительно хотите удалить"
                         "\n текущую запись ?",
                         QMessageBox::Yes|QMessageBox::No,
                         QMessageBox::Yes);
    if (n != QMessageBox::Yes)
    {
        ui->tableView->setFocus();
        return;
    }
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
        db.rollback();
        return;
    }
    query.prepare("DELETE FROM users WHERE id = ?");
    query.addBindValue(id);
    if (!query.exec())
    {
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;

    }
    model_users->setQuery(model_users->query().lastQuery()); // обновляем модель после удаления строки
    if (row  >=  1) // если удаляемая строка не 0
        ui->tableView->selectRow(row-1); // выбираем предыдущую строку
    else
        ui->tableView->selectRow(0);
    ui->tableView->setFocus(); // устанавливаем фокус на просмотр заявок


}
