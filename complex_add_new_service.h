#ifndef COMPLEX_ADD_NEW_SERVICE_H
#define COMPLEX_ADD_NEW_SERVICE_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QMessageBox>
#include <QDebug>
#include <QObject>
#include <QLineEdit>


class ComplexAddService : public QObject {
public:
    /// достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int

    enum class econom :int {id,name_usl};
    Q_ENUM(econom)

    enum class price :int {id,code_price,name_price,closedata};
    Q_ENUM(price)

    Q_OBJECT
    ComplexAddService() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums
};

namespace Ui {
class complex_add_new_service;
}

class complex_add_new_service : public QDialog
{
    Q_OBJECT

public:

    enum Regim {AddServiceShablon,AddServiceComplex,AddServiceDogovor};

    explicit complex_add_new_service(QWidget *parent = nullptr, int idcomplex = 0,int regim = 0);
    ~complex_add_new_service();

    void SetStyleGroupBox();
    void SetModelPrice();
    void SetModelEconom();

public slots:

    void NotAdded();

private slots:

    void AddService();
    void AddServiceInComplex();
    void AddServiceInDogovor();
    void TableView_price_DoubleClicked(QModelIndex index);
    void TableView_econom_DoubleClicked(QModelIndex index);

    void FindeKod(QString str);
    void FindeNamePrice(QString str);
    void FindeNameUsl(QString str);
    void HeaderDoubleClickedPrice(int index);
    void HeaderDoubleClickedUsl(int index);

signals:

    void addServiceInComplex(int,int);
    void addServiceInShablon();
    void addServiceInDogovor(int);

private:
    Ui::complex_add_new_service *ui;
    QSqlQueryModel *model_econom, *model_price;
    int ideconom = 0;
    int idprice = 0;
    int iDcomplex;
    QLineEdit *findeKod;
    QLineEdit *findeNamePrice;
    QLineEdit *findeNameUsl;
    int rEgim;
};

#endif // COMPLEX_ADD_NEW_SERVICE_H
