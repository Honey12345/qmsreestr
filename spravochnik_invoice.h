#ifndef SPRAVOCHNIK_INVOICE_H
#define SPRAVOCHNIK_INVOICE_H

#include <QDialog>
#include <QSqlQueryModel>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDate>
#include <QMessageBox>
#include <QSqlDriver>
#include <QSqlError>
#include <QTimer>

namespace Ui {
class spravochnik_invoice;
}

class spravochnik_invoice : public QDialog
{
    Q_OBJECT

public:

    enum TypeRefresh {Init,Dell,Insert};
    explicit spravochnik_invoice(QWidget *parent = 0);
    ~spravochnik_invoice();
    void refresh_model(TypeRefresh type = TypeRefresh::Init, int row = 0);

private slots:
    void on_pushButton_exit_clicked();
    void setInvoiceNumber(QString);

    void on_pushButton_add_clicked();

    void on_pushButton_del_clicked();

private:
    Ui::spravochnik_invoice *ui;
    QSqlQueryModel *model_invoice;
    QVariant invoiceid_return;
};

#endif // SPRAVOCHNIK_INVOICE_H
