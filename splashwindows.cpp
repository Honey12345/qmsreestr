#include "splashwindows.h"
#include "ui_splashwindows.h"
#include <QMovie>
#include <QDebug>

splashWindows::splashWindows(QWidget *parent, QString text) :
    QDialog(parent),
    ui(new Ui::splashWindows)
{
    ui->setupUi(this);
   // this->setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint | Qt::WindowContextHelpButtonHint);
    this->setWindowTitle("Load...");
    elaps = new QTime(0,0,0);
    ui->label_3->setText(elaps->toString("hh:mm:ss"));
    /* Инициализируем Таймер и подключим его к слоту,
     * который будет обрабатывать timeout() таймера
     * */
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(slotTimerAlarm()));

    if(!text.isEmpty())
        ui->label_2->setText(text);
    m_movie = new QMovie(":/icons/load.gif");
    ui->label->setMovie(m_movie);
    m_movie->start();
    timer->start(1000); // И запустим таймер
}

splashWindows::~splashWindows()
{
    delete elaps;
    delete timer;
    delete ui;
}

bool splashWindows::splashWindowsSqlExec()
{
    return sqlexec;
}

void splashWindows::setText(QString text)
{
    ui->label_2->setText(text);
}


void splashWindows::startSplash()
{
//    splash->show();
    //    splash->showMessage("Загрузка данных",Qt::AlignTop | Qt::AlignCenter, QColor(Qt::white));
}

void splashWindows::slotTimerAlarm()
{
    *elaps = elaps->addSecs(1);
    ui->label_3->setText(elaps->toString("hh:mm:ss"));
}

void splashWindows::splashWindowsEnd(bool ok)
{
    sqlexec = ok;
    accept();
}

