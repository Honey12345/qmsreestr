#include "dogovor_add.h"
#include "ui_dogovor_add.h"
#include "complex.h"
#include "complex_add_new_service.h"
#include "global.h"
#include "dogovor_set_code.h"

dogovor_add::dogovor_add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dogovor_add)
{
    ui->setupUi(this);

    if(settings->value("dogovor_add").isValid()){
        setGeometry(settings->value("dogovor_add").toRect());
    }

    model_services = new my_Dogovor_StandardItemmodel(this);
    delegate = new dogovor_service_delegate(this);
    ui->tableView_services->setModel(model_services);
    ui->tableView_services->setItemDelegateForColumn(static_cast<int>(Dogovor::services::code_service),delegate);
    ui->tableView_services->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    connect(ui->pushButton_add_shablon,&QPushButton::clicked,this,&dogovor_add::AddShablonComplex);
    connect(ui->pushButton_add_usl,&QPushButton::clicked,this,&dogovor_add::AddService);
    connect(ui->pushButton_del_usl,&QPushButton::clicked,this,&dogovor_add::DelService);
    connect(ui->pushButton_cancel,&QPushButton::clicked,this,&dogovor_add::reject);
    connect(ui->pushButton_del_all,&QPushButton::clicked,this,&dogovor_add::DeleteAll);
    connect(ui->pushButton_set_code,&QPushButton::clicked,this,&dogovor_add::SetCode);
    connect(ui->pushButton_set_code_price,&QPushButton::clicked,this,&dogovor_add::SetCodePrice);
    connect(ui->pushButton_save,&QPushButton::clicked,this,&dogovor_add::SaveData);


//    connect(ui->lineEdit_number_dog,&QLineEdit::textChanged,this,&dogovor_add::CheckSave);
//    connect(ui->lineEdit_code_complex,&QLineEdit::textChanged,this,&dogovor_add::CheckSave);
//    connect(ui->lineEdit_kontragent,&QLineEdit::textChanged,this,&dogovor_add::CheckSave);
//    connect(ui->lineEdit_name_complex,&QLineEdit::textChanged,this,&dogovor_add::CheckSave);
//    connect(model_services,&QStandardItemModel::rowsInserted,this,&dogovor_add::CheckSave);
//    connect(model_services,&QStandardItemModel::rowsRemoved,this,&dogovor_add::CheckSave);
//    connect(model_services,&QStandardItemModel::itemChanged,this,&dogovor_add::CheckSave);

    connect(ui->tableView_services->selectionModel(),&QItemSelectionModel::selectionChanged,this,&dogovor_add::SelectionChanged);
    ui->lineEdit_codecomplexqms->setStyleSheet("QLineEdit { background: rgb(225, 225, 225); }");

}

dogovor_add::~dogovor_add()
{
    settings->setValue("dogovor_add", geometry());
    delete delegate;
    delete model_services;
    delete ui;
}

bool dogovor_add::SetData(int regim, int iddogovor)
{
    rEgim = regim;
    if(rEgim == Regim::AddDogovor){
        this->setWindowTitle("Добавление нового договора");
        model_services->setColumnCount(6);
        ui->dateEdit_start->setDate(QDate::currentDate());
        ui->dateEdit_finish->setDate(QDate::currentDate());
    }
    if(rEgim == Regim::AddComplex){
        idDogovor = iddogovor;
        this->setWindowTitle("Добавление нового комплекса в текущий договор");
        model_services->setColumnCount(6);
        QSqlQuery query;
        query.prepare("select * from dogovor where iddogovor = ?");
        query.addBindValue(idDogovor);
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return false;
        }
        query.first();
        ui->lineEdit_number_dog->setText(query.value("numberdogovor").toString());
        SetReadOnly(ui->lineEdit_number_dog);
        ui->lineEdit_kontragent->setText(query.value("namekontragent").toString());
        SetReadOnly(ui->lineEdit_kontragent);
        ui->dateEdit_start->setDate(query.value("start").toDate());
        SetReadOnly(ui->dateEdit_start);
        ui->dateEdit_finish->setDate(query.value("finish").toDate());
        SetReadOnly(ui->dateEdit_finish);
    }
    model_services->setHeaderData(static_cast<int>(Dogovor::services::code_service),Qt::Horizontal,"Код");
    model_services->setHeaderData(static_cast<int>(Dogovor::services::name_usl),Qt::Horizontal,"Наименование");
    model_services->setHeaderData(static_cast<int>(Dogovor::services::code_price),Qt::Horizontal,"Прайс");
    model_services->setHeaderData(static_cast<int>(Dogovor::services::name_price),Qt::Horizontal,"Наименвание по прайсу");

    ui->tableView_services->setColumnHidden(static_cast<int>(Dogovor::services::ideconom),true);
    ui->tableView_services->setColumnHidden(static_cast<int>(Dogovor::services::idprice),true);

    ui->tableView_services->setColumnWidth(static_cast<int>(Dogovor::services::code_service),150);
    ui->tableView_services->setColumnWidth(static_cast<int>(Dogovor::services::name_usl),350);
    ui->tableView_services->setColumnWidth(static_cast<int>(Dogovor::services::code_price),150);
    ui->tableView_services->setColumnWidth(static_cast<int>(Dogovor::services::name_price),350);
    return true;
}

void dogovor_add::AddShablonComplex()
{
    complex *d = new complex(this,1);
    if(!d->SetModelComplex()){
        return;
    }
    if(d->exec()){
        int idcomplex = d->GetIdComplex();
        QSqlQuery query;
        query.prepare("select name_usl,code_price,name_price,ideconom,idprice from complex_services"
                      " left join econom_usl on econom_usl.id = ideconom"
                      " left join qmspriceusl on qmspriceusl.id = idprice"
                      " where idcomplex = ? order by name_usl");
        query.addBindValue(idcomplex);
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        }
        query.first();
        QList<QStandardItem*> list;
        do{
            list.clear();
            list.append(new QStandardItem(""));
            list.append(new QStandardItem(query.value("name_usl").toString()));
            list.append(new QStandardItem(query.value("code_price").toString()));
            list.append(new QStandardItem(query.value("name_price").toString()));
            list.append(new QStandardItem(query.value("ideconom").toString()));
            list.append(new QStandardItem(query.value("idprice").toString()));
            model_services->appendRow(list);
        }while(query.next());
        model_services->sort(static_cast<int>(Dogovor::services::name_usl));
        query.prepare("select namecomplex,codecomplexqms from complex where idcomplex = ?");
        query.addBindValue(idcomplex);
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        }
        query.first();
        ui->lineEdit_name_complex->setText(query.value("namecomplex").toString());
        ui->lineEdit_codecomplexqms->setText(query.value("codecomplexqms").toString());
        SelectionChanged();
    }
    delete d;
}

void dogovor_add::AddService()
{
    complex_add_new_service *d = new complex_add_new_service(this,0,1);
    connect(d,&complex_add_new_service::addServiceInComplex,this,&dogovor_add::SignalAddService);
    connect(this,&dogovor_add::NotAdded,d,&complex_add_new_service::NotAdded);
    d->exec();
    disconnect(d,&complex_add_new_service::addServiceInComplex,this,&dogovor_add::SignalAddService);
    disconnect(this,&dogovor_add::NotAdded,d,&complex_add_new_service::NotAdded);
    delete d;

}

void dogovor_add::SignalAddService(int ideconom, int idprice)
{
    QSqlQuery query;
    QList<QStandardItem*> list;
    QModelIndexList listeconom = model_services->match(model_services->index(0,static_cast<int>(Dogovor::services::ideconom)),Qt::DisplayRole,ideconom,Qt::MatchFixedString);
    QModelIndexList listprice = model_services->match(model_services->index(0,static_cast<int>(Dogovor::services::idprice)),Qt::DisplayRole,idprice,Qt::MatchFixedString);
    if(!listeconom.isEmpty() || !listprice.isEmpty()){
        emit NotAdded();;
    }
    else{
        query.prepare("select * from econom_usl,qmspriceusl where econom_usl.id = ? and qmspriceusl.id = ?");
        query.addBindValue(ideconom);
        query.addBindValue(idprice);
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        }
        query.first();
        list.append(new QStandardItem(""));
        list.append(new QStandardItem(query.value("name_usl").toString()));
        list.append(new QStandardItem(query.value("code_price").toString()));
        list.append(new QStandardItem(query.value("name_price").toString()));
        list.append(new QStandardItem(query.value("econom_usl.id").toString()));
        list.append(new QStandardItem(query.value("qmspriceusl.id").toString()));
        model_services->appendRow(list);
        model_services->sort(static_cast<int>(Dogovor::services::name_usl));
        SelectionChanged();
    }
}

void dogovor_add::DelService()
{
    QModelIndex index = ui->tableView_services->currentIndex();
    model_services->removeRow(index.row());
    ui->tableView_services->selectRow(ui->tableView_services->currentIndex().row());
}

void dogovor_add::DeleteAll()
{
    model_services->removeRows(0,model_services->rowCount());
    SelectionChanged();
}

bool dogovor_add::CheckSave()
{
    if(ui->lineEdit_number_dog->text().isEmpty()){
        QMessageBox::information(this, "Information", "<p style='text-align: center; color: blue'>Заполните номер договора!</p>");
        return false;
    }
    if(ui->lineEdit_kontragent->text().isEmpty()){
        QMessageBox::information(this, "Information", "<p style='text-align: center; color: blue'>Заполните наименование контрагента!</p>");
        return false;
    }
    if(ui->lineEdit_code_complex->text().isEmpty()){
        QMessageBox::information(this, "Information", "<p style='text-align: center; color: blue'>Заполните номер комплекса!</p>");
        return false;
    }
    if(ui->lineEdit_name_complex->text().isEmpty()){
        QMessageBox::information(this, "Information", "<p style='text-align: center; color: blue'>Заполните наименование комплекса!</p>");
        return false;
    }
    if(ui->lineEdit_codecomplexqms->text().isEmpty()){
        QMessageBox::information(this, "Information", "<p style='text-align: center; color: blue'>Код комплекса qMS должен быть заполнен!</p>");
        return false;
    }
    if(model_services->rowCount() == 0){
        QMessageBox::information(this, "Information", "<p style='text-align: center; color: blue'>Добавте список услуг в договор!</p>");
        ui->pushButton_set_code->setEnabled(false);
        ui->pushButton_set_code_price->setEnabled(false);
        return false;
    }
    else{
        ui->pushButton_set_code->setEnabled(true);
        ui->pushButton_set_code_price->setEnabled(true);
    }
    for(int row = 0; row < model_services->rowCount(); row++){
        if(model_services->data(model_services->index(row,static_cast<int>(Dogovor::services::code_service)),Qt::DisplayRole).toString().trimmed().isEmpty()){
            QMessageBox::information(this, "Information", "<p style='text-align: center; color: blue'>Заполните коды услуг в договоре!</p>");
            return false;
        }
    }
    return true;
}

void dogovor_add::SetCode()
{
    QModelIndexList list = ui->tableView_services->selectionModel()->selectedRows();
    dogovor_set_code *d = new dogovor_set_code(this);
    if(d->exec()){
        QString code = d->GetCode();
        QString code_const = "";
        QString code_separate = "";
        QString code_count = " ";
        int count;
        if(code.contains(".")){
            code_const = code.left(code.indexOf("."));
            code_separate =".";
            code_count = code.right(code.length()-code.indexOf(".")-1);
        }else if(code.contains("-")){
            code_const = code.left(code.indexOf("-"));
            code_separate ="-";
            code_count = code.right(code.length()-code.indexOf("-")-1);
        }
        else{
            code_count = code;
        }
        count = code_count.toInt();
        for(int i = 0; i < list.length(); i++){
            code = code_const + code_separate + QString::number(count);
            if(model_services->match(model_services->index(0,static_cast<int>(Dogovor::services::code_service)),Qt::DisplayRole,code,1,Qt::MatchFixedString).length() != 0){
                QMessageBox::information(this, "Information", "Такой код существует!");
                count++;
                continue;
            }
            QModelIndex index = list.at(i);
            model_services->setData(model_services->index(index.row(),static_cast<int>(Dogovor::services::code_service)),QVariant(code));
            count++;
        }
    }
    delete d;
}

void dogovor_add::SetCodePrice()
{
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: black'>Проставить коды комплекса в соответствии :<br>с кодами прайса, построчно ?</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    QString code;
    for(int row = 0; row < model_services->rowCount(); row ++ ){
        code =  model_services->data(model_services->index(row,static_cast<int>(Dogovor::services::code_price))).toString();
        model_services->setData(model_services->index(row,static_cast<int>(Dogovor::services::code_service)),QVariant(code));
    }
}

void dogovor_add::SaveData()
{
    if(!CheckSave()){
        return;
    }
    QSqlQuery query;
    if(rEgim == Regim::AddDogovor){
        query.prepare("select * from dogovor where numberdogovor = ? and closed isnull");
        query.addBindValue(ui->lineEdit_number_dog->text().trimmed());
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        }
        if(query.first())
        {
            QMessageBox::information(this, "Information", "Такое номер договора есть в системе !\n измените номер договора!");
            return ;
        }
    }
    query.prepare("select * from dogovor"
                  " left join dogovor_complex using(iddogovor)"
                  " where numberdogovor = ? and (codecomplex = ? or namecomplex = ?)");
    query.addBindValue(ui->lineEdit_number_dog->text().trimmed());
    query.addBindValue(ui->lineEdit_code_complex->text().trimmed());
    query.addBindValue(ui->lineEdit_name_complex->text().trimmed());
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(query.first())
    {
        QMessageBox::information(this, "Information", "Такое код или название комплекса есть в этом договоре !\n измените данные комплекса!");
        return ;
    }
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {

        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return;
        }
        else{
            int iddogovor = idDogovor;
            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            query.exec();
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
            if(rEgim == Regim::AddDogovor){
                query.prepare("insert into dogovor(numberdogovor,namekontragent,start,finish) values(?,?,?,?) returning iddogovor");
                query.addBindValue(ui->lineEdit_number_dog->text().trimmed());
                query.addBindValue(ui->lineEdit_kontragent->text().trimmed());
                query.addBindValue(ui->dateEdit_start->date().toString("yyyy-MM-dd"));
                query.addBindValue(ui->dateEdit_finish->date().toString("yyyy-MM-dd"));
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                    db.rollback();
                    return;
                }
                query.first();
                iddogovor = query.value("iddogovor").toInt();
            }
            query.prepare("insert into dogovor_complex(iddogovor,codecomplex,namecomplex,stoimost,codecomplexqms) values(?,?,?,?,?) returning idcomplex");
            query.addBindValue(iddogovor);
            query.addBindValue(ui->lineEdit_code_complex->text().trimmed());
            query.addBindValue(ui->lineEdit_name_complex->text().trimmed());
            query.addBindValue(ui->doubleSpinBox_stoimost->value());
            query.addBindValue(ui->lineEdit_codecomplexqms->text().trimmed());
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                db.rollback();
                return;
            }
            query.first();
            int idcomplex = query.value("idcomplex").toInt();
            for(int row = 0; row < model_services->rowCount(); row++){
                query.prepare("insert into dogovor_complex_services(idcomplex,ideconom,idprice,code_service) values(?,?,?,?)");
                query.addBindValue(idcomplex);
                query.addBindValue(model_services->data(model_services->index(row,static_cast<int>(Dogovor::services::ideconom))).toInt());
                query.addBindValue(model_services->data(model_services->index(row,static_cast<int>(Dogovor::services::idprice))).toInt());
                query.addBindValue(model_services->data(model_services->index(row,static_cast<int>(Dogovor::services::code_service))).toString());
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                    db.rollback();
                    return;
                }
            }
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
               return;
            }
            idDogovor = iddogovor;
            idComplex = idcomplex;
            QMessageBox *mbox = new QMessageBox(this);
            mbox->setWindowTitle(tr("Внимание"));
            mbox->setText("Данные успешно сохранены.");
            mbox->setModal(true);
            mbox->setStandardButtons(QMessageBox::Ok);
            mbox->setIcon(QMessageBox::Information);
            QTimer::singleShot(1500,mbox,SLOT(accept()));
            mbox->exec();
            return accept();
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return ;
    }

}

void dogovor_add::SelectionChanged()
{
    QModelIndexList list = ui->tableView_services->selectionModel()->selectedRows();
    if(list.length() > 1){
        ui->pushButton_set_code->setEnabled(true);
    }
    else{
        ui->pushButton_set_code->setEnabled(false);
    }

}

int dogovor_add::GetIdDogovor()
{
    return idDogovor;
}

int dogovor_add::GetIdComplex()
{
    return idComplex;
}

void dogovor_add::SetReadOnly(QWidget *widget)
{
    if(widget->metaObject()->className() == QString("QLineEdit")){
        QLineEdit *lineEdit = static_cast<QLineEdit*>(widget);
        lineEdit->setReadOnly(true);
        lineEdit->setStyleSheet("QLineEdit { color: blue ; background-color: lightGray }");
    };
    if(widget->metaObject()->className() == QString("QDateEdit")){
        QDateEdit *dateEdit = static_cast<QDateEdit*>(widget);
        dateEdit->setReadOnly(true);
        dateEdit->setStyleSheet("QDateEdit { color: blue ; background-color: lightGray }");
    };
}
