#include "set_smtp_server.h"
#include "ui_set_smtp_server.h"
#include "global.h"

set_smtp_server::set_smtp_server(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::set_smtp_server)
{
    ui->setupUi(this);
    if(settings->value("smtpservers").isValid()){
        setGeometry(settings->value("smtpservers").toRect());
    }

    model_smtp_server = new QSqlQueryModel;

    ui->server->setStyleSheet("QLineEdit { background: rgb(231, 237, 252); }");
    ui->port->setCurrentIndex(-1);
    ui->uname->setStyleSheet("QLineEdit { background: rgb(231, 237, 252); }");
    ui->port->setStyleSheet("QComboBox { background: rgb(231, 237, 252); }");

    ui->tableView->setModel(model_smtp_server);

    connect(ui->tableView,&QTableView::doubleClicked,this,&set_smtp_server::TableViewDoubleClicked);
    this->setWindowTitle("Настройка сервера отправки сообщений");

}

set_smtp_server::~set_smtp_server()
{
    settings->setValue("smtpservers", geometry());
    delete model_smtp_server;
    delete ui;
}

bool set_smtp_server::SetSqlQuery()
{
    QSqlQuery query;
    query.prepare("select * from smtpservers order by servername,username  ");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    model_smtp_server->setQuery(query);
    return true;
}

bool set_smtp_server::SetSqlQueryModel()
{
    if(!SetSqlQuery()){
        return false;
    }
    model_smtp_server->setHeaderData(static_cast<int>(set_smtp_server::smtp_server::idsmtpserver),Qt::Horizontal,"id");
    model_smtp_server->setHeaderData(static_cast<int>(set_smtp_server::smtp_server::servername),Qt::Horizontal,"Server");
    model_smtp_server->setHeaderData(static_cast<int>(set_smtp_server::smtp_server::serverport),Qt::Horizontal,"Port");
    model_smtp_server->setHeaderData(static_cast<int>(set_smtp_server::smtp_server::username),Qt::Horizontal,"Username");
    model_smtp_server->setHeaderData(static_cast<int>(set_smtp_server::smtp_server::password),Qt::Horizontal,"password");
    model_smtp_server->setHeaderData(static_cast<int>(set_smtp_server::smtp_server::active),Qt::Horizontal,"active");

    ui->tableView->setColumnHidden(static_cast<int>(set_smtp_server::smtp_server::idsmtpserver),true);

    ui->tableView->setColumnWidth(static_cast<int>(set_smtp_server::smtp_server::servername),150);
    ui->tableView->setColumnWidth(static_cast<int>(set_smtp_server::smtp_server::serverport),50);
    ui->tableView->setColumnWidth(static_cast<int>(set_smtp_server::smtp_server::username),250);
    ui->tableView->setColumnWidth(static_cast<int>(set_smtp_server::smtp_server::password),150);

    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(set_smtp_server::smtp_server::servername),QHeaderView::Stretch);
    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(set_smtp_server::smtp_server::username),QHeaderView::Stretch);

    ui->tableView->selectRow(0);
    return true;
}

void set_smtp_server::on_pushButton_add_clicked()
{
    if(ui->server->text().isEmpty() || ui->uname->text().isEmpty()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Заполните обязательные поля!</p>");
        return;
    }
    QSqlQuery query;
    query.prepare("insert into smtpservers (servername,serverport,username,password)"
                  "values(?,?,?,?) returning idsmtpserver");
    query.addBindValue(ui->server->text().trimmed());
    query.addBindValue(ui->port->currentText().toShort());
    query.addBindValue(ui->uname->text().trimmed());
    query.addBindValue(ui->paswd->text().trimmed());
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return ;
    }
    query.first();
    QString idsmtpserver = query.value("idsmtpserver").toString();
    SetSqlQuery()    ;
    QModelIndexList list = model_smtp_server->match(model_smtp_server->index(0,0),Qt::DisplayRole,QVariant(idsmtpserver));
    if(!list.isEmpty()){
        ui->tableView->selectRow(list.at(0).row());
        ui->tableView->setFocus();
    }
    ui->server->clear();
    ui->port->setCurrentIndex(-1);
    ui->uname->clear();
    ui->paswd->clear();
}

//void set_smtp_server::on_pushButton_clicked()
//{
//    QString file_name = QFileDialog::getOpenFileName(this,"Выбор файла","c:\\","Документы (*.cer)");
//    if(file_name.isEmpty()){
//        ui->certificate->setText("");
//        return ;
//    }
//    ui->certificate->setText(file_name.replace("/","\\"));
//}

void set_smtp_server::on_pushButton_del_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    if(!index.isValid()){
        return;
    }
    int idsmtpserver = model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::idsmtpserver))).toInt();
    QString server_port = model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::servername))).toString()+ " : " +
             model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::serverport))).toString();
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: black'>Удалить почтовый сервер :<br><span style='color: blue'>" + server_port +"</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    int row = ui->tableView->currentIndex().row();
    QSqlQuery query;
    query.prepare("delete from smtpservers where idsmtpserver = ?");
    query.addBindValue(idsmtpserver);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

        return;
    }
    SetSqlQuery();
    if(row > 1){
        row = row-1;
    }
    else{
        row = 0;
    }
    ui->tableView->selectRow(row);
    ui->tableView->setFocus();

}

void set_smtp_server::TableViewDoubleClicked(QModelIndex index)
{
    int idsmtpserver = model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::idsmtpserver))).toInt();
    int row = index.row();
    QSqlQuery query;
    if(index.column() == static_cast<int>(set_smtp_server::smtp_server::active)){
        if(!model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::active))).toBool()){
            query.prepare("update smtpservers set active = false where active = true");
            query.exec();
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                return;
            }
        }
        query.prepare("update smtpservers set active = true where idsmtpserver = ?");
        query.addBindValue(idsmtpserver);
        query.exec();
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

            return;
        }
        SetSqlQuery();
        ui->tableView->selectRow(row);
        ui->tableView->setFocus();
    }
    else{

        ui->server->setText(model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::servername))).toString());
        ui->uname->setText(model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::username))).toString());
        ui->paswd->setText(model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::password))).toString());
        ui->port->setCurrentIndex(ui->port->findText(model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::serverport))).toString()));
    }
}

void set_smtp_server::on_pushButton_save_clicked()
{
    QModelIndex index = ui->tableView->currentIndex();
    if(!index.isValid()){
        return;
    }
    if(ui->server->text().isEmpty() || ui->uname->text().isEmpty()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Заполните обязательные поля!</p>");
        return;
    }
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 14pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: blue'>Изменить текущую запись?</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    int idsmtpserver = model_smtp_server->data(model_smtp_server->index(index.row(),static_cast<int>(set_smtp_server::smtp_server::idsmtpserver))).toInt();
    int row = index.row();
    QSqlQuery query;
    query.prepare("update smtpservers set servername = ?, serverport = ?, username = ?, password = ? where idsmtpserver = ?");
    query.addBindValue(ui->server->text().trimmed());
    query.addBindValue(ui->port->currentText().toShort());
    query.addBindValue(ui->uname->text().trimmed());
    query.addBindValue(ui->paswd->text().trimmed());
    query.addBindValue(idsmtpserver);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return ;
    }
    SetSqlQuery();
    ui->tableView->selectRow(row);
    ui->tableView->setFocus();

    ui->server->clear();
    ui->port->setCurrentIndex(-1);
    ui->uname->clear();
    ui->paswd->clear();

}
