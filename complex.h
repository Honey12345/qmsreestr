#ifndef COMPLEX_H
#define COMPLEX_H

#include "global.h"
#include <QDialog>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QSqlRecord>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QComboBox>
#include <QKeyEvent>
#include <QItemDelegate>
#include <QLineEdit>
#include <QVariant>
#include <QObject>
#include <QDebug>

class Complex : public QObject {
public:
    /// достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int

    enum class complex :int {idcomplex,namecomplex,codecomplexqms};
    Q_ENUM(complex)


    enum class complex_services :int {name_usl,code_price,name_price,idservis,idcomplex,ideconom,idprice};
    Q_ENUM(complex_services)


    Q_OBJECT
    Complex() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums
};


//****************************************** Делегат для редактирования наименования комплекса ********************************

class complex_delegate : public QItemDelegate
{
    Q_OBJECT
public:

    QWidget * parent_w;

    explicit complex_delegate(QWidget *parent)
    {
        parent_w = parent;
    }

    ~complex_delegate()
    {

    }

    // функция инициации процесса редактирования
    bool editorEvent(QEvent *event, QAbstractItemModel *, const QStyleOptionViewItem , const QModelIndex )
    {
        if(event->type()== QEvent::KeyPress)
        {
            QKeyEvent *KeyEvent = static_cast<QKeyEvent*>(event);

            if (KeyEvent->key() == Qt::Key_F2)
            {
                return false;
            }else
                return true; // если не F2 то не редактируем
        }

        if(event->type() != QEvent::MouseButtonDblClick){ //  не двойной клик мышкой
            return false;  // если не двойной клик то выполняем то что положено на 1 клик
        }

        return true;
    }

/* создаем виджет редактора значения    */
//QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
//{
//    if(index.column() == 2 || index.column() == 4) // Если хотим изменить поле уровня доступа
//    {
//        QComboBox *editor = new QComboBox(parent);
//        return editor;
//    }

//  return QItemDelegate::createEditor(parent,option,index);
//}

/*  передаем значение от модели в редактор  */
//void setEditorData(QWidget *editor, const QModelIndex &index) const
//{
//    if(index.column() == 2)
//    {
//        QComboBox *dostup = static_cast<QComboBox*>(editor);
//        for(int i=0; i<strKod_delegate.size(); i++)
//        {
//            dostup->addItem(strKod_delegate.value(i));
//        }
//        dostup->setCurrentText(index.model()->data(index,Qt::DisplayRole).toString());
//        return;
//    }
//    if(index.column()== 1 || index.column() == 3)
//    {
//        QLineEdit *name = static_cast<QLineEdit*>(editor);
//        name->setText(index.model()->data(index,Qt::DisplayRole).toString().trimmed());
//        return;
//    }
//    if(index.column() == 4)
//    {
//        QComboBox *dostup = static_cast<QComboBox*>(editor);
//        dostup->addItem("ДА");
//        dostup->addItem("НЕТ");
//        dostup->setCurrentText(index.model()->data(index,Qt::DisplayRole).toString());
//        return;
//    }

//    return QItemDelegate::setEditorData(editor,index);
//}
    //передает значение от редактора в модель
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
    {
        QLineEdit *name = static_cast<QLineEdit*>(editor);
        if(name->text().trimmed() == model->data(index).toString()){
            return;
        }
        QString idcomplex =  model->data(model->index(index.row(),static_cast<int>(Complex::complex::idcomplex),QModelIndex())).toString();
        QSqlQuery query;
        query.prepare("SELECT * FROM complex  WHERE namecomplex = ?");
        query.addBindValue(name->text().trimmed());
        if(!query.exec())
        {
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        };
        if(query.first())
        {
            QMessageBox::information(parent_w, "Information", "Такое наименование комплекса  существует !");
            return;
        }
        query.prepare("select set_config('auth.username', ?, false)");
        query.addBindValue(UserName);
        if(!query.exec()){
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return ;
        }
        query.prepare("UPDATE complex SET namecomplex = ? WHERE idcomplex = ?");
        query.addBindValue(name->text().trimmed());
        query.addBindValue(idcomplex);
        if(!query.exec())
        {
            QMessageBox::critical(parent_w,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        }
        return QItemDelegate::setModelData(editor,model,index);
    }
};



namespace Ui {
class complex;
}

class complex : public QDialog
{
    Q_OBJECT

public:
    explicit complex(QWidget *parent = nullptr, int regim = 0);
    ~complex();

    bool SetModelComplex();
    void SetModelHeaderData();

    int GetIdComplex();



private slots:

    void SetModelComplexServices();
    void AddNewComplex();
    void DeleteComplex();
    void DeleteService();
    void AddService();
    void SignalAddService();
    void CopyComplex();

    void DoubleClickedComplex(QModelIndex index);

private:
    Ui::complex *ui;
    QStandardItemModel *model_complex;
    QSqlQueryModel *model_complex_services;
    complex_delegate *delegate;
    int iDcomplex = 0;
};

#endif // COMPLEX_H
