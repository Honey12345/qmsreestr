#include "complex.h"
#include "ui_complex.h"
#include "global.h"
#include "complex_add_new.h"
#include "complex_add_new_service.h"

complex::complex(QWidget *parent, int regim) :
    QDialog(parent),
    ui(new Ui::complex)
{
    ui->setupUi(this);
    if(settings->value("complex").isValid()){
        setGeometry(settings->value("complex").toRect());
    }
    this->setWindowTitle("Комплексы (Шаблоны)");
    model_complex = new QStandardItemModel;
    model_complex_services = new QSqlQueryModel;
    ui->tableView->setModel(model_complex);
    ui->tableView_complex_services->setModel(model_complex_services);

    delegate = new complex_delegate(this);

    if(regim == 0){
        ui->tableView->setItemDelegateForColumn(static_cast<int>(Complex::complex::namecomplex),delegate);

        connect(ui->pushButton_add_complex,&QPushButton::clicked,this,&complex::AddNewComplex);
        connect(ui->pushButton_del_complex,&QPushButton::clicked,this,&complex::DeleteComplex);
        connect(ui->pushButton_del_usl,&QPushButton::clicked,this,&complex::DeleteService);
        connect(ui->pushButton_add_usl,&QPushButton::clicked,this,&complex::AddService);
        connect(ui->pushButton_copy_complex,&QPushButton::clicked,this,&complex::CopyComplex);
    }
    else{
        ui->pushButton_add_complex->setHidden(true);
        ui->pushButton_del_complex->setHidden(true);
        ui->pushButton_del_usl->setHidden(true);
        ui->pushButton_add_usl->setHidden(true);
        ui->pushButton_copy_complex->setHidden(true);
        connect(ui->tableView,&MyTableView_complex::doubleClicked,this,&complex::DoubleClickedComplex);
    }

}

complex::~complex()
{
    settings->setValue("complex", geometry());
    delete delegate;
    delete model_complex;
    delete model_complex_services;
    delete ui;
}

bool complex::SetModelComplex()
{
    QSqlQuery query;
    model_complex->clear();
    query.prepare("select * from complex order by namecomplex");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    if(query.first()){
        QList<QStandardItem*> list;
        do{
            list.clear();
            for(int col = 0; col < query.record().count(); col++){
                list.append(new QStandardItem(query.value(col).toString()));
            }
            model_complex->appendRow(list);
        }while(query.next());
    }
    else {
        model_complex->setColumnCount(2);
        SetModelComplexServices();
    }
    SetModelHeaderData();
    connect(ui->tableView,&MyTableView_complex::changeData_complex,this,&complex::SetModelComplexServices);

    if(model_complex->rowCount() != 0){
        ui->tableView->selectRow(0);
        ui->pushButton_copy_complex->setEnabled(true);
    }
    else{
        ui->pushButton_copy_complex->setEnabled(false);
    }
    return true;
}

void complex::SetModelHeaderData()
{
    model_complex->setHeaderData(static_cast<int>(Complex::complex::namecomplex),Qt::Horizontal," Наименование комплекса ");
    model_complex->setHeaderData(static_cast<int>(Complex::complex::codecomplexqms),Qt::Horizontal," Код qMS ");
    ui->tableView->setColumnHidden(static_cast<int>(Complex::complex::idcomplex),true);
    ui->tableView->setColumnWidth(static_cast<int>(Complex::complex::codecomplexqms),100);
    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(Complex::complex::namecomplex),QHeaderView::Stretch);
    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(Complex::complex::codecomplexqms),QHeaderView::Fixed);

}

int complex::GetIdComplex()
{
    return iDcomplex;
}

void complex::SetModelComplexServices()
{
    QModelIndex index = ui->tableView->currentIndex();
    int idcomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(Complex::complex::idcomplex))).toInt();
    QSqlQuery query;
    query.prepare("select name_usl,code_price,name_price,idservices,idcomplex,ideconom,idprice from complex_services"
                  " left join econom_usl on econom_usl.id = ideconom"
                  " left join qmspriceusl on qmspriceusl.id = idprice"
                  " where idcomplex = ?");
    query.addBindValue(idcomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    model_complex_services->setQuery(query);
    model_complex_services->setHeaderData(static_cast<int>(Complex::complex_services::name_usl),Qt::Horizontal,"Наименование услуги комплекса");
    model_complex_services->setHeaderData(static_cast<int>(Complex::complex_services::code_price),Qt::Horizontal,"Код по прайсу");
    model_complex_services->setHeaderData(static_cast<int>(Complex::complex_services::name_price),Qt::Horizontal,"Наименование по прайсу");
    ui->tableView_complex_services->setColumnWidth(static_cast<int>(Complex::complex_services::name_usl),350);
    ui->tableView_complex_services->setColumnWidth(static_cast<int>(Complex::complex_services::code_price),150);
    ui->tableView_complex_services->setColumnWidth(static_cast<int>(Complex::complex_services::name_price),350);
    ui->tableView_complex_services->setColumnHidden(static_cast<int>(Complex::complex_services::idservis),true);
    ui->tableView_complex_services->setColumnHidden(static_cast<int>(Complex::complex_services::idcomplex),true);
    ui->tableView_complex_services->setColumnHidden(static_cast<int>(Complex::complex_services::ideconom),true);
    ui->tableView_complex_services->setColumnHidden(static_cast<int>(Complex::complex_services::idprice),true);
    ui->tableView_complex_services->resizeRowsToContents();
    ui->tableView_complex_services->horizontalHeader()->setStretchLastSection(true);
}

void complex::AddNewComplex()
{
    complex_add_new *d = new complex_add_new(this);
    if(d->exec()){
        SetModelComplex();
        QModelIndexList list =  model_complex->match(model_complex->index(0,static_cast<int>(Complex::complex::idcomplex)),Qt::DisplayRole,QVariant(d->GetIdComplex()),1,Qt::MatchFixedString);
        ui->tableView->selectRow(list.at(0).row());
        ui->tableView->setFocus();
    }
    delete d;
}

void complex::DeleteComplex()
{
    QModelIndex index = ui->tableView->currentIndex();
    int idcomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(Complex::complex::idcomplex))).toInt();
    QString namecomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(Complex::complex::namecomplex))).toString();
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: black'>Удалить комплекс :<br><span style='color: blue'>" + namecomplex +"</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    QSqlQuery query;
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {

        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return;
        }
        else{
            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            query.exec();
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
            query.prepare("delete from complex where idcomplex = ?");
            query.addBindValue(idcomplex);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                db.rollback();
                return;
            }
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
               return;
            }
            QMessageBox *mbox = new QMessageBox(this);
            mbox->setWindowTitle(tr("Внимание"));
            mbox->setText("Данные успешно удалены.");
            mbox->setModal(true);
            mbox->setStandardButtons(QMessageBox::Ok);
            mbox->setIcon(QMessageBox::Information);
            QTimer::singleShot(1500,mbox,SLOT(accept()));
            mbox->exec();
            int row = ui->tableView->currentIndex().row();
            if(row  == model_complex->rowCount()-1 && model_complex->rowCount() > 1){
                row--;
            }
            SetModelComplex();
            ui->tableView->selectRow(row);
            ui->tableView->setFocus();
            return;
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return ;
    }
}

void complex::DeleteService()
{
    QModelIndex index;
    QModelIndexList list = ui->tableView_complex_services->selectionModel()->selectedRows();
    int idservices;
    int row;
    QString name_usl = "";
    for(int i = 0; i < list.length(); i++){
        index = list.at(i);
        name_usl += "<br>" + model_complex_services->data(model_complex_services->index(index.row(),static_cast<int>(Complex::complex_services::name_usl))).toString();
    }
    if(list.length() == 1){
        int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                             "<p style='text-align: center; font-size: 12pt; color: black'>Удалить услугу :<span style='color: blue'>" + name_usl +"</p>",
                                     QMessageBox::Yes|QMessageBox::No,
                                     QMessageBox::No);
        if (n != QMessageBox::Yes){
            return;
        }
    }
    else{
        int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                             "<p style='text-align: center; font-size: 12pt; color: black'>Удалить выделенные услуги:<span style='color: blue'>" + name_usl +"</p>",
                                     QMessageBox::Yes|QMessageBox::No,
                                     QMessageBox::No);
        if (n != QMessageBox::Yes){
            return;
        }
    }
    QSqlQuery query;
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {

        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return;
        }
        else{
            for(int i = 0; i < list.length(); i++){
                index = list.at(i);
                query.prepare("select set_config('auth.username', ?, false)");
                query.addBindValue(UserName);
                query.exec();
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                    db.rollback();
                    return;
                }
                idservices = model_complex_services->data(model_complex_services->index(index.row(),static_cast<int>(Complex::complex_services::idservis))).toInt();
                query.prepare("delete from complex_services where idservices = ?");
                query.addBindValue(idservices);
                if(!query.exec()){
                    QMessageBox::critical(this,"Ошибка",
                                          "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                    db.rollback();
                    return;
                }
            }
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
               return;
            }
            SetModelComplexServices();
            row = list.at(0).row();
            if( row != 0){
                row --;
            }
            ui->tableView_complex_services->selectRow(row);
            ui->tableView_complex_services->setFocus();
            return;
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return ;
    }
}

void complex::AddService()
{
    int idcomplex = model_complex->data(model_complex->index(ui->tableView->currentIndex().row(),static_cast<int>(Complex::complex::idcomplex))).toInt();
    complex_add_new_service *d = new complex_add_new_service(this,idcomplex);
    connect(d,&complex_add_new_service::addServiceInShablon,this,&complex::SignalAddService);
    d->exec();
    disconnect(d,&complex_add_new_service::addServiceInShablon,this,&complex::SignalAddService);
    delete d;
}

void complex::SignalAddService()
{
    SetModelComplexServices();
}

void complex::CopyComplex()
{
    QModelIndex index = ui->tableView->currentIndex();
    int idcomplex_new;
    QString idcomplex =  model_complex->data(model_complex->index(index.row(),static_cast<int>(Complex::complex::idcomplex),QModelIndex())).toString();
    QString namecomplex =  model_complex->data(model_complex->index(index.row(),static_cast<int>(Complex::complex::namecomplex),QModelIndex())).toString();
    QString codecomplexqms = model_complex->data(model_complex->index(index.row(),static_cast<int>(Complex::complex::codecomplexqms),QModelIndex())).toString();
    QString newnamecomplex = "";
    if(!namecomplex.contains(" - Копия ")){
        newnamecomplex = namecomplex + " - Копия 1";
    }
    else{
        newnamecomplex = namecomplex;
    }
    QStringList list = newnamecomplex.split(" ");

    int numbercopy = list.at(list.length()-1).toInt();

    QSqlQuery query,querycomplex;;
    do{
        query.prepare("SELECT * FROM complex  WHERE namecomplex = ?");
        query.addBindValue(newnamecomplex);
        if(!query.exec())
        {
            QMessageBox::critical(this,"Ошибка",
                                  "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            return;
        };
        if(query.first())
        {
            newnamecomplex =  newnamecomplex.left(newnamecomplex.length()-QString::number(numbercopy).length());
            numbercopy++;
            newnamecomplex += QString::number(numbercopy);
        }
        else{
            break;
        }
    }while(true);

    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {

        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return;
        }
        else{

            querycomplex.prepare("select * from complex_services where idcomplex = ?");
            querycomplex.addBindValue(idcomplex);
            if(!querycomplex.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + querycomplex.lastError().text() +"</font></p>");

                db.rollback();
                return;
            }
            if(!querycomplex.first()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center> Нет данных для копирования.</p>");

                db.rollback();
                return;
            }

            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            query.exec();
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
            query.prepare("insert into complex(namecomplex,codecomplexqms) values(?,?) returning idcomplex");
            query.addBindValue(newnamecomplex);
            query.addBindValue(codecomplexqms);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                db.rollback();
                return;
            }
            query.first();
            idcomplex_new = query.value("idcomplex").toInt();
            do{
                    query.prepare("insert into complex_services(idcomplex,ideconom,idprice) values(?,?,?)");
                    query.addBindValue(idcomplex_new);
                    query.addBindValue(querycomplex.value("ideconom").toInt());
                    query.addBindValue(querycomplex.value("idprice").toInt());
                    if(!query.exec()){
                        QMessageBox::critical(this,"Ошибка",
                                              "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                        db.rollback();
                        return;
                    }
            }while(querycomplex.next());
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
               return;
            }
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return ;
    }
    SetModelComplex();
    QModelIndexList listindex =  model_complex->match(model_complex->index(0,static_cast<int>(Complex::complex::idcomplex)),Qt::DisplayRole,QVariant(QString::number(idcomplex_new)),1,Qt::MatchFixedString);
    ui->tableView->selectRow(listindex.at(0).row());
    ui->tableView->setFocus();
}

void complex::DoubleClickedComplex(QModelIndex index)
{
    iDcomplex =  model_complex->data(model_complex->index(index.row(),static_cast<int>(Complex::complex::idcomplex),QModelIndex())).toInt();
    accept();
}


