#include "global.h"
#include "recipients.h"
#include "ui_recipients.h"

recipients::recipients(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::recipients)
{
    ui->setupUi(this);
    if(settings->value("recipients").isValid()){
        setGeometry(settings->value("recipients").toRect());
    }
    model_recipients = new QSqlQueryModel;

   ui->email->setStyleSheet("QLineEdit { background: rgb(231, 237, 252); }");

   ui->tableView->setModel(model_recipients);

   connect(ui->tableView,&QTableView::doubleClicked,this,&recipients::TableViewDoubleClicked);
   connect(ui->pushButton_del,&QPushButton::clicked,this,&recipients::DelPushButton);
   connect(ui->pushButton_add,&QPushButton::clicked,this,&recipients::AddPushButton);
   connect(ui->pushButton_save,&QPushButton::clicked,this,&recipients::SavePushButton);

   connect(ui->email,&QLineEdit::editingFinished,this,&recipients::CheckEMail);

   this->setWindowTitle("Получатели сообщений");
}

recipients::~recipients()
{
    settings->setValue("recipients", geometry());
    delete model_recipients;
    delete ui;
}

bool recipients::SetSqlQuery()
{
    QSqlQuery query;
    query.prepare("select * from recipients order by recipients");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    model_recipients->setQuery(query);
    return true;

}

bool recipients::SetSqlQueryModel()
{
    if(!SetSqlQuery()){
        return false;
    }
    model_recipients->setHeaderData(static_cast<int>(recipients::recipient::idrecipients),Qt::Horizontal,"id");
    model_recipients->setHeaderData(static_cast<int>(recipients::recipient::name_recipients),Qt::Horizontal,"Почта (e-mail)");

    ui->tableView->setColumnHidden(static_cast<int>(recipients::recipient::idrecipients),true);

    ui->tableView->setColumnWidth(static_cast<int>(recipients::recipient::name_recipients),250);

    ui->tableView->horizontalHeader()->setSectionResizeMode(static_cast<int>(recipients::recipient::name_recipients),QHeaderView::Stretch);

    ui->tableView->selectRow(0);
    return true;

}

void recipients::CheckEMail()
{
    if(ui->email->text().trimmed().isEmpty()){
        return;
    }
    QString strEmailAddr(ui->email->text().trimmed());
    QRegExp RX("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");
    if(!strEmailAddr.contains(RX)){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: red'> Неверный формат e-mail адреса!</p>");
        ui->email->setFocus();
    }
}

void recipients::TableViewDoubleClicked(QModelIndex index)
{
    ui->email->setText(model_recipients->data(model_recipients->index(index.row(),static_cast<int>(recipients::recipient::name_recipients))).toString());
}

void recipients::DelPushButton()
{
    QModelIndex index = ui->tableView->currentIndex();
    if(!index.isValid()){
        return;
    }
    int idrecipient = model_recipients->data(model_recipients->index(index.row(),static_cast<int>(recipients::recipient::idrecipients))).toInt();
    QString name_recipient = model_recipients->data(model_recipients->index(index.row(),static_cast<int>(recipients::recipient::name_recipients))).toString();
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: black'>Удалить e-mail :<br><span style='color: blue'>" + name_recipient +"</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    int row = index.row();
    QSqlQuery query;
    query.prepare("delete from recipients where idrecipients = ?");
    query.addBindValue(idrecipient);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

        return;
    }
    SetSqlQuery();
    if(row > 1){
        row = row-1;
    }
    else{
        row = 0;
    }
    ui->tableView->selectRow(row);
    ui->tableView->setFocus();
}

void recipients::AddPushButton()
{
    if(ui->email->text().isEmpty()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Заполните почтовый адрес!</p>");
        return;
    }
    QSqlQuery query;
    query.prepare("insert into recipients (recipients)"
                  "values(?) returning idrecipients");
    query.addBindValue(ui->email->text().trimmed());
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return ;
    }
    query.first();
    QString idrecipients = query.value("idrecipients").toString();
    SetSqlQuery()    ;
    QModelIndexList list = model_recipients->match(model_recipients->index(0,0),Qt::DisplayRole,QVariant(idrecipients));
    if(!list.isEmpty()){
        ui->tableView->selectRow(list.at(0).row());
        ui->tableView->setFocus();
    }
    ui->email->clear();
}

void recipients::SavePushButton()
{
    QModelIndex index = ui->tableView->currentIndex();
    if(!index.isValid()){
        return;
    }
    if(ui->email->text().isEmpty()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Заполните обязательные поля!</p>");
        return;
    }
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 14pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: blue'>Изменить текущую запись?</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    int idrecipient = model_recipients->data(model_recipients->index(index.row(),static_cast<int>(recipients::recipient::idrecipients))).toInt();
    int row = index.row();
    QSqlQuery query;
    query.prepare("update recipients set recipients = ? where idrecipients = ?");
    query.addBindValue(ui->email->text().trimmed());
    query.addBindValue(idrecipient);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return ;
    }
    SetSqlQuery();
    ui->tableView->selectRow(row);
    ui->tableView->setFocus();

    ui->email->clear();
}
