#include "auth.h"
#include "ui_auth.h"
#include "global.h"
#include "enums.h"
#include <QDebug>

QStringList LDAP_connect();
bool LDAPAuth(QString login, QString pass, QString *cn);

auth::auth(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::auth)
{
    ui->setupUi(this);


    connect(ui->radioButton_domain,SIGNAL(clicked(bool)),this,SLOT(AuthDomain()));
    connect(ui->radioButton_login,SIGNAL(clicked(bool)),this,SLOT(AuthLogin()));
    connect(ui->lineEdit_login,SIGNAL(textChanged(QString)),this,SLOT(LineEditEmpty()));
    connect(ui->lineEdit_pass,SIGNAL(textChanged(QString)),this,SLOT(LineEditEmpty()));
    count_auth = 0;
    AuthDomain();

    QMessageBox msgAbout;
    msgAbout.setInformativeText("<span style='text-align: center'><p style='font-size: 30pt; color: red'><b><i>Supposedly</i> set in 30pt font</p>"
                                "<p style='font-size: 20pt; color: blue'><i>Rumoredly</i> set in 20pt font</p>"
                                "<p style='font-size: 10pt; color: green'><i>Putatively</i> set in 10pt font</p>"
                                "<p>Proud not to specify font-size attributes!</span><p>");
//    msgAbout.setStandardButtons(QMessageBox::Ok);
//    msgAbout.setDefaultButton(QMessageBox::Ok);
//    msgAbout.exec();

}

auth::~auth()
{
    delete ui;
}

QString auth::GetUserName()
{
    QStringList envlist,list;
    envlist = QProcess::systemEnvironment();
    QString username,domain,str;
    list = envlist.filter("USERNAME");
    if(!list.isEmpty()){
        str = envlist.filter("USERNAME").at(0);
        username = str.remove("USERNAME=");
    }
    else{
       list = envlist.filter("LOGNAME");
       if(!list.isEmpty()){
           str = envlist.filter("LOGNAME").at(0);
           username = str.remove("LOGNAME=");
       }
       else{
           username = "";
       }
    }    return username;
}

void auth::AuthDomain()
{
    if(os != OS_WINDOWS){
        ui->lineEdit_login->setEnabled(true);
        ui->lineEdit_login->setText(GetUserName());
        ui->lineEdit_pass->setEnabled(true);
        ui->pushButton->setEnabled(false);
        ui->lineEdit_pass->setFocus();
    }
    else {
        ui->lineEdit_login->clear();
        ui->lineEdit_pass->clear();
        ui->lineEdit_login->setEnabled(false);
        ui->lineEdit_pass->setEnabled(false);
        ui->pushButton->setEnabled(true);
    }
}

void auth::AuthLogin()
{
    ui->lineEdit_login->setEnabled(true);
    ui->lineEdit_login->setText(GetUserName());
    ui->lineEdit_pass->setEnabled(true);
    ui->pushButton->setEnabled(false);
}

void auth::LineEditEmpty()
{
    if(!ui->lineEdit_login->text().isEmpty() && !ui->lineEdit_pass->text().isEmpty())
        ui->pushButton->setEnabled(true);
    else
        ui->pushButton->setEnabled(false);
}

void auth::on_pushButton_clicked()
{
    bool auth_value = false;
    QSqlQuery query;
    QString *cn = new QString();


    if(os == OS_WINDOWS){
        QStringList param = LDAP_connect();
        UserName = param[0];
        displayName = param[1];
        auth_value = param[2] == "true" ? true : false ;
    }
    else
    {
        if(!LDAPAuth(ui->lineEdit_login->text().trimmed()+"@VIVEYA.LOCAL",ui->lineEdit_pass->text().trimmed(),cn))
        {
            QMessageBox *mbox = new QMessageBox(this);
            mbox->setWindowTitle(tr("Внимание"));
            mbox->setText("<p style='font-size: 12pt; color: blue' align='center'>Ошибка входа в систему!<br>Убедитесь в корректности<br>введенных данных.</p>");
            mbox->setStandardButtons(QMessageBox::Ok);
            mbox->setIcon(QMessageBox::Information);
            QTimer::singleShot(1500,mbox,SLOT(accept()));
            mbox->exec();

            count_auth++;
            if(count_auth >= 3)
                return reject();
            return;
        }
        else
        {
            UserName = ui->lineEdit_login->text();
            displayName = UserName;
            auth_value = true;
        }
    }
    query.prepare("select kod_access, fio from users where name_domen = ? and active");
    query.addBindValue(UserName);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");
        return reject();
    }
    if(!query.first())
    {
        QMessageBox *mbox = new QMessageBox(this);
        mbox->setWindowTitle(tr("Внимание"));
        mbox->setText("<p style='font-size: 12pt; color: black' align='center'>У пользователя <font color='red'>" +  UserName +
                      "</font><br>отсутствуют роли в системе!</p>");
        mbox->setStandardButtons(QMessageBox::Ok);
        mbox->setIcon(QMessageBox::Information);
        QTimer::singleShot(3000,mbox,SLOT(accept()));
        mbox->exec();
        return reject();
    }
    UserRole = query.value("kod_access").toInt();
    if(!query.value("fio").toString().isEmpty())
    {
        displayName = query.value("fio").toString();
    }

    if(auth_value){
        return accept();
    }
    return reject();
}
