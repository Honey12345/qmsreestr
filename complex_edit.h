#ifndef COMPLEX_EDIT_H
#define COMPLEX_EDIT_H

#include <QDialog>
#include <QSqlQuery>
#include <QMessageBox>
#include <QSqlError>

namespace Ui {
class complex_edit;
}

class complex_edit : public QDialog
{
    Q_OBJECT

public:
    explicit complex_edit(QWidget *parent = nullptr);
    ~complex_edit();

    bool SetComplexData(int idcomplex);

private slots:

    void ChangeDataComplex();
    void SaveData();

private:
    Ui::complex_edit *ui;
    int idcomplex;
    QString codecomplex,namecomplex;
    double stoimost;
};

#endif // COMPLEX_EDIT_H
