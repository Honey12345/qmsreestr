#include "complex_add_new.h"
#include "ui_complex_add_new.h"
#include "global.h"



complex_add_new::complex_add_new(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::complex_add_new)
{
    ui->setupUi(this);
    if(settings->value("complex_add_new").isValid()){
        setGeometry(settings->value("complex_add_new").toRect());
    }
    SetStyleGroupBox();
    this->setWindowTitle("Добавление нового комплекса (шаблона)" );
    model_econom = new QSqlQueryModel;
    model_price = new QSqlQueryModel;
    model_complex = new QStandardItemModel;
    model_complex->setColumnCount(5);
    model_complex->sort(static_cast<int>(ComplexAdd::complex::name_usl),Qt::AscendingOrder);
    model_complex->setHeaderData(static_cast<int>(ComplexAdd::complex::name_usl),Qt::Horizontal,"Наименование ПЭО");
    model_complex->setHeaderData(static_cast<int>(ComplexAdd::complex::code_price),Qt::Horizontal,"Код по прайсу");
    model_complex->setHeaderData(static_cast<int>(ComplexAdd::complex::name_price),Qt::Horizontal,"Наименование по прайсу");
    ui->tableView_complex->setModel(model_complex);
    ui->tableView_complex->setColumnWidth(static_cast<int>(ComplexAdd::complex::name_usl),350);
    ui->tableView_complex->setColumnWidth(static_cast<int>(ComplexAdd::complex::code_price),150);
    ui->tableView_complex->setColumnWidth(static_cast<int>(ComplexAdd::complex::name_price),350);
    ui->tableView_complex->setColumnHidden(static_cast<int>(ComplexAdd::complex::id_usl),true);
    ui->tableView_complex->setColumnHidden(static_cast<int>(ComplexAdd::complex::id_price),true);
    ui->tableView_complex->horizontalHeader()->setStretchLastSection(true);

    SetModelEconom();
    SetModelPrice();

    connect(ui->tableView_sprav_econom,&QTableView::doubleClicked,this,&complex_add_new::TableView_econom_DoubleClicked);
    connect(ui->tableView_sprav_price,&QTableView::doubleClicked,this,&complex_add_new::TableView_price_DoubleClicked);
    //connect(ui->tableView_complex,&QTableView::doubleClicked,this,&complex_add_new::TableView_complex_DoubleClicked);
    connect(ui->pushButton_del,&QPushButton::clicked,this,&complex_add_new::TableView_complex_DoubleClicked);
    connect(model_complex,&QStandardItemModel::rowsInserted,this,&complex_add_new::model_complex_RowsInsertedRemoved);
    connect(model_complex,&QStandardItemModel::rowsRemoved,this,&complex_add_new::model_complex_RowsInsertedRemoved);
    connect(model_complex,&QStandardItemModel::itemChanged,this,&complex_add_new::model_complex_RowsInsertedRemoved);
    connect(ui->lineEdit_name_complex,&QLineEdit::editingFinished,this,&complex_add_new::model_complex_RowsInsertedRemoved);
    connect(ui->pushButton,&QPushButton::clicked,this,&complex_add_new::saveComplex);

    ui->pushButton->setEnabled(false);

    connect(ui->tableView_sprav_price->horizontalHeader(),&QHeaderView::sectionDoubleClicked,this,&complex_add_new::HeaderDoubleClicked);


    // создаем объект QLineEdit, его родителем делаем QHeaderView из нашего QTableView, в результате наш объект
    // QLineEdit будет выводиться в РОДИТЕЛЕ ! Что нам и нужно, определяя какая колонка по индексу выбрана и смещая width
    // geometry(width,0,200,25) объекта QlineElbn на ширину колонок, можно выводить в нужных колонках.

    findeKod = new QLineEdit(ui->tableView_sprav_price->horizontalHeader());
    findeKod->setText("");
    findeKod->close();
    connect(findeKod,&QLineEdit::editingFinished,findeKod,&QLineEdit::close);
    connect(findeKod,&QLineEdit::textChanged,this,&complex_add_new::FindeKod);

}

complex_add_new::~complex_add_new()
{
    settings->setValue("complex_add_new", geometry());
    delete model_econom;
    delete model_price;
    delete model_complex;
    delete findeKod;
    delete ui;
}

void complex_add_new::SetStyleGroupBox()
{

    ui->groupBox->setTitle("Комплекс");
    ui->groupBox->setStyleSheet("QGroupBox {"
                                "border: 2px solid red;"
                                "border-radius: 9px;"
                                "border-color: red;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: red"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_2->setTitle("Услуги ПЭО");
    ui->groupBox_2->setStyleSheet("QGroupBox {"
                                "border: 2px solid blue;"
                                "border-radius: 9px;"
                                "border-color: blue;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: blue"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_3->setTitle("Услуги по прайсу");
    ui->groupBox_3->setStyleSheet("QGroupBox {"
                                "border: 2px solid green;"
                                "border-radius: 9px;"
                                "border-color: green;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: green"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

}

void complex_add_new::SetModelPrice()
{
    QSqlQuery query;
    query.prepare("select * from qmspriceusl where closedate isnull order by code_price");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    model_price->setQuery(query);
    model_price->setHeaderData(static_cast<int>(ComplexAdd::price::code_price),Qt::Horizontal,"Код прайса");
    model_price->setHeaderData(static_cast<int>(ComplexAdd::price::name_price),Qt::Horizontal,"Наименование");
    ui->tableView_sprav_price->setModel(model_price);
    ui->tableView_sprav_price->setColumnWidth(static_cast<int>(ComplexAdd::price::code_price),150);
    ui->tableView_sprav_price->horizontalHeader()->setStretchLastSection(true);
    ui->tableView_sprav_price->setColumnHidden(static_cast<int>(ComplexAdd::price::id),true);
    ui->tableView_sprav_price->setColumnHidden(static_cast<int>(ComplexAdd::price::closedata),true);
}

void complex_add_new::SetModelEconom()
{
    QSqlQuery query;
    query.prepare("select * from econom_usl order by name_usl");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    model_econom->setQuery(query);
    model_econom->setHeaderData(static_cast<int>(ComplexAdd::econom::name_usl),Qt::Horizontal," Наименование услуги ");
    ui->tableView_sprav_econom->setModel(model_econom);
    ui->tableView_sprav_econom->setColumnHidden(static_cast<int>(ComplexAdd::econom::id),true);
    ui->tableView_sprav_econom->horizontalHeader()->setStretchLastSection(true);
    return;
}

int complex_add_new::GetIdComplex()
{
    return idComplex;
}

void complex_add_new::TableView_price_DoubleClicked(QModelIndex index)
{
    QModelIndex index_complex = ui->tableView_complex->currentIndex();
    QString id_price = model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAdd::price::id))).toString();
    QList<QStandardItem*> list;
    if(index_complex.row() == -1 || (model_complex->data(model_complex->index(index_complex.row(),static_cast<int>(ComplexAdd::complex::id_price))).toInt() != 0 && index_complex.row() != -1)){
        list.clear();
        list.append(new QStandardItem(QString()));
        list.append(new QStandardItem(model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAdd::price::code_price))).toString()));
        list.append(new QStandardItem(model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAdd::price::name_price))).toString()));
        list.append(new QStandardItem(QString()));
        list.append(new QStandardItem(model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAdd::price::id))).toString()));
        model_complex->appendRow(list);
        ui->tableView_complex->setFocus();
    }
    else {
        model_complex->setData(model_complex->index(index_complex.row(),static_cast<int>(ComplexAdd::complex::code_price)),model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAdd::price::code_price))).toString());
        model_complex->setData(model_complex->index(index_complex.row(),static_cast<int>(ComplexAdd::complex::name_price)),model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAdd::price::name_price))).toString());
        model_complex->setData(model_complex->index(index_complex.row(),static_cast<int>(ComplexAdd::complex::id_price)),model_price->data(model_price->index(index.row(),static_cast<int>(ComplexAdd::price::id))).toString());
    }
    ui->tableView_sprav_price->setRowHidden(index.row(),true);
    QModelIndexList indexlist = model_complex->match(model_complex->index(0,static_cast<int>(ComplexAdd::complex::id_price)),Qt::DisplayRole,QVariant(id_price),1,Qt::MatchFixedString);
    if(!indexlist.isEmpty()){
        int row = indexlist.at(0).row();
        while(model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_price))).toInt() != 0){
            ui->tableView_complex->selectRow(row);
            row++;
        }
    }
    ui->tableView_sprav_price->setFocus();
}

void complex_add_new::TableView_econom_DoubleClicked(QModelIndex index)
{
    QModelIndex index_complex = ui->tableView_complex->currentIndex();
    QString id_usl = model_econom->data(model_econom->index(index.row(),static_cast<int>(ComplexAdd::econom::id))).toString();
    QList<QStandardItem*> list;
    if(index_complex.row() == -1 || (model_complex->data(model_complex->index(index_complex.row(),static_cast<int>(ComplexAdd::complex::id_usl))).toInt() != 0 && index_complex.row() != -1)){
        list.clear();
        list.append(new QStandardItem(model_econom->data(model_econom->index(index.row(),static_cast<int>(ComplexAdd::econom::name_usl))).toString()));
        list.append(new QStandardItem(QString()));
        list.append(new QStandardItem(QString()));
        list.append(new QStandardItem(model_econom->data(model_econom->index(index.row(),static_cast<int>(ComplexAdd::econom::id))).toString()));
        list.append(new QStandardItem(QString()));
        model_complex->appendRow(list);
        ui->tableView_complex->setFocus();

    }
    else{
        model_complex->setData(model_complex->index(index_complex.row(),static_cast<int>(ComplexAdd::complex::name_usl)),model_econom->data(model_econom->index(index.row(),static_cast<int>(ComplexAdd::econom::name_usl))).toString());
        model_complex->setData(model_complex->index(index_complex.row(),static_cast<int>(ComplexAdd::complex::id_usl)),model_econom->data(model_econom->index(index.row(),static_cast<int>(ComplexAdd::econom::id))).toString());
    }

    ui->tableView_sprav_econom->setRowHidden(index.row(),true);
    QModelIndexList indexlist = model_complex->match(model_complex->index(0,static_cast<int>(ComplexAdd::complex::id_usl)),Qt::DisplayRole,QVariant(id_usl),1,Qt::MatchFixedString);
    if(!indexlist.isEmpty()){
        int row = indexlist.at(0).row();
        while(model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_usl))).toInt() != 0){
            ui->tableView_complex->selectRow(row);
            row++;
        }
    }
    ui->tableView_sprav_econom->setFocus();
}

void complex_add_new::TableView_complex_DoubleClicked()
{
    QModelIndex index = ui->tableView_complex->currentIndex();
    QString ideconom = model_complex->data(model_complex->index(index.row(),static_cast<int>(ComplexAdd::complex::id_usl))).toString();
    QString idprice = model_complex->data(model_complex->index(index.row(),static_cast<int>(ComplexAdd::complex::id_price))).toString();
    QModelIndexList list;
    if(!ideconom.isEmpty()){
        list = model_econom->match(model_econom->index(0,static_cast<int>(ComplexAdd::econom::id)),Qt::DisplayRole,QVariant(ideconom),1,Qt::MatchFixedString);
        if(!list.isEmpty()){
            ui->tableView_sprav_econom->setRowHidden(list.at(0).row(),false);
        }
    }
    if(!idprice.isEmpty()){
        list = model_price->match(model_price->index(0,static_cast<int>(ComplexAdd::price::id)),Qt::DisplayRole,QVariant(idprice),1,Qt::MatchFixedString);
        if(!list.isEmpty()){
            ui->tableView_sprav_price->setRowHidden(list.at(0).row(),false);
        }
    }
    model_complex->removeRow(index.row());
    index = ui->tableView_complex->currentIndex();
    ui->tableView_complex->selectRow(index.row());
}

void complex_add_new::model_complex_RowsInsertedRemoved()
{
    for(int row = 0; row < model_complex->rowCount(); row++){
        if(model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_usl))).toInt() != 0 &&
                model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_price))).toInt() != 0){
            if(!ui->lineEdit_name_complex->text().trimmed().isEmpty()){
                ui->pushButton->setEnabled(true);
                return;
            }
        }
    }
    ui->pushButton->setEnabled(false);
}

void complex_add_new::saveComplex()
{
    if(ui->lineEdit_codecomplexqms->text().trimmed().isEmpty()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: red'>Заполните код комплекса из МИС qMS.</p>");
        return;
    }
    QSqlQuery query;
    query.prepare("select * from complex where namecomplex = ?");
    query.addBindValue(ui->lineEdit_name_complex->text().trimmed());
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    if(query.first()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: red'>Такое наименование комплекса существует.</p>");
        return;
    }
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: blue'> Записать шаблон ? <br>Пустые строки не будут записаны!</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {

        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<p align=center>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return;
        }
        else{
            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            query.exec();
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return;
            }
            query.prepare("insert into complex(namecomplex,codecomplexqms) values(?,?) returning idcomplex");
            query.addBindValue(ui->lineEdit_name_complex->text().trimmed());
            query.addBindValue(ui->lineEdit_codecomplexqms->text().trimmed());
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                db.rollback();
                return;
            }
            query.first();
            int idcomplex = query.value("idcomplex").toInt();
            for(int row = 0; row < model_complex->rowCount(); row++){
                if(model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_usl))).toInt() != 0 &&
                        model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_price))).toInt() != 0){
                    query.prepare("insert into complex_services(idcomplex,ideconom,idprice) values(?,?,?)");
                    query.addBindValue(idcomplex);
                    query.addBindValue(model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_usl))).toInt());
                    query.addBindValue(model_complex->data(model_complex->index(row,static_cast<int>(ComplexAdd::complex::id_price))).toInt());
                    if(!query.exec()){
                        QMessageBox::critical(this,"Ошибка",
                                              "<p align=center>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                        db.rollback();
                        return;
                    }
                }
            }
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p align=center>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
               return;
            }
            idComplex = idcomplex;
            QMessageBox *mbox = new QMessageBox(this);
            mbox->setWindowTitle(tr("Внимание"));
            mbox->setText("Данные успешно сохранены.");
            mbox->setModal(true);
            mbox->setStandardButtons(QMessageBox::Ok);
            mbox->setIcon(QMessageBox::Information);
            QTimer::singleShot(1500,mbox,SLOT(accept()));
            mbox->exec();
            return accept();
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p align=center>Транзакции не поддерживаются в используемой СУБД!</p>");
        return ;
    }
}

void complex_add_new::FindeKod(QString str)
{
    if(str.length() < 2){
        return;
    }
    str+="*";
    QModelIndexList IndexList4 = model_price->match(model_price->index(0,static_cast<int>(ComplexAdd::price::code_price)),Qt::DisplayRole,str,1,Qt::MatchWildcard);
    QModelIndex index;
    if(IndexList4.length() == 0){
        return;
    }
    index = IndexList4.at(0);
    ui->tableView_sprav_price->selectRow(index.row());
}

void complex_add_new::HeaderDoubleClicked(int index)
{
    if(index != 1){
        return;
    }
    ui->tableView_sprav_price->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableView_sprav_price->selectColumn(1);
    int widthColumn = ui->tableView_sprav_price->columnWidth(index);
    QRect lineRect (0,0,widthColumn,25);
    findeKod->setGeometry(lineRect);
    findeKod->show();
    findeKod->setFocus();
    ui->tableView_sprav_price->setSelectionBehavior(QAbstractItemView::SelectRows);
}

