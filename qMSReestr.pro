#-------------------------------------------------
#
# Project created by QtCreator 2020-10-28T15:43:32
#
#-------------------------------------------------

QT       += core gui network sql gui-private

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(qtxlsx/xlsx/qtxlsx.pri)

TARGET = qMSReestr
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#Библиотека для работы с AD для Windows раскоментарит для Linux закоментарить
#LIBS += -lWldap32
# Для Linux раскоментарить для Windows закоментарить
#INCLUDEPATH += /usr/local/include
#LIBS += -L/usr/local/lib -lldapcpp
win32: {
    LIBS += -lWldap32
    SOURCES += ldap.cpp
    RC_ICONS += peo.ico
    DEFINES += WIN32_MODE
}
unix: !macx {
    INCLUDEPATH += /usr/local/include
    SOURCES += ldapauth.cpp
    LIBS += -L/usr/local/lib -lldapcpp
    DEFINES += LINUX_MODE
}

CONFIG += c++11

SOURCES += \
        auth.cpp \
        complex.cpp \
        complex_add_new.cpp \
        complex_add_new_service.cpp \
        complex_edit.cpp \
        dogovor_add.cpp \
        dogovor_set_code.cpp \
        global.cpp \
        inpudatedialog.cpp \
        loadprice.cpp \
        main.cpp \
        mainwindow.cpp \
        mainwindowovdpo.cpp \
        mainwindowparent.cpp \
        ovdpo/ovdpo_complex_edit.cpp \
        ovdpo/ovdpo_kontr_add_edit.cpp \
        price_normalization.cpp \
        recipients.cpp \
        selectinvoice.cpp \
        sesquery.cpp \
        set_smtp_server.cpp \
        smtpdialog.cpp \
        splashwindows.cpp \
        spraveconomuslugi.cpp \
        spravochnik_invoice.cpp \
        spravqmsprice.cpp \
        treeview.cpp \
        users.cpp

HEADERS += \
        auth.h \
        complex.h \
        complex_add_new.h \
        complex_add_new_service.h \
        complex_edit.h \
        custommodel.h \
        customtableview.h \
        dogovor_add.h \
        dogovor_set_code.h \
        enums.h \
        global.h \
        inpudatedialog.h \
        loadprice.h \
        mainwindow.h \
        mainwindowovdpo.h \
        mainwindowparent.h \
        ovdpo/ovdpo_complex_edit.h \
        ovdpo/ovdpo_kontr_add_edit.h \
        price_normalization.h \
        recipients.h \
        selectinvoice.h \
        sesquery.h \
        set_smtp_server.h \
        smtpdialog.h \
        splashwindows.h \
        spraveconomuslugi.h \
        spravochnik_invoice.h \
        spravqmsprice.h \
        treeview.h \
        users.h

FORMS += \
        auth.ui \
        complex.ui \
        complex_add_new.ui \
        complex_add_new_service.ui \
        complex_edit.ui \
        dogovor_add.ui \
        dogovor_set_code.ui \
        inpudatedialog.ui \
        loadprice.ui \
        mainwindow.ui \
        mainwindowovdpo.ui \
        mainwindowparent.ui \
        ovdpo/ovdpo_complex_edit.ui \
        ovdpo/ovdpo_kontr_add_edit.ui \
        price_normalization.ui \
        recipients.ui \
        selectinvoice.ui \
        set_smtp_server.ui \
        splashwindows.ui \
        spraveconomuslugi.ui \
        spravochnik_invoice.ui \
        spravqmsprice.ui \
        users.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc

DISTFILES +=
