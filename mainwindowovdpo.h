#ifndef MAINWINDOWOVDPO_H
#define MAINWINDOWOVDPO_H

#include <QMainWindow>
#include <QFile>
#include <QFileDialog>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QDateTime>
#include <QDesktopServices>
#include <QTextCodec>
#include <QByteArray>
#include <QStandardItemModel>
#include <QMenu>
#include <QAction>
#include <QTableView>
#include <QSqlError>
#include <QMap>
#include <QProgressDialog>
#include <QTimer>


class OVDPO :   public QObject  {
public:

    enum class ovdpo_kontragent :int {idkontragent,innkontragent,namekontragent,finishedkontragent};
    Q_ENUM(ovdpo_kontragent)

    enum class ovdpo_complex    :int{idcomplex,idkontragent,namecomplex,kolvocomplex};
    Q_ENUM(ovdpo_complex)

    enum class ovdpo_service    :int{idservice,idcomplex,codeservice,nameservice,stoimostservice};
    Q_ENUM(ovdpo_service)

    Q_OBJECT
    OVDPO() = delete;
};

class my_Kontragent_QSqlQueryModel   :   public  QSqlQueryModel
{
    Q_OBJECT
public:
    explicit my_Kontragent_QSqlQueryModel()
    {}
    ~my_Kontragent_QSqlQueryModel()
    {}
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const
    {
        QVariant value = QSqlQueryModel::data(index, role);
//        QVariant data=QSqlQueryModel::data(index);
        switch (role)
        {
        case Qt::DisplayRole:
            if(index.column() == static_cast<int>(OVDPO::ovdpo_kontragent::innkontragent)){
                    return QString::number(value.toDouble(),'f',0);
            }
            break;

        case Qt::TextAlignmentRole:
            if(index.column() == static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent) ){
                return int(Qt::AlignLeft| Qt::AlignVCenter);
            }
            else{
                return int(Qt::AlignHCenter| Qt::AlignVCenter);
            }
            break;
        case Qt::TextColorRole:
            if(!QSqlQueryModel::data(QSqlQueryModel::index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::finishedkontragent))).isNull())
            {
                return QBrush(Qt::gray,Qt::SolidPattern);
            }
            break;
        default:
            break;

        }
        return QSqlQueryModel::data(index, role);

    }
};

class MyTableView_ovdpo_kontragent : public QTableView
{
    Q_OBJECT
public:
    explicit MyTableView_ovdpo_kontragent(QWidget *)
    {
        setMouseTracking(true);
    }

    void currentChanged(const QModelIndex &current, const QModelIndex &previous)
    {
        emit changeData_ovdpo_kontragent(current);
        return QTableView::currentChanged(current, previous);
    }

signals:
    void changeData_ovdpo_kontragent(QModelIndex);

public slots:
};

class MyTableView_ovdpo_complex : public QTableView
{
    Q_OBJECT
public:
    explicit MyTableView_ovdpo_complex(QWidget *)
    {
        setMouseTracking(true);
    }

    void currentChanged(const QModelIndex &current, const QModelIndex &previous)
    {
        emit changeData_ovdpo_complex(current);
        return QTableView::currentChanged(current, previous);
    }

signals:
    void changeData_ovdpo_complex(QModelIndex);

public slots:
};


namespace Ui {
class MainWindowOVDPO;
}

class MainWindowOVDPO : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowOVDPO(QWidget *parent = nullptr);
    ~MainWindowOVDPO();

    bool SetData();
    void SetStyleGroupBox();
    bool SetModelKontragent();
    bool SetModelComplex();
    bool SetModelService();

    bool SaveComplexToDB(QString name_complex, QStringList list_service);

protected:

    void closeEvent(QCloseEvent *);

private slots:

    bool SetQueryKontragent();
    bool SetQueryComplex();
    bool SetQueryService();

    void refreshModelComplex();
    void refreshModelService();

    void AddKontragent();
    void EditKontragent();
    void DelKontragent();

    void AddComplex();
    void EditComplex();
    void DelComplex();

    void Invoice();

    void Exit();

signals:

    void exit();

private:
    Ui::MainWindowOVDPO *ui;
    QSqlQueryModel *model_kontragent, *model_complex, *model_service;
};

#endif // MAINWINDOWOVDPO_H
