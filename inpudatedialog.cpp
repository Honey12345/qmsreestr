#include "inpudatedialog.h"
#include "ui_inpudatedialog.h"
#include "global.h"
#include <QDebug>

InpuDateDialog::InpuDateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InpuDateDialog)
{
    ui->setupUi(this);
    if(settings->value("InputDateDialog").isValid()){
        setGeometry(settings->value("InputDateDialog").toRect());
    }
    connect(ui->pushButton,&QPushButton::clicked,this,&InpuDateDialog::accept);
}

InpuDateDialog::~InpuDateDialog()
{
    settings->setValue("InputDateDialog",geometry());
    delete ui;
}

void InpuDateDialog::SetDataDialog(QString title, QDate data)
{
    this->setWindowTitle(title);
    ui->dateEdit->setDate(data);
}

QDate InpuDateDialog::GetDate()
{
    return ui->dateEdit->date();
}
