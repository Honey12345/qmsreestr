#ifndef GLOBAL_H
#define GLOBAL_H
#include <QString>
#include <QSqlDatabase>
#include <QStringList>
#include <QSettings>
#include <QMessageBox>

#include <QDebug>

enum OperatingSytem {OS_WINDOWS, OS_UNIX, OS_LINUX, OS_MAC};
//class global
//{
//public:
//    global();
    extern QString UserName;
    extern QString displayName;
    extern int UserRole;
    extern QSqlDatabase db;
    extern QSettings *settings;
    extern OperatingSytem os;
    extern QStringList NameKodeAccess;
    extern QString ver;
//};

#endif // GLOBAL_H
