#ifndef SET_SMTP_SERVER_H
#define SET_SMTP_SERVER_H

#include <QDialog>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QMessageBox>
#include <QDialog>
#include <QFile>
#include <QFileDialog>

namespace Ui {
class set_smtp_server;
}

class set_smtp_server : public QDialog
{
    Q_OBJECT

public:

    enum class smtp_server :int {idsmtpserver,servername,serverport,username,password,active};
    Q_ENUM(smtp_server)

    explicit set_smtp_server(QWidget *parent = nullptr);
    ~set_smtp_server();

    bool SetSqlQuery();
    bool SetSqlQueryModel();

private slots:
    void on_pushButton_add_clicked();

//    void on_pushButton_clicked();

    void on_pushButton_del_clicked();

    void TableViewDoubleClicked(QModelIndex);

    void on_pushButton_save_clicked();

private:
    Ui::set_smtp_server *ui;
    QSqlQueryModel *model_smtp_server;
};

#endif // SET_SMTP_SERVER_H
