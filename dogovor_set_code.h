#ifndef DOGOVOR_SET_CODE_H
#define DOGOVOR_SET_CODE_H

#include <QDialog>
#include <QRegExpValidator>
#include <QDebug>

namespace Ui {
class dogovor_set_code;
}

class dogovor_set_code : public QDialog
{
    Q_OBJECT

public:
    explicit dogovor_set_code(QWidget *parent = nullptr);
    ~dogovor_set_code();
    QString GetCode();

private slots:

    void SetCursorPosition();
    void SetCode();

private:
    Ui::dogovor_set_code *ui;
    QString code = "";
};

#endif // DOGOVOR_SET_CODE_H
