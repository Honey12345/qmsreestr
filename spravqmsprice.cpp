#include "spravqmsprice.h"
#include "ui_spravqmsprice.h"
#include "global.h"

spravqmsprice::spravqmsprice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::spravqmsprice)
{
    ui->setupUi(this);
    if(settings->value("spravqmsprice").isValid()){
        setGeometry(settings->value("spravqmsprice").toRect());
    }
    setWindowTitle("Справочник услуг по прайсу из qMS");
    model = new QStandardItemModel;

    // создаем объект QLineEdit, его родителем делаем QHeaderView из нашего QTableView, в результате наш объект
    // QLineEdit будет выводиться в РОДИТЕЛЕ ! Что нам и нужно, определяя какая колонка по индексу выбрана и смещая width
    // geometry(width,0,200,25) объекта QlineElbn на ширину колонок, можно выводить в нужных колонках.

    findeKod = new QLineEdit(ui->tableView->horizontalHeader());
    findeKod->setText("");
    findeKod->close();

    setModel();

    connect(ui->tableView->horizontalHeader(),&QHeaderView::sectionDoubleClicked,this,&spravqmsprice::HeaderViewSectionClicked);

    connect(findeKod,&QLineEdit::editingFinished,findeKod,&QLineEdit::close);
    connect(findeKod,&QLineEdit::textChanged,this,&spravqmsprice::FindKod);
}

spravqmsprice::~spravqmsprice()
{
    settings->setValue("spravqmsprice", geometry());
    delete findeKod;
    delete model;
    delete ui;
}

void spravqmsprice::setModel()
{
    QSqlQuery query;
    query.prepare("select * from qmspriceusl order by code_price");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
    }
    int countColumn = query.record().count();
    QList<QStandardItem*>list;
    model->clear();
    model->setColumnCount(countColumn);
    if(query.first()){
        do{
            for(int col = 0; col < countColumn; col++){
                list.append(new QStandardItem(query.value(col).toString()));
            }
            model->appendRow(list);
            list.clear();
        }while(query.next());

    }
    setHeaderData();
    setView();

}

void spravqmsprice::setHeaderData()
{
    model->setHeaderData(0,Qt::Horizontal,"id");
    model->setHeaderData(1,Qt::Horizontal,"Код прайса");
    model->setHeaderData(2,Qt::Horizontal,"Наименование");
    model->setHeaderData(3,Qt::Horizontal,"Закрыта");
}

void spravqmsprice::setView()
{
    ui->tableView->setModel(model);
    ui->tableView->setColumnWidth(0,50);
    ui->tableView->setColumnWidth(1,150);
    ui->tableView->setColumnWidth(2,800);
    ui->tableView->setColumnWidth(3,100);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
    ui->tableView->setColumnHidden(0,true);

    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void spravqmsprice::FindKod(QString str)
{
    if(str.length() < 2){
        return;
    }
    str+="*";
    QModelIndexList IndexList4 = model->match(model->index(0,1),Qt::DisplayRole,str,1,Qt::MatchWildcard);
    QModelIndex index;
    if(IndexList4.length() == 0){
        return;
    }
    index = IndexList4.at(0);
    ui->tableView->selectRow(index.row());
}

void spravqmsprice::HeaderViewSectionClicked(int index)
{
    if(index != 1){
        return;
    }
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tableView->selectColumn(1);
    int widthColumn = ui->tableView->columnWidth(index);
    QRect lineRect (0,0,widthColumn,25);
    findeKod->setGeometry(lineRect);
    findeKod->show();
    findeKod->setFocus();
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}
