#include "mainwindowovdpo.h"
#include "ui_mainwindowovdpo.h"
#include "global.h"
#include "enums.h"
#include "ovdpo/ovdpo_kontr_add_edit.h"
#include "ovdpo/ovdpo_complex_edit.h"
#include <qtxlsx/xlsx/xlsxdocument.h>

MainWindowOVDPO::MainWindowOVDPO(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowOVDPO)
{
    ui->setupUi(this);

    if(settings->value("MainWindowOVDPO").isValid()){
        setGeometry(settings->value("MainWindowOVDPO").toRect());
    }
    this->setWindowTitle("Пользователь : " + displayName + " | База данных : " + db.databaseName());

    model_kontragent = new my_Kontragent_QSqlQueryModel;
    model_complex = new QSqlQueryModel;
    model_service = new QSqlQueryModel;


    ui->tableView_kontragent->setModel(model_kontragent);
    ui->tableView_complex->setModel(model_complex);
    ui->tableView_service->setModel(model_service);


    connect(ui->pushButton_add_kontr,&QPushButton::clicked,this,&MainWindowOVDPO::AddKontragent);
    connect(ui->pushButton_edit_kontr,&QPushButton::clicked,this,&MainWindowOVDPO::EditKontragent);
    connect(ui->pushButton_del_kontr,&QPushButton::clicked,this,&MainWindowOVDPO::DelKontragent);
    connect(ui->tableView_kontragent,&MyTableView_ovdpo_kontragent::doubleClicked,this,&MainWindowOVDPO::EditKontragent);
    connect(ui->pushButton_add_complex,&QPushButton::clicked,this,&MainWindowOVDPO::AddComplex);
    connect(ui->pushButton_edit_complex,&QPushButton::clicked,this,&MainWindowOVDPO::EditComplex);
    connect(ui->tableView_complex,&MyTableView_ovdpo_complex::doubleClicked,this,&MainWindowOVDPO::EditComplex);
    connect(ui->pushButton_del_complex,&QPushButton::clicked,this,&MainWindowOVDPO::DelComplex);

    SetStyleGroupBox();
}

MainWindowOVDPO::~MainWindowOVDPO()
{
    settings->setValue("MainWindowOVDPO", geometry());
    delete ui;
}

bool MainWindowOVDPO::SetData()
{
    if(!SetModelKontragent()){
        return false;
    }
    if(!SetModelComplex()){
        return false;
    }
    if(!SetModelService()){
        return false;
    }
    connect(ui->tableView_kontragent,&MyTableView_ovdpo_kontragent::changeData_ovdpo_kontragent,this,&MainWindowOVDPO::refreshModelComplex);
    connect(ui->tableView_complex,&MyTableView_ovdpo_complex::changeData_ovdpo_complex,this,&MainWindowOVDPO::refreshModelService);
    connect(ui->action_exit,&QAction::triggered,this,&MainWindowOVDPO::Exit);
    connect(ui->action_schet,&QAction::triggered,this,&MainWindowOVDPO::Invoice);
    return true;
}

void MainWindowOVDPO::SetStyleGroupBox()
{
    ui->groupBox_kontragent->setStyleSheet("QGroupBox {"
                                "border: 2px solid red;"
                                "border-radius: 9px;"
                                "border-color: red;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: red"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_complex->setStyleSheet("QGroupBox {"
                                "border: 2px solid blue;"
                                "border-radius: 9px;"
                                "border-color: blue;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: blue"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

    ui->groupBox_service->setStyleSheet("QGroupBox {"
                                "border: 2px solid green;"
                                "border-radius: 9px;"
                                "border-color: green;"
                                "margin-top: 0.5em;"
                                "font-size: 11pt;"
                                "font-weight: bold;"
                                "color: green"
                                "} "
                                "QGroupBox::title {"
                                "subcontrol-origin: margin;"
                                "left: 3px;"
                                "padding: -3px 3px 0px 3px;"
                                "}");

}

bool MainWindowOVDPO::SetModelKontragent()
{
    if(SetQueryKontragent()){
        model_kontragent->setHeaderData(static_cast<int>(OVDPO::ovdpo_kontragent::innkontragent),Qt::Horizontal,"ИНН");
        model_kontragent->setHeaderData(static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent),Qt::Horizontal,"Наименование контрагента");
        model_kontragent->setHeaderData(static_cast<int>(OVDPO::ovdpo_kontragent::finishedkontragent),Qt::Horizontal,"Закрыт");

        ui->tableView_kontragent->setColumnHidden(static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent),true);

        ui->tableView_kontragent->setColumnWidth(static_cast<int>(OVDPO::ovdpo_kontragent::innkontragent),150);
        ui->tableView_kontragent->setColumnWidth(static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent),300);
        ui->tableView_kontragent->setColumnWidth(static_cast<int>(OVDPO::ovdpo_kontragent::finishedkontragent),100);

        ui->tableView_kontragent->selectRow(0);

        ui->tableView_kontragent->horizontalHeader()->setSectionResizeMode(static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent),QHeaderView::Stretch);
        return true;
    }
    return false;
}

bool MainWindowOVDPO::SetModelComplex()
{
    if(SetQueryComplex()){
        model_complex->setHeaderData(static_cast<int>(OVDPO::ovdpo_complex::namecomplex),Qt::Horizontal,"Наименование комплекса");
        model_complex->setHeaderData(static_cast<int>(OVDPO::ovdpo_complex::kolvocomplex),Qt::Horizontal,"Кол-во");

        ui->tableView_complex->setColumnHidden(static_cast<int>(OVDPO::ovdpo_complex::idcomplex),true);
        ui->tableView_complex->setColumnHidden(static_cast<int>(OVDPO::ovdpo_complex::idkontragent),true);

        ui->tableView_complex->setColumnWidth(static_cast<int>(OVDPO::ovdpo_complex::namecomplex),300);
        ui->tableView_complex->setColumnWidth(static_cast<int>(OVDPO::ovdpo_complex::kolvocomplex),100);

        ui->tableView_complex->selectRow(0);

        ui->tableView_complex->horizontalHeader()->setSectionResizeMode(static_cast<int>(OVDPO::ovdpo_complex::namecomplex),QHeaderView::Stretch);

        QModelIndex index = ui->tableView_kontragent->currentIndex();
        QString str = "Договор № " + model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent))).toString();
        ui->groupBox_complex->setTitle(str);
        return true;
    }
    return false;
}

bool MainWindowOVDPO::SetModelService()
{
    if(SetQueryService()){
        model_service->setHeaderData(static_cast<int>(OVDPO::ovdpo_service::codeservice),Qt::Horizontal,"Код");
        model_service->setHeaderData(static_cast<int>(OVDPO::ovdpo_service::nameservice),Qt::Horizontal,"Наименование");
        model_service->setHeaderData(static_cast<int>(OVDPO::ovdpo_service::stoimostservice),Qt::Horizontal,"Цена");

        ui->tableView_service->setColumnHidden(static_cast<int>(OVDPO::ovdpo_service::idservice),true);
        ui->tableView_service->setColumnHidden(static_cast<int>(OVDPO::ovdpo_service::idcomplex),true);

        ui->tableView_service->setColumnWidth(static_cast<int>(OVDPO::ovdpo_service::idservice),100);
        ui->tableView_service->setColumnWidth(static_cast<int>(OVDPO::ovdpo_service::nameservice),300);
        ui->tableView_service->setColumnWidth(static_cast<int>(OVDPO::ovdpo_service::stoimostservice),150);

        ui->tableView_service->horizontalHeader()->setSectionResizeMode(static_cast<int>(OVDPO::ovdpo_service::nameservice),QHeaderView::Stretch);

        QModelIndex index = ui->tableView_complex->currentIndex();
        QString str = "Комплекс : " + model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::namecomplex))).toString();
        ui->groupBox_service->setTitle(str);
        return true;
    }
    return false;
}

bool MainWindowOVDPO::SaveComplexToDB(QString name_complex, QStringList list_service)
{
    QSqlQuery query;
    QModelIndex index = ui->tableView_kontragent->currentIndex();
    int idkontragent = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent))).toInt();
    query.prepare("select * from ovdpo.complex where namecomplex = ? and idkontragent = ?");
    query.addBindValue(name_complex);
    query.addBindValue(idkontragent);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: blue'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    if(query.first()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: red'>Такое наименование комплекса существует.</p>");
        return false;
    }

    if(db.driver()->hasFeature(QSqlDriver::Transactions))   // если драйвер базы данных поддерживает транзакции
    {

        if(!db.transaction())
        {
            QMessageBox::warning(this,"Внимание",
                                 "<<p style='text-align: center; color: red>Не вышло открыть транзакциюю.<br>Попробуйте позднее или "
                                 "обратитесь к Администратору!<br>" + db.lastError().text() + "</p>");
            return false;
        }
        else{
            query.prepare("select set_config('auth.username', ?, false)");
            query.addBindValue(UserName);
            query.exec();
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center; color: blue'>Ошибка выполнения запроса set_config.<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
                return false ;
            }
            query.prepare("insert into ovdpo.complex(idkontragent, namecomplex,kolvocomplex) values(?,?,?) returning idcomplex");
            query.addBindValue(idkontragent);
            query.addBindValue(name_complex);
            query.addBindValue(0);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center; color: blue'>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                db.rollback();
                return false;
            }
            query.first();
            int idcomplex = query.value("idcomplex").toInt();

            for(int row = 0; row < list_service.count(); row++){
                QString str = list_service.at(row);
                QStringList str_list = str.split(";");
                    query.prepare("insert into ovdpo.service(idcomplex,codeservice,nameservice,stoimostservice) values(?,?,?,?)");
                    query.addBindValue(idcomplex);
                    query.addBindValue(str_list.at(0));
                    query.addBindValue(str_list.at(1));
                    query.addBindValue(str_list.at(2));
                    if(!query.exec()){
                        QMessageBox::critical(this,"Ошибка",
                                              "<p style='text-align: center; color: blue'>Ошибка выполнения запроса.<br><font color=red>" + query.lastError().text() +"</font></p>");

                        db.rollback();
                        return false;
                    }
            }
            if(!db.commit()){
                QMessageBox::critical(this,"Ошибка",
                                      "<p style='text-align: center; color: blue'>Транзакция не может быть выполнена!<br><font color=red>" + query.lastError().text() +"</font></p>");
                db.rollback();
               return false;
            }
            QMessageBox *mbox = new QMessageBox(this);
            mbox->setWindowTitle(tr("Внимание"));
            mbox->setText("Данные успешно сохранены.");
            mbox->setModal(true);
            mbox->setStandardButtons(QMessageBox::Ok);
            mbox->setIcon(QMessageBox::Information);
            QTimer::singleShot(1000,mbox,SLOT(accept()));
            mbox->exec();
            return true;
        }
    }
    else
    {
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center; color: blue'>Транзакции не поддерживаются в используемой СУБД!</p>");
        return false ;
    }

}

void MainWindowOVDPO::closeEvent(QCloseEvent *)
{
    emit exit();
}

bool MainWindowOVDPO::SetQueryKontragent()
{
    QSqlQuery query;
    query.prepare("select * from ovdpo.kontragent order by namekontragent");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    model_kontragent->setQuery(query);
    return true;
}

bool MainWindowOVDPO::SetQueryComplex()
{
    QModelIndex index = ui->tableView_kontragent->currentIndex();
    int idkontragent = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent))).toInt();
    QSqlQuery query;
    query.prepare("select idcomplex, idkontragent, namecomplex, to_char(kolvocomplex,'990') || ' чел.' from ovdpo.complex where idkontragent = ? order by namecomplex");
    query.addBindValue(idkontragent);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: blue'>Ошибка выполнения запроса.<br><span style='color: black'>(" + query.lastQuery() +")<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    model_complex->setQuery(query);
    return true;
}

bool MainWindowOVDPO::SetQueryService()
{
    QModelIndex index = ui->tableView_complex->currentIndex();
    int idcomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::idcomplex))).toInt();
    QSqlQuery query;
    query.prepare("select idservice,idcomplex,codeservice,nameservice,to_char(stoimostservice,'999999990.00') || ' руб.' from ovdpo.service where idcomplex = ? order by nameservice");
    query.addBindValue(idcomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    model_service->setQuery(query);
    return true;
}

void MainWindowOVDPO::refreshModelComplex()
{
    QModelIndex index = ui->tableView_kontragent->currentIndex();
    QString str = "Договор № " + model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent))).toString();
    ui->groupBox_complex->setTitle(str);
    SetQueryComplex();
    if(!model_complex->rowCount() == 0){
        ui->tableView_complex->selectRow(0);
    }
    else{
        refreshModelService();
    }
    ui->tableView_kontragent->setFocus();
}

void MainWindowOVDPO::refreshModelService()
{
    QModelIndex index = ui->tableView_complex->currentIndex();
    QString str = "Комплекс : " + model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::namecomplex))).toString();
    ui->groupBox_service->setTitle(str);
    SetQueryService();
    ui->tableView_service->selectRow(0);
    ui->tableView_complex->setFocus();
}

void MainWindowOVDPO::AddKontragent()
{
    ovdpo_kontr_add_edit *d = new ovdpo_kontr_add_edit(this);
    if(d->exec()){
        SetQueryKontragent();
        QModelIndexList indexlist = model_kontragent->match(model_kontragent->index(0,static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent)),Qt::DisplayRole,QVariant(d->GetIdkontragent()),1);
        int row = 0;
        if(!indexlist.isEmpty()){
            row = indexlist.at(0).row();
        }
        ui->tableView_kontragent->selectRow(row);
        ui->tableView_kontragent->setFocus();
    }
    delete d;
}

void MainWindowOVDPO::EditKontragent()
{
    QModelIndex index = ui->tableView_kontragent->currentIndex();
    if(!index.isValid()){
        return;
    }
    ovdpo_kontr_add_edit *d = new ovdpo_kontr_add_edit(this);
    int idkontragent = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent))).toInt();
    QString inn = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::innkontragent))).toString();
    QString name = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent))).toString();
    d->SetData(idkontragent, inn, name);
    if(d->exec()){
        SetQueryKontragent();
        ui->tableView_kontragent->selectRow(index.row());
        ui->tableView_kontragent->setFocus();
    }
    delete d;

}

void MainWindowOVDPO::DelKontragent()
{
    QModelIndex index = ui->tableView_kontragent->currentIndex();
    if(!index.isValid()){
        return;
    }
    int idkontragent = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent))).toInt();
//    QString inn = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::innkontragent))).toString();
    QString name = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent))).toString();

    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                        "<p style='text-align: center; font-size: 12pt; color: black'>Будет удален контрагент <span style='color: blue'>" + name + "<span style='color: black'> и все данные по нему!"
                                        "<br> Удалить ?</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    QSqlQuery query;
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center; color: blue'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.prepare("delete from ovdpo.kontragent cascade where idkontragent = ?");
    query.addBindValue(idkontragent);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: blue'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }

    int row = index.row();
    if(model_kontragent->rowCount() == 0){
        row = 0;
    }
    else if(row == model_kontragent->rowCount()-1){
        row--;
    }
    SetQueryKontragent();
    ui->tableView_kontragent->selectRow(row);
    ui->tableView_kontragent->setFocus();
}

void MainWindowOVDPO::AddComplex()
{
    enum columns {number,nom,codecomplex,namecomplex,codeservice,nameservice,cena};
    QModelIndex index = ui->tableView_kontragent->currentIndex();
    int idkontragent = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent))).toInt();
    if(idkontragent == 0){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center ; color: red'>Не выбран контрагент</p>");
        return;
    }
    QString file_name = QFileDialog::getOpenFileName(this,"Выбор файла","c:\\TMP\\","Документы (*.xlsx *.xls)");
    if(file_name.isEmpty()){
        return ;
    }

//    QMap<QString,QString> map_complex;
//    QMap<QString,QStringList> map_services;

    QRegExp reg("^[1-9]\\d?\\d?$");
    QXlsx::Document xlsx(file_name);

    QString complex_pol = " женщины";
    if(xlsx.read(4,nom).toString() == "Мужской"){
        complex_pol = " мужчины";
    }

    if(xlsx.read(7,nom).toString() != "№"){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: red'>Неверный формат файла.</p>");
        return;
    }

    int row = 7;
    int rowEmpty = 0;

    QProgressDialog *ppwt;
    ppwt = new QProgressDialog("Загрузка прайса...","Отмена",0,100
                               ,this);
    ppwt->setWindowTitle("Wait...");
    ppwt->setMinimumDuration(0);
    ppwt->setValue(0);
    int n = 0;
//    QString str_current_codecomplex ="";
    QString name_complex = "";
    QStringList str_list_service = QStringList();
    while(rowEmpty < 10) {
        row++;
        if(!xlsx.read(row,nom).isValid()){
            rowEmpty++;
            continue;
        }
        else{
            rowEmpty = 0;
        }
        QString str_nom = xlsx.read(row,nom).toString();
        QString str_codecomplex = xlsx.read(row,codecomplex).toString();
        QString str_namecomplex = xlsx.read(row,namecomplex).toString();
        QString str_codeservice = xlsx.read(row,codeservice).toString();
        QString str_nameservice = xlsx.read(row,nameservice).toString();
        QString str_cena = xlsx.read(row,cena).toString();
        if(!reg.exactMatch(str_nom)){
            continue;
        }
        if(!str_codecomplex.isEmpty()){
            name_complex += "[" + str_namecomplex + "] ";
        }
        str_list_service.append(str_codeservice + ";" +str_nameservice + ";" + str_cena);

//        if (!str_codecomplex.isEmpty() && reg.exactMatch(str_nom)) {
//            if(!map_complex.contains(str_codecomplex)){
//                map_complex.insert(str_codecomplex,str_namecomplex);
//                str_current_codecomplex = str_codecomplex;
//                str_list_service.clear();
//                str_list_service.append(str_codeservice + ";" +str_nameservice + ";" + str_cena);
//            }
//        }
//        else if (str_codecomplex.isEmpty() && !str_list_service.isEmpty() && reg.exactMatch(str_nom)) {
//            str_list_service.append(str_codeservice + ";" +str_nameservice + ";" + str_cena);
//        }
//        else if (str_nom == "Итого") {
//            map_services.insert(str_current_codecomplex,str_list_service);
//            str_list_service.clear();
//        }
        n++;
        qApp->processEvents();
        ppwt->setValue(n++);
    }
    name_complex = name_complex.trimmed() + complex_pol;
//    qDebug() << name_complex;
//    qDebug() << str_list_service;
//    qDebug() << map_complex;
//    qDebug() << map_services;
    delete ppwt;
    if(SaveComplexToDB(name_complex,str_list_service)){
        SetQueryComplex();
        QModelIndexList indexlist = model_complex->match(model_complex->index(0,static_cast<int>(OVDPO::ovdpo_complex::namecomplex)),Qt::DisplayRole,QVariant(name_complex));
        if(indexlist.length() != 0){
            QModelIndex index = indexlist.at(0);
            ui->tableView_complex->selectRow(index.row());
            ui->tableView_complex->setFocus();
        }
    }
}

void MainWindowOVDPO::EditComplex()
{
    QModelIndex index = ui->tableView_complex->currentIndex();
    if(!index.isValid()){
        return;
    }
    ovdpo_complex_edit *d = new ovdpo_complex_edit(this);
    int idcomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::idcomplex))).toInt();
    QString namecomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::namecomplex))).toString();
    int kolvocomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::kolvocomplex))).toString().remove(" чел.").toInt();
//    qDebug() << idcomplex << namecomplex << kolvocomplex;
    d->setData(idcomplex,namecomplex,kolvocomplex);
    if(d->exec()){
        QSqlQuery query;
        query.prepare("select set_config('auth.username', ?, false)");
        query.addBindValue(UserName);
        query.exec();
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                                  "<p style='text-align: center; color: blue'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            delete d;
            return;
        }
        query.prepare("update ovdpo.complex SET kolvocomplex = ? where idcomplex = ?");
        query.addBindValue(d->getKolvo());
        query.addBindValue(idcomplex);
        if(!query.exec()){
            QMessageBox::critical(this,"Ошибка",
                            "<p style='text-align: center; color: blue'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
            delete d;
            return;
        }
        SetQueryComplex();
        ui->tableView_complex->selectRow(index.row());
        ui->tableView_complex->setFocus();
    }
    delete d;
}

void MainWindowOVDPO::DelComplex()
{
    QString namekontragent = model_kontragent->data(model_kontragent->index(ui->tableView_kontragent->currentIndex().row(),static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent))).toString();
    QModelIndex index = ui->tableView_complex->currentIndex();
    if(!index.isValid()){
        return;
    }
    QString namecomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::namecomplex))).toString();
    int idcomplex = model_complex->data(model_complex->index(index.row(),static_cast<int>(OVDPO::ovdpo_complex::idcomplex))).toInt();
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                        "<p style='text-align: center; font-size: 12pt; color: blue'>Будет удален комплекс контрагента " + namekontragent +
                                        "<br> Удалить комплекс <span style='color: red'>" + namecomplex + "</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
    QSqlQuery query;
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    query.exec();
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center; color: blue'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    query.prepare("delete from ovdpo.complex cascade where idcomplex = ?");
    query.addBindValue(idcomplex);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center; color: blue'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return;
    }
    refreshModelComplex();
}

void MainWindowOVDPO::Invoice()
{
    QModelIndex index = ui->tableView_kontragent->currentIndex();
    if(!index.isValid()){
        return;
    }
    int idkontragent = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::idkontragent))).toInt();
    QString namekontragent = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::namekontragent))).toString();
    QString inn = model_kontragent->data(model_kontragent->index(index.row(),static_cast<int>(OVDPO::ovdpo_kontragent::innkontragent))).toString();

    QSqlQuery query, query_complex;
    query_complex.prepare("select namecomplex, kolvocomplex from ovdpo.complex where idkontragent = ?");
    query_complex.addBindValue(idkontragent);
    query_complex.exec();
    query_complex.first();

    query.prepare("select codeservice,nameservice, sum(kolvocomplex) kol , stoimostservice, sum(kolvocomplex * stoimostservice) sum from ovdpo.service"
                  " left join ovdpo.complex using(idcomplex)"
                  " left join ovdpo.kontragent using(idkontragent)"
                  " where idkontragent = ?"
                  " group by codeservice,nameservice, stoimostservice");
    query.addBindValue(idkontragent);
    query.exec();
    query.first();
    query.first();

    QVariant s;
    int rowoffset = 0;
    QXlsx::Document xlsx;
    QXlsx::Format formatHeader, formatLeft,formatRigth, formatCenter, formatMoney, formatData,formatNumber;
    QXlsx::Format formatLeftTextBold,formatRigthText;
    QXlsx::CellRange range;

    QString filename,str;
    QString winpath;
    winpath = "/Downloads/";

    filename = QDir::homePath() + winpath + inn + "_" + QDateTime::currentDateTime().toString("ddMMyyyyHHmm") + ".xlsx";

    formatHeader.setFontColor(QColor(Qt::blue));
    formatHeader.setFontSize(16);
    formatHeader.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatHeader.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatHeader.setBorderStyle(QXlsx::Format::BorderThin);
    formatHeader.setTextWarp(true);

    formatLeft.setFontColor(QColor(Qt::black));
    formatLeft.setFontSize(12);
    formatLeft.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeft.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatLeft.setBorderStyle(QXlsx::Format::BorderThin);
    formatLeft.setTextWarp(true);

    formatRigth.setFontColor(QColor(Qt::black));
    formatRigth.setFontSize(12);
    formatRigth.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigth.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatRigth.setBorderStyle(QXlsx::Format::BorderThin);

    formatCenter.setFontColor(QColor(Qt::black));
    formatCenter.setFontSize(12);
    formatCenter.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatCenter.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatCenter.setBorderStyle(QXlsx::Format::BorderThin);
    formatCenter.setTextWarp(true);

    formatMoney.setFontColor(QColor(Qt::black));
    formatMoney.setFontSize(12);
    formatMoney.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatMoney.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatMoney.setBorderStyle(QXlsx::Format::BorderThin);
    formatMoney.setNumberFormatIndex(8);

    formatData.setFontColor(QColor(Qt::black));
    formatData.setFontSize(12);
    formatData.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatData.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatData.setBorderStyle(QXlsx::Format::BorderThin);
    formatData.setNumberFormat("dd.mm.yyyy");

    formatNumber.setFontColor(QColor(Qt::black));
    formatNumber.setFontSize(12);
    formatNumber.setHorizontalAlignment(QXlsx::Format::AlignHCenter);
    formatNumber.setVerticalAlignment(QXlsx::Format::AlignVCenter);
    formatNumber.setBorderStyle(QXlsx::Format::BorderThin);
    formatNumber.setNumberFormatIndex(2);


    formatLeftTextBold.setFontColor(QColor(Qt::black));
    formatLeftTextBold.setFontName("Times New Roman");
    formatLeftTextBold.setFontSize(14);
    formatLeftTextBold.setFontBold(true);
    formatLeftTextBold.setHorizontalAlignment(QXlsx::Format::AlignLeft);
    formatLeftTextBold.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    formatRigthText.setFontColor(QColor(Qt::black));
    formatRigthText.setFontSize(12);
    formatRigthText.setHorizontalAlignment(QXlsx::Format::AlignRight);
    formatRigthText.setVerticalAlignment(QXlsx::Format::AlignVCenter);

    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset);
    range.setFirstColumn(1);
    range.setLastColumn(6);
    str = " Контрагент : " + inn + " " + namekontragent;
    xlsx.mergeCells(range,formatCenter);
    xlsx.write(1+rowoffset,1,str,formatCenter);
    rowoffset++;
    str = "";
    int kol_complex = 0;
    do{
        str += query_complex.value("namecomplex").toString() + "    " + query_complex.value("kolvocomplex").toString() + "  человек\n";
        kol_complex++;
    }while (query_complex.next());

    formatCenter.setFontColor(QColor(Qt::darkGreen));
    range.setFirstRow(1+rowoffset);
    range.setLastRow(1+rowoffset+kol_complex);
    range.setFirstColumn(1);
    range.setLastColumn(6);
    xlsx.mergeCells(range,formatCenter);
    xlsx.write(1+rowoffset,1,str,formatCenter);
    formatCenter.setFontColor(QColor(Qt::black));
    rowoffset+=kol_complex;
    rowoffset++;
    int col = 0;
    {
        s = QString("№ п.п.");
        xlsx.setColumnWidth(col+1,10);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Код услуги");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Наименование услуги");
        xlsx.setColumnWidth(col+1,60);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Цена");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Кол-во услуг");
        xlsx.setColumnWidth(col+1,20);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
        s = QString("Сумма");
        xlsx.setColumnWidth(col+1,30);
        xlsx.write(1+rowoffset, col+1, s, formatHeader);col++;
    }
    rowoffset++;
    int nom = 0;
    double itogo = 0;
    do{
        nom++;
        s = QString::number(nom);
        xlsx.write(1+rowoffset,1,s,formatCenter);
        s = query.value("codeservice").toString();
        xlsx.write(1+rowoffset,2,s,formatCenter);
        s = query.value("nameservice").toString();
        xlsx.write(1+rowoffset,3,s,formatLeft);
        s = query.value("stoimostservice").toDouble();
        xlsx.write(1+rowoffset,4,s,formatNumber);
        s = query.value("kol").toString();
        xlsx.write(1+rowoffset,5,s,formatCenter);
        s = query.value("sum").toDouble();
        itogo += query.value("sum").toDouble();
        xlsx.write(1+rowoffset,6,s,formatMoney);
        rowoffset++;
    }while(query.next());
    rowoffset++;
    xlsx.write(1+rowoffset,6,itogo,formatMoney);
    if(xlsx.saveAs(filename)){
//        filename = "Данные успешно сохранены.\n" + filename;
//        QMessageBox *mbox = new QMessageBox(this);
//        mbox->setWindowTitle(tr("Внимание"));
//        mbox->setText(filename);
//        mbox->setModal(true);
//        mbox->setStandardButtons(QMessageBox::Ok);
//        mbox->setIcon(QMessageBox::Information);
//        QTimer::singleShot(5000,mbox,SLOT(accept()));
//        mbox->exec();
        //**** Проверка на доступность файла *************************

        if(!QDesktopServices::openUrl(QUrl(QUrl::fromLocalFile(filename))))
        {

            QMessageBox::critical(this,"Ошибка",
                                  "<p style='text-align: center; color: red'>Ошибка открытия файла: <span style='color: blue'>" + filename + "</p>");
            return;

        }
    }
    else
    {
        filename = "Ошибка записи файла.\n" + filename;
        QMessageBox mbox(this);
        mbox.setWindowTitle(tr("Внимание"));
        mbox.setText(filename);
        mbox.setModal(true);
        mbox.setStandardButtons(QMessageBox::Ok);
        mbox.setIcon(QMessageBox::Critical);
        QTimer::singleShot(3000,&mbox,SLOT(accept()));
        mbox.exec();
    }
    return ;

}

void MainWindowOVDPO::Exit()
{
    emit exit();
}
