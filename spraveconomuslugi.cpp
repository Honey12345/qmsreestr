#include "spraveconomuslugi.h"
#include "ui_spraveconomuslugi.h"
#include "global.h"

spraveconomuslugi::spraveconomuslugi(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::spraveconomuslugi)
{
    ui->setupUi(this);
    if(settings->value("spraveconomuslugi").isValid()){
        setGeometry(settings->value("spraveconomuslugi").toRect());
    }
    setWindowTitle("Справочник услуг по ПЭО");
    model = new QStandardItemModel(this);
    ui->tableView->setModel(model);
    ui->pushButton_save->setEnabled(false);

    connect(ui->pushButton_add,&QPushButton::clicked,this,&spraveconomuslugi::addRecord);
    connect(ui->pushButton_save,&QPushButton::clicked,this,&spraveconomuslugi::SaveChanges);
    connect(ui->pushButton_del,&QPushButton::clicked,this,&spraveconomuslugi::delRecord);
    connect(this,&spraveconomuslugi::rejected,this,&spraveconomuslugi::CloseWindows);

    spravecon_delegate * spravecon_delegate_class = new spravecon_delegate(this); // делегат редактирования для справлчнико пользователей

    connect(spravecon_delegate_class,&spravecon_delegate::closeEditor,this,&spraveconomuslugi::SetChangeData);
    ui->tableView->setItemDelegateForColumn(static_cast<int>(Econom::econom::name_usl),spravecon_delegate_class);


//    ui->pushButton_add->setStyleSheet("QPushButton {"
//    "color: white;"
//    "background-color: #32CD32;"
//    "border-style: solid;"
//    "border-width:2px;"
//    "border-radius:10px;"
//    "border-color:  #32CD32;"
//    "max-width:30px;"
//    "max-height:30px;"
//    "min-width:30px;"
//    "min-height:30px;}"
//    "QPushButton:hover {background-color: #069D06;}"
//    "QPushButton:pressed {background-color: #069DA3;}");

//    ui->pushButton_del->setStyleSheet("QPushButton {"
//    "color: white;"
//    "background-color: #FF0000;"
//    "border-style: solid;"
//    "border-width:2px;"
//    "border-radius:10px;"
//    "border-color:  #FF0000;"
//    "max-width:30px;"
//    "max-height:30px;"
//    "min-width:30px;"
//    "min-height:30px;}"
//    "QPushButton:hover {background-color: #A73232;}"
//    "QPushButton:pressed {background-color: #FFABAB;}");
}

spraveconomuslugi::~spraveconomuslugi()
{
    settings->setValue("spraveconomuslugi", geometry());
    delete model;
    delete ui;
}

bool spraveconomuslugi::SetModel()
{
    QSqlQuery query;
    model->clear();
    query.prepare("select * from econom_usl order by name_usl");
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return false;
    }
    if(query.first()){
        QList<QStandardItem*> list;
        do{
            list.clear();
            for(int col = 0; col < query.record().count(); col++){
                list.append(new QStandardItem(query.value(col).toString()));
            }
            list.append(new QStandardItem(QString("false")));
            model->appendRow(list);
        }while(query.next());
        ui->tableView->selectRow(0);
    }
    else {
        model->setColumnCount(3);
    }
    SetHeaderData();
    SetColumnsWidth();
    ui->tableView->setFocus();
    return true;
}

void spraveconomuslugi::SetHeaderData()
{
    model->setHeaderData(static_cast<int>(Econom::econom::id),Qt::Horizontal," id ");
    model->setHeaderData(static_cast<int>(Econom::econom::name_usl),Qt::Horizontal," Наименование услуги ");
}

void spraveconomuslugi::SetColumnsWidth()
{
    ui->tableView->setColumnWidth(static_cast<int>(Econom::econom::id),50);
    ui->tableView->setColumnWidth(static_cast<int>(Econom::econom::name_usl),650);
    ui->tableView->setColumnHidden(static_cast<int>(Econom::econom::id),true);
    ui->tableView->setColumnHidden(static_cast<int>(Econom::econom::changes),true);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);

}

void spraveconomuslugi::addRecord()
{
    QList<QStandardItem*> list;
    list.append(new QStandardItem(QString()));
    list.append(new QStandardItem(QString()));
    list.append(new QStandardItem(QString("false")));
    model->appendRow(list);
    ui->tableView->selectRow(model->rowCount()-1);
}

void spraveconomuslugi::delRecord()
{
    QModelIndex index = ui->tableView->currentIndex();
    int id = model->data(model->index(index.row(),0)).toInt();
    if(id == 0){
        model->removeRow(index.row());
        ui->tableView->setFocus();
        ui->tableView->selectRow(ui->tableView->currentIndex().row());
        return;
    }
    QSqlQuery query;
    query.prepare("delete from econom_usl where id = ?");
    query.addBindValue(id);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                        "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
        return ;
    }
    model->removeRow(index.row());
    ui->tableView->setFocus();
    ui->tableView->selectRow(ui->tableView->currentIndex().row());
    return;
}

void spraveconomuslugi::SetChangeData()
{

    QModelIndexList list = model->match(model->index(0,static_cast<int>(Econom::econom::changes)),Qt::DisplayRole,QVariant("true"),-1,Qt::MatchFixedString);
    if(!list.isEmpty()){
        ui->pushButton_save->setEnabled(true);
    }
}

void spraveconomuslugi::SaveChanges()
{

    QSqlQuery   query;
    query.prepare("select set_config('auth.username', ?, false)");
    query.addBindValue(UserName);
    if(!query.exec()){
        QMessageBox::critical(this,"Ошибка",
                              "<p style='text-align: center'>Ошибка выполнения запроса set_config.<br><span style='color: red'>" + query.lastError().text() +"</p>");
       return;
    }

    for(int row = 0;row < model->rowCount(); row++){
        if(model->data(model->index(row,static_cast<int>(Econom::econom::name_usl)),Qt::DisplayRole).toString().isEmpty() || !(model->data(model->index(row,static_cast<int>(Econom::econom::changes)),Qt::DisplayRole).toBool())){
                continue;
        }
        int id = model->data(model->index(row,static_cast<int>(Econom::econom::id)),Qt::DisplayRole).toInt();
        if(id == 0){
            query.prepare("insert into econom_usl (name_usl) values(?)");
            query.addBindValue(model->data(model->index(row,static_cast<int>(Econom::econom::name_usl)),Qt::DisplayRole).toString());
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                return ;
            }
        }
        else{
            query.prepare("update econom_usl set name_usl = ? where id = ?");
            query.addBindValue(model->data(model->index(row,static_cast<int>(Econom::econom::name_usl))).toString());
            query.addBindValue(id);
            if(!query.exec()){
                QMessageBox::critical(this,"Ошибка",
                                "<p style='text-align: center'>Ошибка выполнения запроса.<br><span style='color: red'>" + query.lastError().text() +"</p>");
                return ;
            }

        }
    }
    SetModel();
    ui->pushButton_save->setEnabled(false);
}

void spraveconomuslugi::CloseWindows()
{
    QModelIndexList list = model->match(model->index(0,static_cast<int>(Econom::econom::changes)),Qt::DisplayRole,QVariant("true"),-1,Qt::MatchFixedString);
    if(list.isEmpty()){
       return;
    }
    int n = QMessageBox::warning(this,"","<p style='text-align: center; font-size: 16pt; color: red'> ВНИМАНИЕ !</p>"
                                         "<p style='text-align: center; font-size: 12pt; color: blue'> Несохраненные данные ,будут потеряны!<br>Записать данные ?</p>",
                                 QMessageBox::Yes|QMessageBox::No,
                                 QMessageBox::No);
    if (n != QMessageBox::Yes){
        return;
    }
   SaveChanges();
   return;
}
