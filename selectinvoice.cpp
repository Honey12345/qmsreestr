#include "selectinvoice.h"
#include "ui_selectinvoice.h"

selectinvoice::selectinvoice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::selectinvoice)
{
    ui->setupUi(this);
    QSqlQuery query;
    query.prepare("SELECT 'Счет ' || invoicenum || ' за ' || case when month = 1 then 'январь'"
                                     " when month = 2 then 'февраль'"
                                     " when month = 3 then 'март'"
                                     " when month = 4 then 'апрель'"
                                     " when month = 5 then 'май'"
                                     " when month = 6 then 'июнь'"
                                     " when month = 7 then 'июль'"
                                     " when month = 8 then 'август'"
                                     " when month = 9 then 'сентябрь'"
                                     " when month = 10 then 'октябрь'"
                                     " when month = 11 then 'ноябрь'"
                                     " when month = 12 then 'декабрь' end || ' ' || to_char(year,'9999') as schet, invoiceid FROM invoice ORDER BY year DESC,month,invoicenum");

    query.exec();
    model_comboBox_invoice = new QSqlQueryModel;
    model_comboBox_invoice->setQuery(query);
    ui->comboBox_invoice->setModel(model_comboBox_invoice);
    ui->comboBox_invoice->setModelColumn(0);
    ui->label->setStyleSheet("QLabel {color: red} ");
    connect(ui->pushButton,&QPushButton::clicked,this,&selectinvoice::SetInvoiceId);

}

selectinvoice::~selectinvoice()
{
    delete model_comboBox_invoice;
    delete ui;
}

int selectinvoice::GetInvoiceId()
{
    return invoiceid;
}

void selectinvoice::SetInvoiceId()
{
    QSqlRecord rec;
    rec = model_comboBox_invoice->record(ui->comboBox_invoice->currentIndex());
    invoiceid = rec.value(rec.indexOf("invoiceid")).toInt();
    this->accept();
}
