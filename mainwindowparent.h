#ifndef MAINWINDOWPARENT_H
#define MAINWINDOWPARENT_H

#include <QMainWindow>

namespace Ui {
class MainWindowParent;
}

class MainWindowParent : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowParent(QWidget *parent = nullptr);
    ~MainWindowParent();

private slots:

    void About();
    void userSpravochnik();

    void MainWindowsDogovora();
    void priceNormalization();
    void MainWindowProfOsmotr();

private:
    Ui::MainWindowParent *ui;
};

#endif // MAINWINDOWPARENT_H
