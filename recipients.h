#ifndef RECIPIENTS_H
#define RECIPIENTS_H

#include <QDialog>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QMessageBox>
#include <QDialog>

namespace Ui {
class recipients;
}

class recipients : public QDialog
{
    Q_OBJECT

public:

    enum class recipient :int {idrecipients,name_recipients};
    Q_ENUM(recipient)

    explicit recipients(QWidget *parent = nullptr);
    ~recipients();

    bool SetSqlQuery();
    bool SetSqlQueryModel();

private slots:

    void CheckEMail();
    void TableViewDoubleClicked(QModelIndex index);
    void DelPushButton();
    void AddPushButton();
    void SavePushButton();

private:
    Ui::recipients *ui;
    QSqlQueryModel *model_recipients;
};

#endif // RECIPIENTS_H
