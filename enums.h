#ifndef ENUMS_H
#define ENUMS_H
#include <QObject>
// Набор глобальных перечислителей
class Enums : public QObject {
public:
    /// достуно с std=с++11, 'class' прячет имена значений внутри имени типа Access ':int' фиксирует размер на указанном int
    enum class Access : int {AdminRole,
                             GuestRole,
                             EconomistRole,
                             SisterOVDPO};
    Q_ENUM(Access)

//    enum class TypeReestr : int {fz44Kontr,
//                                 fz223Kontr,
//                                 fz44Dog,
//                                 fz223Dog,
//                                 fzAll};
//    Q_ENUM(TypeReestr)

//    enum TypeHelp {regExp};
//    Q_ENUM(TypeHelp)

//    enum Regim {ViewOnly, Edit};
//    Q_ENUM(Regim)


//    enum class FieldsReestr : int {id,nom_por,istochnik,snizenie,kvr,kosgu,predmet,max_cena,sposob,name_otv,nom_izv,data_izv,inn,naimenovanie,nom_prot,
//                                    data_prot,sum_obespech,obespechenie,nom_gar,data_gar,nom_dog,osnovanie,nom_osn,data_osn,data_dog,
//                                    summa_dog,ist,data_post,srok_dogovora,izmenenia,filename,nom_dog_reestr,data_kas,data_otch,data};
//    enum class FieldsReestr : int {id, nom_por, predmet, max_cena, inn, naimenovanie, name_otv, filename, data, kvr, kosgu, typereestr, nom_dog, nom_izv};
//    Q_ENUM(FieldsReestr)

//    enum EnumB {
//        A,//< коллизий с EnumA::A не будет, EnumA::A не доступно как A, в отличие от EnumB::A
//        D,
//    };
//    Q_ENUM(EnumB)

    Q_OBJECT
    Enums() = delete; //< std=c++11, обеспечивает запрет на создание любого экземпляра Enums
};
#endif // ENUMS_H
